<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
//use Validator;

class Setting extends Model
{
    public $timestamps = false;

    protected $table = 'tbl_common_setting';

    public static function getList()
    {
        $query = DB::table('tbl_common_setting')
            ->select(['id','key_name','value','status','form_type'])
            ->where([['key_name','!=','ADMIN_ACTIVITY_PASSWORD']])
            ->whereIn('status',[0,1])
            ->orderBy('id', 'DESC');

        $list = $query->get();

        $array1 = [
            'MATCHODD_SETTING',
            'VIRTUAL_CRICKET_SETTING',
            'OTHERMARKET_SETTING',
            'ODDEVEN_SETTING',
            'KHADO_SETTING',
            'METER_SETTING',
            "BALLBYBALL_SETTING",
            'WINNER_SETTING',
            'BOOKMAKER_SETTING',
            'SESSION_SETTING',
            'BINARY_SETTING',
            'FOOTBALL_EVENT_SETTING',
            'TENNIS_EVENT_SETTING',
            'CRICKET_EVENT_SETTING',
            ];
            $array2 = [
                'FOOTBALL_EVENT_SETTING',
                'TENNIS_EVENT_SETTING',
                'CRICKET_EVENT_SETTING'
                ];
            
            foreach ($list as $key => $value) {
                if(in_array($value->key_name, $array1)){
                
                $list[$key]->value_array = json_decode($list[$key]->value);
                
                if($value->key_name == 'SESSION_SETTING')
                {
                    $arr_session_val = json_decode($list[$key]->value);
                    
                    $arrSessionSetting["Max_stack"] = (!empty($arr_session_val->MFY_MAX_STAKE_DEFAULT))?$arr_session_val->MFY_MAX_STAKE_DEFAULT :0; 
                    $arrSessionSetting["Min_stack"] = (!empty($arr_session_val->MFY_MIN_STAKE_DEFAULT))?$arr_session_val->MFY_MIN_STAKE_DEFAULT :0;
                    $arrSessionSetting["Max_profit_limit"] = (!empty($arr_session_val->MFY_MAX_PROFIT_LIMIT_DEFAULT))?$arr_session_val->MFY_MAX_PROFIT_LIMIT_DEFAULT :0;
                    $arrSessionSetting["Max_odd_limit"] = (!empty($arr_session_val->MFY_MAX_ODD_LIMIT_DEFAULT))?$arr_session_val->MFY_MAX_ODD_LIMIT_DEFAULT :0;
                    $arrSessionSetting["Bet_delay"] = (!empty($arr_session_val->BETDELAY))?$arr_session_val->BETDELAY :0;
                    
                    $list[$key]->value_array = $arrSessionSetting;
                }
             }


                if(in_array($value->key_name, $array2)){
                    $list[$key]->fieldKey = 1;
                    }else{
                    $list[$key]->fieldKey = 0;
                }
            }

        return $list;
    }

    public static function doCreate($data)
    {
        $response = ['status' => 0, 'error' => ['message' => 'Something Wrong!']];

        $cUser = Auth::user();

        if( $cUser != null && $cUser->roleName == 'ADMIN' ){

            $keyName = strtoupper(str_replace(' ','_',trim($data['key'])));

            $checkUnique = Setting::where([['key_name',$keyName]])->first();

            if( $checkUnique != null ){
                $response = ['status' => 0, 'error' => ['message' => 'This key is already used!']];
                return $response;
            }

            $setting = new Setting();

            $setting->key_name = $keyName;
            $setting->value = trim($data['value']);

            if ($setting->save()) {

                if( $setting->key_name == 'GLOBAL_COMMENTARY' ){
                    DB::table('tbl_global_commentary')->insert(['commentary' => trim($data['value'])]);
                }

                $response = [
                    'status' => 1,
                    'success' => [
                        'message' => 'Created successfully!'
                    ]
                ];
            }

        }

        return $response;
    }

    public static function doUpdate($data)
    {
        $response = ['status' => 0, 'error' => ['message' => 'Something Wrong!']];

        $cUser = Auth::user();

        if( $cUser != null && $cUser->roleName == 'ADMIN'){

            $setting = Setting::where([['id',$data['id']]])->first();

            if( $setting != null ){
                if( isset($data['status']) ){
                    $setting->status = trim($data['status']);
                    if( $data['status'] == 1 ){
                        $message = 'Active successfully!';
                    }else if( $data['status'] == 0 ){
                        $message = 'InActive successfully!';
                    }else{
                        $message = 'Delete successfully!';
                    }
                }else{

                    $array1 = [
                        'MATCHODD_SETTING',
                        'VIRTUAL_CRICKET_SETTING',
                        'OTHERMARKET_SETTING',
                        'ODDEVEN_SETTING',
                        'KHADO_SETTING',
                        'METER_SETTING',
                        "BALLBYBALL_SETTING",
                        'WINNER_SETTING',
                        'BOOKMAKER_SETTING',
                        'SESSION_SETTING',
                        'BINARY_SETTING',
                        'FOOTBALL_EVENT_SETTING',
                        'TENNIS_EVENT_SETTING',
                        'CRICKET_EVENT_SETTING'
                        ];
                        if(in_array($setting->key_name, $array1)){
                        // $validator = Validator::make($data,[
                        // 'Max_stack' => 'required',
                        // 'Min_stack' => 'required',
                        // 'Max_odd_limit' => 'required',
                        // 'Bet_delay' => 'required',
                        // ]);
                        // if($validator->fails()){
                        // $arrValidatorErrors = $validator->errors()->toArray();
                        // $err_msgs = array_values($arrValidatorErrors);
                        // return ['status' => 1,'key' => $setting->key_name, 'success' => ["message" => $err_msgs[0][0],403]];
                        // }
                        $setting_array["Max_stack"] = $data['Max_stack'];
                        $setting_array["Min_stack"] = $data['Min_stack'];
                        $setting_array["Max_profit_limit"] = $data['Max_profit_limit'];
                        $setting_array["Max_odd_limit"] = $data['Max_odd_limit'];
                        $setting_array["Bet_delay"] = $data['Bet_delay'];
                        if( $setting->key_name == 'FOOTBALL_EVENT_SETTING' || $setting->key_name == 'CRICKET_EVENT_SETTING' ||
                        $setting->key_name == 'TENNIS_EVENT_SETTING' )
                        {
                        $setting_array["Max_profit_limit"] = $setting_array["Max_profit_all_limit"] = isset($data["Max_profit_all_limit"])?$data["Max_profit_all_limit"]:"0";
                        if(isset($data['Event_Max_profit_all_limit'])){
                        $setting_array["Event_Max_profit_all_limit"] = $data['Event_Max_profit_all_limit'];
                        }
                        else{
                        $response = [ 'status' => 0,'success' => [ 'message' => 'Please enter max profit all limit value.' ] ];
                        return $response;
                        }
                        //remove Max_profit_limit
                        }
                        
                        if($setting->key_name == 'SESSION_SETTING'){
                        $setting_array = [
                        "MFY_MAX_STAKE_DEFAULT" => $data['Max_stack'],
                        "MFY_MIN_STAKE_DEFAULT" => $data['Min_stack'],
                        "MFY_MAX_PROFIT_LIMIT_DEFAULT" => $data['Max_profit_limit'],
                        "MFY_MAX_ODD_LIMIT_DEFAULT" => $data['Max_odd_limit'],
                        "BETDELAY" => $data['Bet_delay'],
                        ];
                        }
                        $data['value'] = json_encode($setting_array);
                        }


                    $setting->value = trim($data['value']);
                    $message = 'Updated successfully!';
                }

                if ($setting->save()) {
                    if( !isset($data['status']) && $setting->key_name == 'GLOBAL_COMMENTARY' ){
                        DB::table('tbl_global_commentary')->insert(['commentary' => trim($data['value'])]);
                    }
                    $response = [
                        'status' => 1,
                        'key' => $setting->key_name,
                        'success' => [
                            'message' => $message
                        ]
                    ];
                }
            }

        }

        return $response;
    }

}
