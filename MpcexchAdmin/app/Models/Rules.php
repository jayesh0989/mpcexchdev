<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Rules extends Model
{
    public $timestamps = false;

    protected $table = 'tbl_common_rules';

    public static function getList()
    {
        $query = DB::table('tbl_common_rules')
            ->select(['id','sport','market','content','status'])
            ->whereIn('status',[0,1])
            ->orderBy('id', 'DESC');

        $list = $query->get();

        return $list;
    }

    public static function doCreate($data)
    {
        $response = ['status' => 0, 'error' => ['message' => 'Something Wrong!']];

        $cUser = Auth::user();

        if( $cUser != null && $cUser->role == 1){

            $sport = trim($data['sport']);
            $market = trim($data['market']);

            $checkUnique = Rules::where([['sport',$sport],['market',$market]])->first();

            if( $checkUnique != null ){
                $response = ['status' => 0, 'error' => ['message' => 'This sport and market is already added!']];
                return $response;
            }

            $setting = new Rules();

            $setting->sport = $sport;
            $setting->market = $market;
            $setting->content = trim($data['rules']);

            if ($setting->save()) {
                $response = [
                    'status' => 1,
                    'success' => [
                        'message' => 'Created successfully!'
                    ]
                ];
            }

        }

        return $response;
    }

    public static function doUpdate($data)
    {
        $response = ['status' => 0, 'error' => ['message' => 'Something Wrong!']];

        $cUser = Auth::user();

        if( $cUser != null && $cUser->role == 1){

            $setting = Rules::where([['id',$data['id']]])->first();

            if( $setting != null ){
                if( isset($data['status']) ){
                    $setting->status = trim($data['status']);
                    if( $data['status'] == 1 ){
                        $message = 'Active successfully!';
                    }else if( $data['status'] == 0 ){
                        $message = 'InActive successfully!';
                    }else{
                        $message = 'Delete successfully!';
                    }
                }else{
                    $setting->content = trim($data['rules']);
                    $message = 'Updated successfully!';
                }

                if ($setting->save()) {
                    $response = [
                        'status' => 1,
                        'success' => [
                            'message' => $message
                        ]
                    ];
                }
            }

        }

        return $response;
    }

}
