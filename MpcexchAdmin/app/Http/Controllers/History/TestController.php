<?php

namespace App\Http\Controllers\History;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class TestController extends Controller
{

    /**
     * testUserData
     */
    public function checkUserSettlement1(){

        $response = ['status' => 0, 'error' => ['message' => 'Something Wrong! Data not available on this moment !']];

        try {
            $userListArr1 = [];
            $userListArr = [16232,16225,16223,16221,16220,16219,16218,16217,16216,16215,16214,16207,16205,16202,16201,16200,16197,16196,16195,16194,16191,16190,16189,16188,16187,16186,16185,16184,16183,16182,16181,16180,16172,16170,16169,16168,16163,16160,16157,16153,16146,16144,16142,16091,16090,16089,16088,16087,15857,15343,15181,15180,15179,15178,15177,15176,15175,15174,15173,15172,15148,14938,14937,14936,13815,13814,13543,13271,12417,12413,12412,12411,12351,12167,11999,11998,11997,11996,11882,11736,11735,11718,11717,11716,11715,11714,11712,11711,11709,11708,11707,11706,11705,11704,11703,11702,11701,11700,11699,11698,11697,11696,11695,11686,11685,11684,11683,11682,11681,11680,11679,11678,11677,11676,11675,11674,11673,11672,11671,11670,11669,11668,11667,11666,11665,11664,10728,10455,10454,10453,10161,9961];
            //$userListArr = [16232,16225,16223,16221,16220,16219,16218,16217,16216,16215];

//            $conn3 = DB::connection('mysql3');
//            foreach ( $userListArr as $k=>$uid ){
//                $transData = $conn3->table('tbl_transaction_client')
//                    ->where([['eType',0],['userId',$uid],['mid','!=',0],['status',1]])->first();
//                if( $transData != null ){
//                    $userListArr1[] = $uid;
//                }
//            }


            $userList = DB::table('tbl_user')->select(['id','username'])
                ->where([['status', 1], ['role', 4]])
                ->whereIn('id',$userListArr1)->get();

//            $userList = DB::table('tbl_user as u')->select(['u.id as id'])
//                ->leftjoin('tbl_user_info as ui', 'u.id', '=', 'ui.uid')
//                ->where([['u.status', 1], ['u.role', 4], ['u.id','>', 9000]])
//                ->orderBy('u.id', 'desc')
//                ->offset(6000)->limit(1000)->get();

            if( $userList->isNotEmpty() ){
                $userListArr = [];
                foreach ( $userList as $user ){
                    $userListArr[] = ['userId' => $user->username.' ('.$user->id.') '];
//                    $profitLossClient = DB::table('tbl_user_profit_loss')
//                        ->where([['clientId', $user->id],['userId', 1]])->first();
//                    if( $profitLossClient == null ){
//                        $userListArr[] = $user->id;
//                    }

                }
            }

            $response = [ 'userListArr' => $userListArr ];
            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * testUserData
     */
    public function testUserDataOld(){

        $response = ['status' => 0, 'error' => ['message' => 'Something Wrong! Data not available on this moment !']];

        try {
            $marketId = '4.1596545581-BM';
            $tbl = 'tbl_bet_history';
            $clientArr1 = $clientArr2 = $clients1 = $clients2 = [];
            $query1 = DB::table($tbl)
                ->where([['mid',$marketId],['status',1]]);

            $clientArr1 = $query1->distinct()->select('uid')->get();
            if( $clientArr1->isNotEmpty() ){
                foreach ($clientArr1 as $data){
                    $clients1[] = $data->uid;
                }
            }
            $query2 = DB::table('tbl_transaction_client_view')
                ->where([['mid',$marketId],['status',1]]);

            $clientArr2 = $query2->distinct()->select('userId')->get();
            if( $clientArr2->isNotEmpty() ){
                foreach ($clientArr2 as $data){
                    $clients2[] = $data->userId;
                }
            }

            $response = [
                'diff' => array_diff($clients1,$clients2),
                'betUser' => [ 'count' => count($clientArr1), 'list' => $clients1 ],
                'trasUser' => [ 'count' => count($clientArr2), 'list' => $clients2 ] ];
            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }
    /**
     * checkEncryptDecrypt
     */
    public function checkEncryptDecrypt()
    {
        $key = 'ca0761c0b720b9ce1c93183d8f03d854';
        //$jsonData = '{"username":"admin","password":"NEW@2020dream","systemId":1}';
        $jsonData = '{"status":1,"data":{"token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiMTk1Y2Q4NmVmNGEzNTdlMmM2Y2NkY2ZkMDMzODY4NGQwMzcxYjkyOTA1ZTNiZjZmYjFmMjI3MDczZDc5ZDQwOGE4OGJkN2NmMzQ1YTdkOGQiLCJpYXQiOjE1OTEwODY2MzEsIm5iZiI6MTU5MTA4NjYzMSwiZXhwIjoxNjIyNjIyNjMxLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.Fo6fe4HMW4u9WgebzEGDUtO0AwYvuHtBnjsuTxqAz5NkEBgFbixOIVEIli-0n8xKUqMt5BX_CzSMcPQu-xF_bTxM29PXYApWHUQUItPidIl2ZJ9B_uB90W2uWRjO2-GpbqY-uTu6OtS-ShgFbZPLyeegyOA0-O40YNT4eqtnOtFw53H2TbiyWcjat8kgAjdc-U25a4jT8rKXzdrwzmU0rnpOGM4mLGcurVpTlUGmboAB_XscBIO1_K5BvBudbG_PS9XlC6kujZBxCuzh8iiS9v9N4O3xFu6f2In6jjS3c8ga8u63gl7JmV3g3rPqA2iK0yDhX2Y3zKaeCOeAO4QmMzZHIvu6QDXcLrGFlHo5Qbpxm9zeA_37Mqq7eC1bU03AL58nmlFoyTcVL5oEkAAv1ZEcSa8piBdqL9LZIpibuLHzCERvuJDCaVVabK01Ws2z_R1bPOzoC13fSTs8Tif-lZZYJHCjM9zhtRRDbprKmlYY8QmfMvUKESCD5Py0_XA4y1-TZbOilvlDZsI29oUReeaYanHRim-H7Ek42P47Hx39epHkr4eDiJd5vL0P4itT0vTjUDDpyriUNJ11sTvHejLHKD0Fx27wW7EC_Z9xDlAx9AsChdmII55IeA7TqP7RGwybTacDDubvdIybRMiCWoSRAWJTZ0orn9uQAt5TcT4","systemId":1,"auth_id":1,"name":"Admin","username":"admin","role":"ADMIN","last_logged_on":"2020-03-09 06:08:47","is_password_updated":1},"success":{"message":"Logged In successfully!"}}';
        echo 'jsonData : ' . $jsonData;
        $encrypted = $this->cryptoJsAesEncrypt1($key, $jsonData);
        echo '  ==> encrypted : ' . $encrypted;
        $decrypted = $this->cryptoJsAesDecrypt1($key, $encrypted);

        echo '  ==> decrypted : ' . $decrypted;
    }

    /**
     * Decrypt data from a CryptoJS json encoding string
     */
    public function cryptoJsAesDecrypt1($passphrase, $jsonString){
        $jsondata = json_decode($jsonString, true);
        $salt = hex2bin($jsondata["s"]);
        $ct = base64_decode($jsondata["ct"]);
        $iv  = hex2bin($jsondata["iv"]);
        $concatedPassphrase = $passphrase.$salt;
        $md5 = array();
        $md5[0] = md5($concatedPassphrase, true);
        $result = $md5[0];
        for ($i = 1; $i < 3; $i++) {
            $md5[$i] = md5($md5[$i - 1].$concatedPassphrase, true);
            $result .= $md5[$i];
        }
        $key = substr($result, 0, 32);
        $data = openssl_decrypt($ct, 'aes-256-cbc', $key, true, $iv);
        return json_decode($data, true);
    }

    /**
     * Encrypt value to a cryptojs compatiable json encoding string
     */
    public function cryptoJsAesEncrypt1($passphrase, $value){
        $salt = openssl_random_pseudo_bytes(8);
        $salted = '';
        $dx = '';
        while (strlen($salted) < 48) {
            $dx = md5($dx.$passphrase.$salt, true);
            $salted .= $dx;
        }
        $key = substr($salted, 0, 32);
        $iv  = substr($salted, 32,16);
        $encrypted_data = openssl_encrypt(json_encode($value), 'aes-256-cbc', $key, true, $iv);
        $data = array("ct" => base64_encode($encrypted_data), "iv" => bin2hex($iv), "s" => bin2hex($salt));
        return json_encode($data);
    }

    /**
     * checkHashKey
     */
    public function checkHashKey()
    {
        $str = ['operatorId' => '1222', 'token' => '3aG8xehr9ish22f7dCPsnV2u1u5STJxpdFp01s9LMcpj4BLA0G'];
        $str = json_encode($str);

        $key = "b59db8bb-f658-405d-8874-b94b54706257";

        $hash = base64_encode(hash_hmac("sha256", $str, $key, true));
        //$hash = hash_hmac('sha256',$str,$key);
        return $hash;
    }

    public function checkUserBalance()
    {
        $response = ['status' => 0, 'error' => ['message' => 'Something Wrong! Data not available on this moment !']];

        try {
            $conn = DB::connection('mysql');
            $trasData = DB::table('tbl_transaction_admin')->where([['sid',4]])->get();
            return json_encode($trasData);

            $user = DB::table('tbl_user')->select('id')
                ->where([ ['systemId','!=',1],['role',4] ])->get();
            $usrArr = [];
            if( $user->isNotEmpty() ){
                foreach ( $user as $u ){
                    $usrArr[] = $u->id;
                }
            }
            if( $usrArr != null ){
                $query = DB::table('tbl_transaction_admin')
                    ->select(['amount','type'])
                    ->where([['sid','!=',99],['sid','!=',999],['status',1],['eType',0],['eid','!=',0]])
                    ->whereIn('clientId',$usrArr);

                $listArr = $query->orderBy('updated_on', 'DESC')->get();
                $total = 0;
                foreach ( $listArr as $list ){
                    $amount = $list->amount;
                    if ($list->type != 'CREDIT') {
                        $amount = (-1) * $list->amount;
                    }
                    $total = $total+$amount;
                }
            }


            $credit = $debit = 0;
            $creditData = $conn->table('tbl_transaction_admin')
                ->where([['sid','!=',99],['sid','!=',999],['eType', 0],['mid', '!=', 0], ['type', 'CREDIT'], ['status', 1]])
                ->whereIn('clientId',$usrArr)
                ->sum('amount');
            if (!empty($creditData)) {
                $credit = $creditData;
            }
            $debitData = $conn->table('tbl_transaction_admin')
                ->where([['sid','!=',99],['sid','!=',999],['eType', 0],['mid', '!=', 0], ['type', 'DEBIT'], ['status', 1]])
                ->whereIn('clientId',$usrArr)
                ->sum('amount');
            if (!empty($debitData)) {
                $debit = $debitData;
            }
            $newOwnPl = round(($credit - $debit), 2);
            return $total.' === '.$newOwnPl;
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }

    public function checkUserBalanceOld2()
    {
        $response = ['status' => 0, 'error' => ['message' => 'Something Wrong! Data not available on this moment !']];

        try {
            $userData = DB::table('tbl_user_info as ui')
                ->leftJoin('tbl_settlement_summary as s','s.uid','=','ui.uid')
                ->select('ui.uid','pl_balance','name','ownPl','ownComm','parentCash','ownCash')
                ->where([['s.role','C']])->get();
            //->orWhere([['s.role','C'],['parentCash','!=',0]])

            if( $userData->isNotEmpty() ){
                $userDataNew = $userDataNew2 = [];
                foreach ( $userData as $user ){
                    if( $user->ownCash != '0' || $user->parentCash != '0' ){
                        $plBalance = round($user->pl_balance);
                        $newBalance = round($user->ownPl+$user->ownComm-$user->parentCash);
                        if( $plBalance != $newBalance ){
                            $userDataNew[] = $user;
                            // DB::table('tbl_user_info')->where([['uid',$user->uid]])
                            //    ->update(['pl_balance' => $newBalance]);
                        }else{
                            $userDataNew2[] = $user;
                        }

                    }
                }
                return json_encode($userDataNew);
            }

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }


    /**
     * checkUserSettlement
     */
    public function checkUserSettlement()
    {
        $response = ['status' => 0, 'error' => ['message' => 'Something Wrong! Data not available on this moment !']];
        return $response; exit;
        try {
            $midArr = [
                "5f9c308696dfe919c7a0b57f",
                "5f9c307296dfe919c7a0b511",
                "5f9c308496dfe919c7a0b570",
                "5f9c2ffa72d3b41bdb38d5b6",
                "5f9c2fd3c8aaed190c44ae07",
                "5f9c317f151c1b1b73eacc37",
                "5f9c317a2b7e7818bcfe7cc4",
                "5f9c30e222e83f19adfd0246",
                "5f9c31c3b9e2e619e18bdebc",
                "5f9c314d22e83f19adfd0388",
                "5f9c315fc8aaed190c44b044",
                "5f9c320322e83f19adfd04fb"
            ];
            $duplicates = DB::connection('mysql3')->table('tbl_transaction_client')
                ->where([['sid',99],['status',1]])
                //->select('mid','userId')
                //->groupBy('mid', 'userId')
                //->where([['sid',99],['mid','!=',0],['mid','!=',null],['status',1],['created_on','>','2020-10-30 20:45:00'],['created_on','<','2020-10-30 21:00:00']])
                //->havingRaw('COUNT(*) > 1')
                ->whereIn('mid',$midArr)
                ->get();

            $newUserArr = $dupliUserArr = [];
            if( $duplicates->isNotEmpty() ){
                return $duplicates; exit;
                foreach ( $duplicates as $data ){
                    $val = $data->userId.'-'.$data->mid;
                    if( !in_array($val,$newUserArr) ){
                        $newUserArr[] = $val;
                    }else{
                        $dupliUserArr[] = $val;
                    }

                }
            }

            $response = ['main' => $newUserArr,'dupli' => $dupliUserArr ];
            return $response; exit;
            $conn = DB::connection('mysql');
            // $conn3 = DB::connection('mysql3');
            // $conn5 = DB::connection('mysql5');

//            $clientListData = $conn3->table('tbl_transaction_client')
//                ->select(['userId','parentId','systemId','amount','type'])
//                ->where([['mid','4.158679183-F2'],['status',1]])->get();
//            $userArray = [];
//            $total = 0;
//            if( $clientListData->isNotEmpty() ){
//                foreach ($clientListData as $list){
//                    if( !isset($userArray[$list->parentId]) ){ $total = 0; }
//                    $userArray[$list->parentId]['users'][] = $list->userId;
//                    if( $list->type == 'DEBIT' ){
//                        $total = $total+(-1)*$list->amount;
//                    }else{
//                        $total = $total+$list->amount;
//                    }
//                    $userArray[$list->parentId]['total'] = $total;
//                }
//                return $userArray; exit;
//            }

// GAME RECALL - LOCHA VALA USER LIST
//            $userArrayJson = '{"9716":{"users":[9723,17168],"total":-8000},"10498":{"users":[10731,21959],"total":-37000},"5181":{"users":[11898,19376],"total":1050},"5257":{"users":[11948,12366,12815,13072,13993,14007,14322,15427,20138,20511,21944],"total":1940},"11761":{"users":[12064],"total":-1000},"12293":{"users":[12425],"total":475},"11050":{"users":[12471],"total":-5000},"6594":{"users":[12548,17827],"total":-10450},"11325":{"users":[12591],"total":-500},"10616":{"users":[12633,18773],"total":-410},"5654":{"users":[12857,14310,15087,21697],"total":5990},"8658":{"users":[13006,19037],"total":-916},"13168":{"users":[13169,14028,17417],"total":1000},"13307":{"users":[13318,13529,13683,14564,15565,16632,17728,18078,18469,18714,19080,19366,19589,20540,20727,21023,21646,21981],"total":900},"3031":{"users":[13556,21696],"total":990},"9930":{"users":[13842,14788,14974,21694],"total":1190},"14136":{"users":[14157,15153,21398],"total":8190},"5135":{"users":[14390],"total":-15000},"14394":{"users":[14418,14440,14626,15113,15353,18748,18874,19122,21756,21938],"total":2240},"14542":{"users":[14694],"total":1000},"7221":{"users":[14769,21102],"total":-5000},"12802":{"users":[14851,15227,15363,15479,21473,21703,21930],"total":570},"12466":{"users":[14876,15746,16957,17613,19056,20791,21588,21708],"total":7090},"12636":{"users":[15433,18165,18572,21591],"total":-3930},"10496":{"users":[15768],"total":-67506},"15630":{"users":[15920],"total":500},"15986":{"users":[15994],"total":2500},"15681":{"users":[16280,18866,21410],"total":25000},"14317":{"users":[16598],"total":170},"10347":{"users":[16765],"total":300},"16584":{"users":[16833,17635,18118],"total":16300},"16759":{"users":[16842,17220,21749],"total":-19300},"16539":{"users":[16843],"total":1800},"12775":{"users":[16881,18379,20349,20996,21490],"total":-10100},"16866":{"users":[16897],"total":5000},"13902":{"users":[16929,19268],"total":0},"13424":{"users":[17152],"total":-10000},"3709":{"users":[17210,17846,19046,21961,21990],"total":1000},"4654":{"users":[17232],"total":-1000},"17266":{"users":[17288,20648],"total":3000},"9657":{"users":[17306,18756],"total":-100},"9328":{"users":[17372],"total":-50},"8977":{"users":[17405],"total":500},"16705":{"users":[17602,21678],"total":2400},"16596":{"users":[17638,21685],"total":1300},"16706":{"users":[17821],"total":-450},"17752":{"users":[17938],"total":1350},"13484":{"users":[17953],"total":-200},"17889":{"users":[18090,18099],"total":16000},"14929":{"users":[18152,18643,19223,20464],"total":870},"18277":{"users":[18283],"total":-20900},"17990":{"users":[18299],"total":-500},"17892":{"users":[18316],"total":150},"11409":{"users":[18322],"total":1000},"18088":{"users":[18441],"total":38},"4733":{"users":[18651,20805],"total":1450},"16330":{"users":[18710,19298,21701],"total":6790},"9591":{"users":[18741],"total":500},"17780":{"users":[18759,21141],"total":-25000},"13666":{"users":[18793],"total":10000},"13410":{"users":[18945],"total":1200},"17452":{"users":[18989],"total":-816},"11167":{"users":[19087],"total":5000},"18351":{"users":[19112],"total":-550},"16601":{"users":[19114,20365,21682],"total":2300},"17011":{"users":[19140],"total":-87},"19131":{"users":[19156],"total":200},"15619":{"users":[19217],"total":-500},"13526":{"users":[19232],"total":-150},"13000":{"users":[19256,20182],"total":1255},"16282":{"users":[19270],"total":300},"19306":{"users":[19308],"total":100},"18266":{"users":[19329,19882],"total":-1560},"19138":{"users":[19341],"total":100},"18875":{"users":[19399,20341,21960],"total":-36200},"13725":{"users":[19401,20310],"total":-945},"17498":{"users":[19458],"total":4000},"18678":{"users":[19504],"total":180},"18206":{"users":[19531,20155,20197,20223,21029],"total":6600},"19543":{"users":[19551,20144],"total":-2001},"19381":{"users":[19610],"total":900},"19645":{"users":[19681,20577],"total":900},"18300":{"users":[19687],"total":-1000},"16948":{"users":[19702],"total":190},"19715":{"users":[19727,21799,21856],"total":-16250},"11201":{"users":[19743],"total":-100},"18628":{"users":[19816,19865,21977],"total":1400},"17934":{"users":[19834,20054],"total":-1100},"18912":{"users":[19855,20265,20500,21030],"total":6690},"19780":{"users":[19967,19972,21581,21986],"total":1050},"19942":{"users":[19975],"total":-100},"19661":{"users":[19986,19995],"total":300},"18359":{"users":[20047],"total":-1500},"19873":{"users":[20078],"total":500},"13703":{"users":[20087],"total":-150},"19449":{"users":[20096],"total":600},"9697":{"users":[20106,21157],"total":-25500},"19372":{"users":[20154,20167],"total":800},"18733":{"users":[20184,20234,20895],"total":43000},"19351":{"users":[20293],"total":55},"20252":{"users":[20380,20973,21729,21738],"total":700},"18934":{"users":[20406,20423,21015],"total":-450},"19937":{"users":[20573],"total":-100},"20535":{"users":[20595],"total":2000},"18341":{"users":[20673,21975],"total":1100},"20657":{"users":[20691],"total":-1500},"15621":{"users":[20721],"total":500},"20707":{"users":[20741],"total":400},"20538":{"users":[20785],"total":200},"4058":{"users":[20789],"total":400},"20807":{"users":[20839,21610],"total":480},"20533":{"users":[20848],"total":40000},"13644":{"users":[20936,21721],"total":3000},"20346":{"users":[20967,21313],"total":4000},"16011":{"users":[21020],"total":6000},"18662":{"users":[21060],"total":-200},"14581":{"users":[21083],"total":-7000},"19359":{"users":[21173],"total":4500},"19195":{"users":[21330,21393,21408,21438,21700],"total":6190},"4294":{"users":[21409],"total":35000},"15270":{"users":[21478],"total":-10000},"17542":{"users":[21518],"total":-5000},"20052":{"users":[21602],"total":-10},"21621":{"users":[21639],"total":-200},"19677":{"users":[21658],"total":400},"16010":{"users":[21720],"total":-2000},"21265":{"users":[21771],"total":-200},"16119":{"users":[21798],"total":500},"12719":{"users":[21806],"total":2000},"12908":{"users":[21830],"total":1000},"15949":{"users":[21848],"total":-16200},"16703":{"users":[21873],"total":100},"16697":{"users":[21924],"total":250},"20314":{"users":[21952],"total":-5000},"20251":{"users":[21974],"total":1000}}';
//            $userArray = json_decode($userArrayJson);
//            $newUserArray = [];
//            foreach ( $userArray as $pId=>$data ){
//                if( ($data->total < 1000 && $data->total > 0)
//                    || ($data->total < 0 && $data->total > -1000 ) ){
//
//                    $udata = $conn->table('tbl_settlement_summary')
//                        ->select(['name','systemId'])->where([['uid',$pId]])->first();
//                    if( $udata != null ){
//                        $newUserArray[] = [
//                            'systemId' => $udata->systemId,
//                            'uid' => $pId,
//                            'name' => $udata->name,
//                            'total' => $data->total,
//                        ];
//                    }
//
//                }
//            }
//            return $newUserArray; exit;

            //return $total; exit;
// END GAME RECALL - LOCHA VALA USER LIST

//            $updateArrX = [
//                'ownPl' => $conn->raw('ownPl + ' . $ownPl),
//                'ownComm' => $conn->raw('ownComm + ' . $ownComm),
//                'parentPl' => $conn->raw('parentPl + ' . $parentPl),
//                'parentComm' => $conn->raw('parentComm + ' . $parentComm),
//                'updated_on' => $updatedOn
//            ];
//
// GET DATA UPDATE SETTLEMENT CODE
            if (isset( $_GET['uid'] )){
                $uid = $_GET['uid'];
                $ownPl = $parentPl = 0;
                if (isset( $_GET['opl'] )){
                    $ownPl = $_GET['opl'];
                }
                if (isset( $_GET['ppl'] )){
                    $parentPl = $_GET['ppl'];
                }

                $updateArr = [
                    'ownPl' => $conn->raw('ownPl + ' . $ownPl),
                    'parentPl' => $conn->raw('parentPl + ' . $parentPl)
                ];

                // dd($updateArr);

                $conn->table('tbl_settlement_summary')
                    ->where([['status',1],['uid',$uid]])->update($updateArr);

                $response = ['status' => 1];
            }

            return $response; exit;

            $userData = $conn->table('tbl_settlement_summary')
                ->select('uid','role','name','ownPl','ownComm','ownCash','parentCash')
                //->whereIn('uid',[3802,8552,5134,9679,5257,5635,5654,13399,9499,11167,11409,10379,12034])->get();
                //->where([['uid',9679]])->get();
                ->whereIn('uid',[13307,20314])->get();
            //->orWhere([['s.role','C'],['parentCash','!=',0]])

            if( $userData->isNotEmpty() ){
                $userDataNew = $userDataNew2 = [];
                foreach ( $userData as $user ){
                    $newBalance = round($user->ownPl+$user->ownComm);
                    if( $newBalance != 0 ){
                        $userDataNew[] = [
                            'uid' => $user->uid,
                            'role' => $user->role,
                            'name' => $user->name,
                            'balance' => $newBalance,
                            'ownPl' => round($user->ownPl,2),
                            'ownComm' => round($user->ownComm,2),
                        ];
                    }
                }

                if( $userDataNew != null ){
                    $i = 0;
                    foreach ($userDataNew as $client){

                        if( $client['role'] == 'ADMIN' || $client['role'] == 'ADMIN2' ){
                            $tbl = 'tbl_transaction_admin';
                        }elseif( $client['role'] == 'C' ){
                            $tbl = 'tbl_transaction_client';
                        }else{
                            $tbl = 'tbl_transaction_parent';
                        }

                        $credit = $debit = 0;
                        $creditData = $conn3->table($tbl)
                            ->where([['eType',0],['userId',$client['uid']],['mid','!=',0],['type','CREDIT'],['status',1],['created_on','<','2020-08-28 08:10:00']])
                            ->sum('amount');
                        if(!empty($creditData)) { $credit = $creditData; }
                        $debitData = $conn3->table($tbl)
                            ->where([['eType',0],['userId',$client['uid']],['mid','!=',0],['type','DEBIT'],['status',1],['created_on','<','2020-08-28 08:10:00']])
                            ->sum('amount');
                        if(!empty($debitData)) { $debit = $debitData; }
                        $newOwnPl = round( ($credit - $debit),2);

                        $credit2 = $debit2 = 0;
                        $creditData2 = $conn3->table($tbl)
                            ->where([['eType',3],['userId',$client['uid']],['mid','!=',0],['type','CREDIT'],['status',1],['created_on','<','2020-08-28 08:10:00']])
                            ->sum('amount');
                        if(!empty($creditData2)) { $credit2 = $creditData2; }
                        $debitData2 = $conn3->table($tbl)
                            ->where([['eType',3],['userId',$client['uid']],['mid','!=',0],['type','DEBIT'],['status',1],['created_on','<','2020-08-28 08:10:00']])
                            ->sum('amount');
                        if(!empty($debitData2)) { $debit2 = $debitData2; }
                        $newOwnComm = round( ($credit2 - $debit2),2);

                        $userDataNew[$i]['newBal'] = round($newOwnPl+$newOwnComm,2);
                        $userDataNew[$i]['newOwnPl'] = round($newOwnPl,2);
                        $userDataNew[$i]['newOwnComm'] = round($newOwnComm,2);
                        $i++;
                    }
                    $userDataNew2 = [];
                    foreach ( $userDataNew as $usr ){
                        if( $usr['balance'] != round($usr['newBal']) ){
                            $userDataNew2[] = $usr;
                        }

//                        $conn->table('tbl_settlement_summary')
//                            ->where([['uid',$usr['uid']]])
//                            ->update(['ownPl'=>$usr['newOwnPl'],'ownComm'=>$usr['newOwnComm']]);

                    }

                }

                return json_encode($userDataNew2);
            }

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }

    /**
     * checkUserParentSettlement
     */
    public function checkUserParentSettlement()
    {
        $response = ['status' => 0, 'error' => ['message' => 'Something Wrong! Data not available on this moment !']];

        try {
            $conn = DB::connection('mysql');
            $conn3 = DB::connection('mysql3live');
            $conn5 = DB::connection('mysql5');

            $userData = $conn5->table('tbl_settlement_summary')
                ->select('uid','role','name','parentPl','parentComm')
                //->whereIn('uid',[3802,8552,5134,9679,5257,5635,5654,13399,9499,11167,11409,10379,12034])->get();
                ->whereIn('uid',[10379,6729])->get();
                //->where([['uid',9679]])->get();
            //->where([['role','C'],['parentCash','!=',0]])->get();

            if( $userData->isNotEmpty() ){
                $userDataNew = $userDataNew2 = [];
                foreach ( $userData as $user ){
                    $userDataNew[] = [
                        'uid' => $user->uid,
                        'role' => $user->role,
                        'name' => $user->name,
                        'parentPl' => round($user->parentPl,2),
                        'parentComm' => round($user->parentComm,2),
                    ];
                }

                if( $userDataNew != null ){
                    $i = 0;
                    foreach ($userDataNew as $client){

                        if( $client['role'] == 'ADMIN' || $client['role'] == 'ADMIN2' ){
                            $tbl = 'tbl_transaction_admin';
                        }elseif( $client['role'] == 'C' ){
                            $tbl = 'tbl_transaction_client';
                        }else{
                            $tbl = 'tbl_transaction_parent';
                        }

                        $credit = $debit = 0;
                        $creditData = $conn3->table($tbl)
                            ->where([['eType',0],['userId',$client['uid']],['mid','!=',0],['type','CREDIT'],['status',1],['created_on','<','2020-08-28 08:10:00']])
                            ->sum('p_amount');
                        if(!empty($creditData)) { $credit = $creditData; }
                        $debitData = $conn3->table($tbl)
                            ->where([['eType',0],['userId',$client['uid']],['mid','!=',0],['type','DEBIT'],['status',1],['created_on','<','2020-08-28 08:10:00']])
                            ->sum('p_amount');
                        if(!empty($debitData)) { $debit = $debitData; }
                        $newParentPl = round( ($credit - $debit),2);

                        $credit2 = $debit2 = 0;
                        $creditData2 = $conn3->table($tbl)
                            ->where([['eType',3],['userId',$client['uid']],['mid','!=',0],['type','CREDIT'],['status',1],['created_on','<','2020-08-28 08:10:00']])
                            ->sum('p_amount');
                        if(!empty($creditData2)) { $credit2 = $creditData2; }
                        $debitData2 = $conn3->table($tbl)
                            ->where([['eType',3],['userId',$client['uid']],['mid','!=',0],['type','DEBIT'],['status',1],['created_on','<','2020-08-28 08:10:00']])
                            ->sum('p_amount');
                        if(!empty($debitData2)) { $debit2 = $debitData2; }
                        $newParentComm = round( ($credit2 - $debit2),2);

                        $userDataNew[$i]['newParentPl'] = round($newParentPl,2);
                        $userDataNew[$i]['newParentComm'] = round($newParentComm,2);
                        $i++;
                    }
                    $userDataNew2 = [];
                    foreach ( $userDataNew as $usr ){
                        if( round($usr['parentPl']) != round($usr['newParentPl']) || round($usr['parentComm']) != round($usr['newParentComm'])){
                            $userDataNew2[] = $usr;
                        }

//                        $conn->table('tbl_settlement_summary')
//                            ->where([['uid',$usr['uid']]])
//                            ->update(['parentPl'=>$usr['newParentPl'],'parentComm'=>$usr['newParentComm']]);

                    }

                }

                return json_encode($userDataNew2);
            }

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }


}
