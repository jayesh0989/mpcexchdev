<?php

namespace App\Http\Controllers\AppUser;

use App\Http\Controllers\Controller;
use App\Models\ManageSystem;
use Illuminate\Http\Request;
use mysql_xdevapi\Exception;
use Illuminate\Support\Facades\DB;

class ManageSystemController extends Controller
{

    /**
     * Super Master Manage List
     */
    public function manage()
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        $data = ManageSystem::getList();

        if( $data != null ){
            $response = [ 'status' => 1, 'data' => $data ];
        }else{
            $response = [ 'status' => 1, 'data' => [] ];
        }

        return response()->json($response, 200);

    }

    /**
     * Super Master Reference List
     */
    public function reference($id)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        $data = ManageSystem::getList($id);

        if( $data != null ){
            $response = [ 'status' => 1, 'data' => $data ];
        }else{
            $response = [ 'status' => 1, 'data' => [] ];
        }

        return response()->json($response, 200);

    }

    /**
     * system Create
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:3',
            'username' => 'required|unique:tbl_user',
            'password' => 'required|min:6',
            'pl' => 'required|min:1',
            'operatorId' => 'required|min:4',
            'systemname' => 'required|unique:tbl_system',
        ]);

        $response = ManageSystem::create($request->input());
        if( $response['status'] == 1 ){
            $data['title'] = 'Create User';
            $data['description'] = 'New system ( '. $request->systemname .' ) created';
            $this->addNotification($data);
        }
        return response()->json($response, 200);
    }

    public function detail(Request $request)
    {
        $response = response()->json(['status'=>0,'message'=>'Bad request !!!','data'=>null]);
        if(isset($request->operatorId)){
            $operatorDetail = DB::table('tbl_system')->select('*')->where('operatorId',$request->operatorId)->first();
            if(!empty($operatorDetail)){
                $response = response()->json(['status'=>1,'data'=>$operatorDetail]);
            }
        }else{
            $response = response()->json(['status'=>2,'code'=>200,'message'=>'Data not found!!!','data'=>null]);
        }

        return $response;

    }

}
