<?php

namespace App\Http\Controllers\AppUser;
use App\Http\Controllers\Controller;
use App\Models\Master;
use Illuminate\Http\Request;


class MasterController extends Controller
{

    /**
     * Master Manage List
     * Action - Get
     * Created at FEB 2020 by dream
     */
    public function manage()
    {
        try{
            $data = Master::getList();

            if( $data != null ){
                $response = [ 'status' => 1, 'data' => $data ];
            }else{
                $response = [ 'status' => 1, 'data' => [] ];
            }

            return response()->json($response, 200);
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * Master Reference List
     * Action - Get
     * Created at FEB 2020 by dream
     */
    public function reference($id)
    {
        try{
            $data = Master::getList($id);
            if( $data != null ){
                $response = [ 'status' => 1, 'data' => $data ];
            }else{
                $response = [ 'status' => 1, 'data' => [] ];
            }
            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * Master Create
     * Action - Post
     * Created at FEB 2020 by dream
     */
    public function create(Request $request)
    {
        try{
            if( isset( $_SERVER['HTTP_POSTMAN_TOKEN'] ) ){
                $response = [ 'status' => 0, 'error' => [ 'message' => 'Bad request!' ] ];
                return $response; exit;
            }
            $response = Master::create($request->input());
            if( $response['status'] == 1 ) {
                $data['title'] = 'Create User';
                $data['description'] = 'New master ( ' . $request->username . ' ) created';
                $this->addNotification($data);
            }
            return response()->json($response, 200);
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

}
