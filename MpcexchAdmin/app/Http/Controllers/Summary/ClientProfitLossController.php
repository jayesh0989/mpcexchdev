<?php

namespace App\Http\Controllers\Summary;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class ClientProfitLossController extends Controller
{

    public function currentTable($tbl){
        $cTime = date('Y-m-d H:i:s');
        $y = date('Y', strtotime($cTime));
        $m = date('m', strtotime($cTime));
        return $tbl.'_'.$m.$y;
    }
    /**
     * mergeTable
     */
    public function mergeTable($requestData){

        $tbl = 'tbl_transaction_client';

        if( isset($requestData[ 'start' ]) && isset($requestData[ 'end' ]) ){
            $sd = explode('-',trim($requestData[ 'start' ]));
            $ed = explode('-',trim($requestData[ 'end' ]));
            $td = explode('-',trim(Carbon::now()->format('Y-m-d')));
            if( count($td) == 3 && count($sd) == 3 && count($ed) == 3){
                $sm = $sd[1]; $em = $ed[1]; $tm = $td[1];
                $sy = $sd[0]; $ey = $sd[0]; $ty = $td[0];
                $smy = $sm.$sy; $emy = $em.$ey; $tmy = $tm.$ty;

                if( $smy == $emy && $smy == $tmy ){
                    $tbl1 = $tbl.'_'.$tmy;
                    $tbl2 = null;
                }elseif($smy != $emy && $emy == $tmy){
                    $tbl1 = $tbl.'_'.$smy;
                    $tbl2 = $tbl.'_'.$tmy;
                }else{
                    $tbl1 = $tbl.'_'.$smy;
                    $tbl2 = $tbl.'_'.$emy;
                }
            }else{
                $tbl = $this->currentTable($tbl);
                $tbl1 = $tbl;
                $tbl2 = null;
            }
        }else{
            $tbl = $this->currentTable($tbl);
            $tbl1 = $tbl;
            $tbl2 = null;
        }

        return ['tbl1' => $tbl1,'tbl2' => $tbl2];
    }

    /**
     * action getProfitLoss
     */
    public function getProfitLoss($uid = null)
    {

        $response = ['status' => 0, 'error' => ['message' => 'Something Wrong! Data not available on this moment !']];

        try {
            $requestData = json_decode( file_get_contents('php://input') , JSON_FORCE_OBJECT );
            $userData = $userData1 = $userData2 = $userData3 = [];

            if ($uid == null) {
                $isBack = false;
                $user = Auth::user();
                $uid = $user->id;
                if ($user->role == 6) {
                    $uid = 1;
                }
            } else {
                $isBack = true;
                $user = DB::table('tbl_user')->where([['id', $uid], ['status', 1]])->first();
            }

            if ($uid == Auth::id()) {
                $isBack = false;
            }

            if ($user != null) {

                $cUserData = [
                    'cUserName' => $user->name . ' [' . $user->username . ']',
                    'pid' => $user->parentId,
                    'role' => $user->roleName,
                    'isBack' => $isBack
                ];
                $systemId = $user->systemId;
                $userDataArr = DB::table('tbl_settlement_summary')
                    ->select(['uid', 'pid', 'name', 'role'])
                    ->where([['pid', $uid], ['status', 1], ['systemId', $systemId]])
                    ->get();
                $userTotalData = [
                    'a' => 0,
                    'sm1' => 0,
                    'sm2' => 0,
                    'm1' => 0,
                    'c' => 0
                ];
                if ($userDataArr->isNotEmpty()) {
                    foreach ($userDataArr as $uData) {
                        if( $uData->role == 'SM1' || $uData->role == 'SM2' ){
                            $a = $sm1 = $sm2 = $m1 = $c = 0;
                            //if( !in_array($user->roleName,['ADMIN','ADMIN2']) ){
                                $cpl = $this->getProfitLossForClient($uData,$requestData);
                                if( $user->roleName == 'ADMIN' ){
                                    $a = isset( $cpl['a'] ) ? $cpl['a'] : $a;
                                    $userTotalData['a'] = round($userTotalData['a']+$a,2);
                                }
                                if( $user->roleName == 'ADMIN2' ){
                                    $a = isset( $cpl['a2'] ) ? $cpl['a2'] : $a;
                                    $userTotalData['a'] = round($userTotalData['a']+$a,2);
                                }

                                $sm1 = isset( $cpl['sm1'] ) ? $cpl['sm1'] : $sm1;
                                $sm2 = isset( $cpl['sm2'] ) ? $cpl['sm2'] : $sm2;
                                $m1 = isset( $cpl['m1'] ) ? $cpl['m1'] : $m1;
                                $c = isset( $cpl['c'] ) ? $cpl['c'] : $c;

                                $userTotalData['sm1'] = round($userTotalData['sm1']+$sm1,2);
                                $userTotalData['sm2'] = round($userTotalData['sm2']+$sm2,2);
                                $userTotalData['m1'] = round($userTotalData['m1']+$m1,2);
                                $userTotalData['c'] = round($userTotalData['c']+$c,2);
                            //}

                            $userData1[] = [
                                'uid' => $uData->uid,
                                'pid' => $uData->pid,
                                'name' => $uData->name,
                                'role' => $uData->role,
                                'a' => round($a,2),
                                'sm1' => round($sm1,2),
                                'sm2' => round($sm2,2),
                                'm1' => round($m1,2),
                                'c' => round($c,2)
                            ];
                        }elseif($uData->role == 'M1'){
                            $a = $sm1 = $sm2 = $m1 = $c = 0;
                            //if( !in_array($user->roleName,['ADMIN','ADMIN2']) ){
                                $cpl = $this->getProfitLossForClient($uData,$requestData);
                                if( isset( $cpl['sm2'] ) ){
                                    $sm2 = isset( $cpl['sm2'] ) ? $cpl['sm2'] : $sm2;
                                    $userTotalData['sm2'] = round($userTotalData['sm2']+$sm2,2);
                                }else{
                                    $sm1 = isset( $cpl['sm1'] ) ? $cpl['sm1'] : $sm1;
                                    $userTotalData['sm1'] = round($userTotalData['sm1']+$sm1,2);
                                }
                                $m1 = isset( $cpl['m1'] ) ? $cpl['m1'] : $m1;
                                $c = isset( $cpl['c'] ) ? $cpl['c'] : $c;

                                $userTotalData['m1'] = round($userTotalData['m1']+$m1,2);
                                $userTotalData['c'] = round($userTotalData['c']+$c,2);
                            //}
                            $userData2[] = [
                                'uid' => $uData->uid,
                                'pid' => $uData->pid,
                                'name' => $uData->name,
                                'role' => $uData->role,
                                'a' => $a,
                                'sm1' => round($sm1,2),
                                'sm2' => round($sm2,2),
                                'm1' => round($m1,2),
                                'c' => round($c,2)
                            ];
                        }elseif($uData->role == 'C'){
                            $a = $sm1 = $sm2 = $m1 = $c = 0;
                            //if( !in_array($user->roleName,['ADMIN','ADMIN2']) ){
                                $cpl = $this->getProfitLossForClient($uData,$requestData);
                                if( isset( $cpl['m1'] ) ){
                                    $m1 = isset( $cpl['m1'] ) ? $cpl['m1'] : $m1;
                                    $userTotalData['m1'] = round($userTotalData['m1']+$m1,2);
                                }else if( isset( $cpl['sm2'] ) ){
                                    $sm2 = isset( $cpl['sm2'] ) ? $cpl['sm2'] : $sm2;
                                    $userTotalData['sm2'] = round($userTotalData['sm2']+$sm2,2);
                                }else{
                                    $sm1 = isset( $cpl['sm1'] ) ? $cpl['sm1'] : $sm1;
                                    $userTotalData['sm1'] = round($userTotalData['sm1']+$sm1,2);
                                }

                                $c = isset( $cpl['c'] ) ? $cpl['c'] : $c;
                                $userTotalData['c'] = round($userTotalData['c']+$c,2);
                            //}
                            $userData3[] = [
                                'uid' => $uData->uid,
                                'pid' => $uData->pid,
                                'name' => $uData->name,
                                'role' => $uData->role,
                                'a' => $a,
                                'sm1' => round($sm1,2),
                                'sm2' => round($sm2,2),
                                'm1' => round($m1,2),
                                'c' => round($c,2)
                            ];
                        }
                    }

                    if( $userData1 != null ){
                        foreach ($userData1 as $uData1){
                            $userData[] = $uData1;
                        }
                    }
                    if( $userData2 != null ){
                        foreach ($userData2 as $uData2){
                            $userData[] = $uData2;
                        }
                    }
                    if( $userData3 != null ){
                        foreach ($userData3 as $uData3){
                            $userData[] = $uData3;
                        }
                    }
                }

                $response = ['status' => 1, 'data' => $userData, 'totalData' => $userTotalData, 'cUserData' => $cUserData];

            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    // getProfitLossForClient
    public function getProfitLossForClient($uData,$requestData)
    {

        $response = ['status' => 0, 'error' => ['message' => 'Something Wrong! Data not available on this moment !']];

        try {
            $uData = (object)$uData;
            $total = 0; $searchDate = false; $start = $end = null;
            $uid = $uData->uid;
            //$startTime = '00:00:01'; $endTime = '23:59:59';
            if( !isset( $requestData[ 'ftype' ] ) && isset( $requestData[ 'start' ] ) && isset( $requestData[ 'end' ] ) ) {
                if( strpos($requestData[ 'start' ],'T') > 0 ){
                    $requestData[ 'start' ] = str_replace('T',' ',$requestData[ 'start' ]).':00';
                }
                if( strpos($requestData[ 'end' ],'T') > 0 ){
                    $requestData[ 'end' ] = str_replace('T',' ',$requestData[ 'end' ]).':00';
                }
                $start = Carbon::parse($requestData[ 'start' ]);
                $end = Carbon::parse($requestData[ 'end' ]);
                $searchDate = true;
            }
            if( isset( $requestData[ 'ftype' ] ) ) {
                $now = Carbon::now();
                if( $requestData[ 'ftype' ] == 'week' ){
                    $end = $now->format('Y-m-d');
                    $start = $now->subDays(7)->format('Y-m-d');
                }elseif ( $requestData[ 'ftype' ] == 'month' ){
                    $end = $now->format('Y-m-d');
                    $start = $now->subDays(30)->format('Y-m-d');
                }else{
                    $end =  $now->format('Y-m-d');
                    $start = $now->subDays(1)->format('Y-m-d');
                }

                $start = Carbon::parse($start); $end = Carbon::parse($end);
                $searchDate = true;
            }

            $tbls = $this->mergeTable($requestData);
            $tbl1 = $tbls['tbl1'];
            if( strpos($tbl1,'_012021') > 0 ){
                $tbl1 = str_replace('_012021','_022021',$tbl1);
            }
            $tbl2 = $tbls['tbl2'];
            $query = DB::connection('mysql3')->table($tbl1)
                ->select(['amount','type','userId']);
            if( $uData->role != 'C' ){
                $where = [['status',1]];
                $query->where($where);
                $clientJson = DB::table('tbl_user_child_data')
                    ->select('clients')->where([['uid',$uid]])->first();
                if( $clientJson != null && isset($clientJson->clients)){
                    $clients = json_decode($clientJson->clients);
                    $query->whereIn('userId',$clients);
                }else{
                    $where = [['userId',$uid],['status',1]];
                    $query->where($where);
                }
            }else{
                $where = [['userId',$uid],['status',1]];
                $query->where($where);
            }
            $query->whereIn('eType',[0,3]);

            if( $searchDate == true && $start != null && $end != null ){
                if(isset( $requestData[ 'ftype' ] )){
                    $query->whereDate('updated_on','<=',$end->format('Y-m-d'))
                        ->whereDate('updated_on','>=',$start->format('Y-m-d'));
                }else{
                    $query->whereBetween('updated_on',[$start->format('Y-m-d H:i:s'),$end->format('Y-m-d H:i:s')]);
                }
            }
            $listArr1 = $query->orderBy('updated_on', 'DESC')->get();

            $listArr = [];
            if( $tbl2 != null ){
                $query2 = DB::connection('mysql3')->table($tbl2)
                    ->select(['amount','type','userId']);
                if( $uData->role != 'C' ){
                    $where = [['status',1]];
                    $query2->where($where);
                    $clientJson = DB::table('tbl_user_child_data')
                        ->select('clients')->where([['uid',$uid]])->first();
                    if( $clientJson != null && isset($clientJson->clients)){
                        $clients = json_decode($clientJson->clients);
                        $query2->whereIn('userId',$clients);
                    }else{
                        $where = [['userId',$uid],['status',1]];
                        $query2->where($where);
                    }
                }else{
                    $where = [['userId',$uid],['status',1]];
                    $query2->where($where);
                }
                $query2->whereIn('eType',[0,3]);

                if( $searchDate == true && $start != null && $end != null ){
                    if(isset( $requestData[ 'ftype' ] )){
                        $query2->whereDate('updated_on','<=',$end->format('Y-m-d'))
                            ->whereDate('updated_on','>=',$start->format('Y-m-d'));
                    }else{
                        $query2->whereBetween('updated_on',[$start->format('Y-m-d H:i:s'),$end->format('Y-m-d H:i:s')]);
                    }
                }
                $listArr2 = $query2->orderBy('updated_on', 'DESC')->get();

                if($listArr2->isNotEmpty() && $listArr1->isNotEmpty()){
                    if($listArr2) {
                        foreach ( $listArr2 as $data ){
                            $listArr[] = $data;
                        }
                    }
                    if($listArr1) {
                        foreach ( $listArr1 as $data ){
                            $listArr[] = $data;
                        }
                    }
                }elseif($listArr2->isNotEmpty() && $listArr1->isEmpty()){
                    $listArr = $listArr2;
                }else{
                    $listArr = $listArr1;
                }
            }else{
                $listArr = $listArr1;
            }

            $finalUserArr = $userTotal = []; $total1 = 0;
            if( !empty($listArr) ){
                foreach ( $listArr as $list ){
                    $amount = $list->amount;
                    if ($list->type != 'DEBIT') {
                        $amount = (-1) * $list->amount;
                    }

                    $total1 = $total1+$amount;

                    if( isset($userTotal[$list->userId]) ){
                        $userTotal[$list->userId] = $userTotal[$list->userId]+$amount;
                    }else{
                        $userTotal[$list->userId] = $amount;
                    }
                }
            }

            $finalUserArr['c'] = round($total1,2);
            if( $userTotal != null ){
                foreach ( $userTotal as $userId => $total){

                    $parentUserData = DB::table('tbl_user_profit_loss as pl')
                        ->select(['pl.actual_profit_loss as apl','u.roleName as roleName'])
                        ->leftjoin('tbl_user as u', 'pl.userId', '=', 'u.id')
                        ->where([['pl.clientId',$userId],['pl.actual_profit_loss','!=',0]])->orderBy('pl.userId','desc')->get();

                    foreach ( $parentUserData as $pUser ){
                        //if( !in_array($pUser->roleName,['ADMIN','ADMIN2']) ){
                            if( $total != 0 ){
                                $parentTotal = round(( $total*$pUser->apl )/100 , 2);
                            }else{
                                $parentTotal = $total;
                            }

                            if( $pUser->roleName == 'ADMIN' ){
                                $roleName = 'a';
                            }else if( $pUser->roleName == 'ADMIN2' ){
                                $roleName = 'a2';
                            }else{
                                $roleName = strtolower($pUser->roleName);
                            }
                            
                            if( isset($finalUserArr[$roleName]) ){
                                $finalUserArr[$roleName] = $finalUserArr[$roleName]+$parentTotal;
                            }else{
                                $finalUserArr[$roleName] = $parentTotal;
                            }
                        //}
                    }
                }
            }

            return $finalUserArr;

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }

}
