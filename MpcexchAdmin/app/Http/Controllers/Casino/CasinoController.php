<?php

namespace App\Http\Controllers\Casino;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class CasinoController extends Controller
{
    public function viewList()
    {
        try{
            $response = [ "status" => 0 , "code" => 400 , "message" => "Bad request!" ];
    
            $data = DB::connection('mongodb')->table('tbl_casino_games')->select('*')
            ->where('status',1)->get();

            return [ "status" => 1 ,'code'=> 200, "data" => $data];
        }
        catch (\Exception $e) {
          $response = $this->errorLog($e);
          return response()->json($response, 501);
        }
    }

    public function updateMaxStack(Request $request)
    {
        try{
            $response = [ "status" => 0 , "code" => 400 , "message" => "Bad request!" ];

            $id = $request->id;
            $maxstack = $request->maxstack;
                if(isset($id) && isset($maxstack) && !empty($id) && !empty($maxstack)){
                    $update = DB::connection('mongodb')->table('tbl_casino_games')->where([['_id',$id]])->update(['max_stack'=>$maxstack]);

                    $response = [ "status" => 1 , "code" => 200 , "message" => "max stack updated successfully." ];
                }
                return $response;
        }
        catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
            }
        }
}
