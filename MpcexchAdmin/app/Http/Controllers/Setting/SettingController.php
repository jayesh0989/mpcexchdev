<?php
namespace App\Http\Controllers\Setting;
use App\Http\Controllers\Controller;
use App\Session\EventSetting;
use App\Session\Setting;
use App\Sport;
use Illuminate\Http\Request;
use DB;
use Validator;
use Illuminate\Support\Facades\Redis;


class SettingController extends Controller
{


    /**
     * Manage
     */
    public function gameSetting(Request $request)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        $data = Setting::getGameList($request->sid);
        if( $data != null ){
            $response = [ 'status' => 1, 'data' => $data ];
        }

        return response()->json($response, 200);
    }
    public function getGameSettingData(Request $request)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        $data = Setting::getSelectedGame($request->id,$request->sid);
        if( $data != null ){
            $response = [ 'status' => 1, 'data' => $data ];
        }

        return response()->json($response, 200);
    }

    public function addNewGame(Request $request)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];
        $tbl='tbl_live_game';
        if($request->sid==7){
            $tbl='tbl_binary_game';
        }
        $data = Setting::getSelectedGame($request->id,$request->sid);
        if($data != null && $request->id !=0 ){
            $updateData = [
                'name' => $request->marketname,
                'slug' => $request->slug,
                'is_block' => $request->is_block,
                'min_stake' => $request->min_stake,
                'max_stake' => $request->max_stake,
                'max_profit_limit' => $request->max_profit_limit,
                'suspend' => $request->suspend,
                'bet_allowed' => $request->bet_allowed
            ];

            DB::connection('mysql')->table($tbl)->where('id',$request->id)->update($updateData);
            $response = [ 'status' => 1, 'success' => [ 'message' => 'Data updated successfully!' ] ];
        }else{
            if($request->sid ==7 ) {
                $insertData = [
                    'sportId' => $request->sid,
                    'name' => $request->marketname,
                    'slug' => $request->slug,
                    'mType' => $request->mtype,
                    'is_block' => $request->is_block,
                    'min_stake' => $request->min_stake,
                    'max_stake' => $request->max_stake,
                    'max_profit_limit' => $request->max_profit_limit,
                    'suspend' => $request->suspend,
                    'bet_allowed' => $request->bet_allowed,
                    'status' => 1
                ];
            }else {
                $insertData = [
                    'sportId' => $request->sid,
                    'eventId' => $request->eventId,
                    'name' => $request->marketname,
                    'slug' => $request->slug,
                    'mType' => $request->mType,
                    'is_block' => $request->is_block,
                    'min_stake' => $request->min_stake,
                    'max_stake' => $request->max_stake,
                    'max_profit_limit' => $request->max_profit_limit,
                    'suspend' => $request->suspend,
                    'bet_allowed' => $request->bet_allowed,
                    'status' => 1
                ];
            }
            DB::connection('mysql')->table($tbl)->insert($insertData);
            $response = [ 'status' => 1, 'success' => [ 'message' => 'Data added successfully!' ] ];
        }

        return response()->json($response, 200);
    }


    public function gameSettingstatus(Request $request)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];
        $tbl='tbl_live_game';
        $activity='Live Game';
        if($request->sid==7){
            $tbl='tbl_binary_game';
            $activity='Live Game';
        }
        if($request->sid==999){
            $activity='Live Game 2';
        }
        $data = Setting::getSelectedGame($request->id,$request->sid);
        if($data != null && $request->id !=0 ){
            $updateData = [
                'status' => $request->status,
            ];

            DB::connection('mysql')->table($tbl)->where('id',$request->id)->update($updateData);
            $data = Setting::getSelectedGame($request->id,$request->sid);
            if( $data->status == 1 ){
                $message = 'Active successfully!';
            }else if( $data->status == 0 ){
                $message = 'InActive successfully!';
            }else{
                $message = 'Delete successfully!';
            }

            $data1['title'] = $activity.' activity';
            $data1['description'] = $activity.': '.$message;
            $this->addNotification($data1);
            $this->activityLogCommon(json_encode($data1));
            $response = [ 'status' => 1, 'success' => [ 'message' => $message ] ];
        }

        return response()->json($response, 200);
    }

    public function addEventSetting(Request $request)
    {
    	 if(!empty($request->upcoming_min_stack) && !empty($request->upcoming_max_stack)) {

    	   $Setting = EventSetting::where('eid', $request->eid)->first();
    	 	$eid = $request->eid;

             $cache = Redis::connection();
             $eventTitle = '';
             $eventData=$cache->get("Event_".$eid);
             if(!empty($eventData)){
                 $eventData = json_decode($eventData);
                 $eventTitle =$eventData->name;
             }
    	  if(null == $Setting || $Setting == ' ' ){

            $Setting = new EventSetting();
            $Setting->eid = $request->eid;
            $Setting->upcoming_min_stake= $request->upcoming_min_stack;
            $Setting->upcoming_max_stake	=$request->upcoming_max_stack;
            $Setting->max_odd_limit=$request->max_odd;
            $Setting->min_stack= $request->min_stack;
            $Setting->max_stack=$request->max_stack;
            $Setting->max_profit_limit=$request->max_profit;
            $Setting->overall_profit_limit = $request->overall_profit;
            $Setting->bet_delay=$request->bet_delay;
            $Setting->save();

              $data['title'] = 'Event Setting Change';
              $data['description'] = $eventTitle.'('.$request->eid.') setting change';
              $this->addNotification($data);
              $data['request'] = $request->input();
              $this->activityLogCommon(json_encode($data));
          }else{


                $data['title'] = 'Event Setting Change';
                $data['description'] = $eventTitle.'('.$request->eid.') setting change';
                $this->addNotification($data);
                $data['request'] = $request->input();
                $this->activityLogCommon(json_encode($data));

            $values=array(
            	'upcoming_min_stake'=>$request->upcoming_min_stack,
            	'upcoming_max_stake'=>$request->upcoming_max_stack,
            	'max_odd_limit' => $request->max_odd,
                'overall_profit_limit' => $request->overall_profit,
                'min_stack' => $request->min_stack,
                'max_stack' => $request->max_stack,
                'max_profit_limit' => $request->max_profit,
                'bet_delay' => $request->bet_delay

               );

             EventSetting::where('eid',$request->eid)->update($values);
            

          }

             $cache = Redis::connection();
             $query = DB::table('tbl_event_limits')->select('*')
                 ->where('eid',$request->eid);
             $eventLimit = $query->first();
             if(!empty($eventLimit)){
                 $eventLimit_Data = json_encode($eventLimit);
                 $cache->set("Event_limit_".$request->eid, $eventLimit_Data);
             }
            

              return response()->json(['status' => 1, 'success' => ["message" => "Setting saved successfully!"]]);

    	 }else{
            return response()->json(['status' => 400, 'error' => ["message" => 'Bad request!']]);
        }
    }

    public function getsportMarketSetting(Request $request)
    {
        $SettingData=null;
        $Upcoming_min_stake=$Upcoming_max_stake=$Upcoming_max_profit=$Max_odd_limit=$accept_unmatch_bet=0;
        $query = DB::connection('mysql')->table('tbl_common_setting')
            ->select(['id', 'key_name', 'value', 'status'])->where('key_name', 'UPCOMING_EVENT_SETTING');
        $list = $query->first();
        if(!empty($list)) {
            $Setting = json_decode($list->value);
            $Upcoming_min_stake=$Setting->Upcoming_min_stake;
            $Upcoming_max_stake=$Setting->Upcoming_max_stake;
            $Upcoming_max_profit=$Setting->Upcoming_max_profit;
            $Max_odd_limit=$Setting->Max_odd_limit;
            $accept_unmatch_bet=$Setting->Accept_unmatch_bet;

        }
        if($request->sid==1){
            $query = DB::connection('mysql')->table('tbl_common_setting')
                ->select(['id', 'key_name', 'value', 'status'])->where('key_name', 'FOOTBALL_EVENT_SETTING');
            $list = $query->first();
            if(!empty($list)) {
                $Setting = json_decode($list->value);
                $SettingData=array('min_stake'=>$Setting->Min_stack,
                    'max_stake'=>$Setting->Max_stack,
                    'max_profit'=>0,
                    'max_profit_limit'=>$Setting->Max_profit_all_limit,
                    'bet_delay'=>$Setting->Bet_delay,
                    'max_profit_all_limit'=>$Setting->Max_profit_all_limit,
                    'max_odd_limit'=>$Max_odd_limit,
                    'upcoming_max_profit'=>$Upcoming_max_profit,
                    'upcoming_max_stake'=>$Upcoming_max_stake,
                    'upcoming_min_stake'=>$Upcoming_min_stake,
                    'accept_unmatch_bet'=>$accept_unmatch_bet
                );
            }
        }elseif($request->sid==2){
            $query = DB::connection('mysql')->table('tbl_common_setting')
                ->select(['id', 'key_name', 'value', 'status'])->where('key_name', 'TENNIS_EVENT_SETTING');
            $list = $query->first();
            if(!empty($list)) {
                $Setting = json_decode($list->value);
                $SettingData=array('min_stake'=>$Setting->Min_stack,
                    'max_stake'=>$Setting->Max_stack,
                    'max_profit'=>0,
                    'max_profit_limit'=>$Setting->Max_profit_all_limit,
                    'bet_delay'=>$Setting->Bet_delay,
                    'max_profit_all_limit'=>$Setting->Max_profit_all_limit,
                    'max_odd_limit'=>$Max_odd_limit,
                    'upcoming_max_profit'=>$Upcoming_max_profit,
                    'upcoming_max_stake'=>$Upcoming_max_stake,
                    'upcoming_min_stake'=>$Upcoming_min_stake,
                    'accept_unmatch_bet'=>$accept_unmatch_bet
                );
            }
        }else{
            $query = DB::connection('mysql')->table('tbl_common_setting')
                ->select(['id', 'key_name', 'value', 'status'])->where('key_name', 'CRICKET_EVENT_SETTING');
            $list = $query->first();
            if(!empty($list)) {
                $Setting = json_decode($list->value);
                $SettingData=array('min_stake'=>$Setting->Min_stack,
                    'max_stake'=>$Setting->Max_stack,
                    'max_profit'=>0,
                    'max_profit_limit'=>$Setting->Max_profit_all_limit,
                    'bet_delay'=>$Setting->Bet_delay,
                    'max_profit_all_limit'=>$Setting->Max_profit_all_limit,
                    'max_odd_limit'=>$Max_odd_limit,
                    'upcoming_max_profit'=>$Upcoming_max_profit,
                    'upcoming_max_stake'=>$Upcoming_max_stake,
                    'upcoming_min_stake'=>$Upcoming_min_stake,
                    'accept_unmatch_bet'=>$accept_unmatch_bet
                );
            }
        }


        if (!empty($Setting)) {
            return response()->json(['status' => 1,'data'=> $SettingData,'message' => ' Data Found']);
        } else {
            return response()->json(['status' => 0,'data'=> null, 'message' => 'Data Not Found']);
        }

    }



    public function createnewtable(Request $request)
    {
        if(!empty($request->month) && !empty($request->year)) {
            $updated_on = date('Y-m-d H:i:s');
            $table_name="tbl_transaction_admin_".$request->month.$request->year;

            $query = DB::connection('mysql3')->table('tbl_account_table')->select('*')->where('table_name', $table_name);
            $list = $query->first();
            if(empty($list)) {
                $addData = ['table_name' => $table_name, 'table_type' => 'Admin','month'=> $request->month ,'year'=> $request->year, 'created_on' => $updated_on, 'status' => 1];
                DB::connection('mysql3')->table('tbl_account_table')->insert($addData);

                DB::connection('mysql3')->statement("CREATE TABLE $table_name (
                  `id` int(11) NOT NULL,
                  `systemId` int(11) NOT NULL DEFAULT '1',
                  `clientId` int(11) NOT NULL DEFAULT '0',
                  `userId` int(11) NOT NULL DEFAULT '0',
                  `childId` int(11) NOT NULL DEFAULT '0',
                  `parentId` int(11) NOT NULL DEFAULT '0',
                  `sid` int(11) NOT NULL DEFAULT '0',
                  `eid` int(11) NOT NULL DEFAULT '0',
                  `mid` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                  `result` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                  `round` int(11) NOT NULL DEFAULT '0',
                  `eType` smallint(6) NOT NULL COMMENT '0:default,1:chip,2:cash,3:commission,4:settled',
                  `mType` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'market type',
                  `type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'CREDIT,DEBIT',
                  `amount` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
                  `p_amount` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '0',
                  `c_amount` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '0',
                  `balance` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
                  `current_balance` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
                  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
                  `remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                  `status` smallint(6) NOT NULL DEFAULT '1'
                );");


                $table_name = "tbl_transaction_parent_" . $request->month . $request->year;
                $addData = ['table_name' => $table_name, 'table_type' => 'Parent','month'=>$request->month ,'year'=>$request->year, 'created_on' => $updated_on, 'status' => 1];
                DB::connection('mysql3')->table('tbl_account_table')->insert($addData);

                DB::connection('mysql3')->statement("CREATE TABLE $table_name (
                  `id` int(11) NOT NULL,
                  `systemId` int(11) NOT NULL DEFAULT '1',
                  `clientId` int(11) NOT NULL DEFAULT '0',
                  `userId` int(11) NOT NULL DEFAULT '0',
                  `childId` int(11) NOT NULL DEFAULT '0',
                  `parentId` int(11) NOT NULL DEFAULT '0',
                  `sid` int(11) NOT NULL DEFAULT '0',
                  `eid` int(11) NOT NULL DEFAULT '0',
                  `mid` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                  `result` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                  `round` int(11) NOT NULL DEFAULT '0',
                  `eType` smallint(6) NOT NULL COMMENT '0:default,1:chip,2:cash,3:commission,4:settled',
                  `mType` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'market type',
                  `type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'CREDIT,DEBIT',
                  `amount` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
                  `p_amount` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '0',
                  `c_amount` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '0',
                  `balance` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
                  `current_balance` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
                  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
                  `remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                  `status` smallint(6) NOT NULL DEFAULT '1'
                );");



                $table_name = "tbl_transaction_client_" . $request->month . $request->year;
                $addData = ['table_name' => $table_name, 'table_type' => 'Client','month'=>$request->month ,'year'=>$request->year,'created_on' => $updated_on, 'status' => 1];
                DB::connection('mysql3')->table('tbl_account_table')->insert($addData);

                DB::connection('mysql3')->statement("CREATE TABLE $table_name (
                  `id` int(11) NOT NULL,
                  `systemId` int(11) NOT NULL DEFAULT '1',
                  `clientId` int(11) NOT NULL DEFAULT '0',
                  `userId` int(11) NOT NULL DEFAULT '0',
                  `childId` int(11) NOT NULL DEFAULT '0',
                  `parentId` int(11) NOT NULL DEFAULT '0',
                  `sid` int(11) NOT NULL DEFAULT '0',
                  `eid` int(11) NOT NULL DEFAULT '0',
                  `mid` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                  `round` int(11) NOT NULL DEFAULT '0',
                  `result` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                  `eType` smallint(6) NOT NULL COMMENT '0:default,1:chip,2:cash,3:commission,4:settled',
                  `mType` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'market type',
                  `type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'CREDIT,DEBIT',
                  `amount` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
                  `p_amount` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '0',
                  `c_amount` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '0',
                  `balance` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
                  `current_balance` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
                  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
                  `remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                  `status` smallint(6) NOT NULL DEFAULT '1'
                );");
                return response()->json(['status' => 1, 'success' => ["message" => "table saved successfully!"]]);
            }else{
                return response()->json(['status' => 1, 'success' => ["message" => "This table already Exists."]]);
            }
        }else{
            return response()->json(['status' => 400, 'error' => ["message" => 'Bad request!']]);
        }
    }


    public function gettableList(Request $request)
    {
            $query = DB::connection('mysql3')->table('tbl_account_table')->select('*')->whereIn('status', [1,2]);
            $list = $query->orderBy('id', 'DESC')->get();
            if($list != null){
                return response()->json(['status' => 1,'data'=>$list]);
            } else {
                return response()->json(['status' => 0, 'error' => ["message" => 'failed , please try again or contact admin!']]);
            }

    }

    public function getEventSetting(Request $request)
    {
        $cache = Redis::connection();

        $Setting = $cache->get("Admin_Event_limit_".$request->eid);
        if(empty($Setting)){
        $Setting = $cache->get("Event_limit_".$request->eid);
        }
        $Setting = json_decode($Setting);
        if (empty($Setting)) {
            $SettingData=null;
            $Upcoming_min_stake=$Upcoming_max_stake=$Upcoming_max_profit=$Max_odd_limit=$accept_unmatch_bet=0;
            $query = DB::connection('mysql')->table('tbl_common_setting')
                ->select(['id', 'key_name', 'value', 'status'])->where('key_name', 'UPCOMING_EVENT_SETTING');
            $list = $query->first();
            if(!empty($list)) {
                $Setting = json_decode($list->value);
                $Upcoming_min_stake=$Setting->Upcoming_min_stake;
                $Upcoming_max_stake=$Setting->Upcoming_max_stake;
                $Upcoming_max_profit=$Setting->Upcoming_max_profit;
                $Max_odd_limit=$Setting->Max_odd_limit;
                $accept_unmatch_bet=$Setting->Accept_unmatch_bet;

            }
            if($request->sid==1){
                $query = DB::connection('mysql')->table('tbl_common_setting')
                    ->select(['id', 'key_name', 'value', 'status'])->where('key_name', 'FOOTBALL_EVENT_SETTING');
                $list = $query->first();
                if(!empty($list)) {
                    $Setting = json_decode($list->value);
                    $SettingData=array('min_stake'=>$Setting->Min_stack,
                        'max_stake'=>$Setting->Max_stack,
                        'max_profit'=>$Setting->Max_profit_all_limit,
                        'max_profit_limit'=>$Setting->Max_profit_all_limit,
                        'bet_delay'=>$Setting->Bet_delay,
                        'max_profit_all_limit'=>$Setting->Event_Max_profit_all_limit,
                        'max_odd_limit'=>$Max_odd_limit,
                        'upcoming_max_profit'=>$Upcoming_max_profit,
                        'upcoming_max_stake'=>$Upcoming_max_stake,
                        'upcoming_min_stake'=>$Upcoming_min_stake,
                        'accept_unmatch_bet'=>$accept_unmatch_bet
                    );
                }
            }elseif($request->sid==2){
                $query = DB::connection('mysql')->table('tbl_common_setting')
                    ->select(['id', 'key_name', 'value', 'status'])->where('key_name', 'TENNIS_EVENT_SETTING');
                $list = $query->first();
                if(!empty($list)) {
                    $Setting = json_decode($list->value);
                    $SettingData=array('min_stake'=>$Setting->Min_stack,
                        'max_stake'=>$Setting->Max_stack,
                        'max_profit'=>$Setting->Max_profit_all_limit,
                        'max_profit_limit'=>$Setting->Max_profit_all_limit,
                        'bet_delay'=>$Setting->Bet_delay,
                        'max_profit_all_limit'=>$Setting->Event_Max_profit_all_limit,
                        'max_odd_limit'=>$Max_odd_limit,
                        'upcoming_max_profit'=>$Upcoming_max_profit,
                        'upcoming_max_stake'=>$Upcoming_max_stake,
                        'upcoming_min_stake'=>$Upcoming_min_stake,
                        'accept_unmatch_bet'=>$accept_unmatch_bet
                    );
                }
            }elseif($request->sid==11){
                $query = DB::connection('mysql')->table('tbl_common_setting')
                    ->select(['id', 'key_name', 'value', 'status'])->where('key_name', 'CRICKET_CASINO');
                $list = $query->first();
                if(!empty($list)) {
                    $Setting = json_decode($list->value);
                    $SettingData=array('min_stake'=>$Setting->Min_stack,
                        'max_stake'=>$Setting->Max_stack,
                        'max_profit'=>$Setting->Max_profit_limit,
                        'max_profit_limit'=>$Setting->Max_profit_limit,
                        'bet_delay'=>$Setting->Bet_delay,
                        'max_profit_all_limit'=>$Setting->Event_Max_profit_all_limit,
                        'max_odd_limit'=>$Max_odd_limit,
                        'upcoming_max_profit'=>$Upcoming_max_profit,
                        'upcoming_max_stake'=>$Upcoming_max_stake,
                        'upcoming_min_stake'=>$Upcoming_min_stake,
                        'accept_unmatch_bet'=>$accept_unmatch_bet
                    );
                }
            }elseif($request->sid==10){
                $query = DB::connection('mysql')->table('tbl_common_setting')
                    ->select(['id', 'key_name', 'value', 'status'])->where('key_name', 'DEFAULT_JACKPOT_SETTING');
                $list = $query->first();
                if(!empty($list)) {
                    $Setting = json_decode($list->value);
                    $SettingData=array('min_stake'=>$Setting->Min_stack,
                        'max_stake'=>$Setting->Max_stack,
                        'max_profit'=>$Setting->Max_profit_limit,
                        'max_profit_limit'=>$Setting->Max_profit_limit,
                        'bet_delay'=>$Setting->Bet_delay,
                        'max_profit_all_limit'=>$Setting->Event_Max_profit_all_limit,
                        'max_odd_limit'=>$Max_odd_limit,
                        'upcoming_max_profit'=>$Upcoming_max_profit,
                        'upcoming_max_stake'=>$Upcoming_max_stake,
                        'upcoming_min_stake'=>$Upcoming_min_stake,
                        'accept_unmatch_bet'=>$accept_unmatch_bet
                    );
                }
            }else{
                $query = DB::connection('mysql')->table('tbl_common_setting')
                    ->select(['id', 'key_name', 'value', 'status'])->where('key_name', 'CRICKET_EVENT_SETTING');
                $list = $query->first();
                if(!empty($list)) {
                    $Setting = json_decode($list->value);
                    $SettingData=array('min_stack'=>$Setting->Min_stack,
                        'max_stack'=>$Setting->Max_stack,
                        'max_profit'=>$Setting->Max_profit_all_limit,
                        'max_profit_limit'=>$Setting->Max_profit_all_limit,
                        'bet_delay'=>$Setting->Bet_delay,
                        'overall_profit_limit'=>$Setting->Event_Max_profit_all_limit,
                        'max_odd_limit'=>$Max_odd_limit,
                        'upcoming_max_profit'=>$Upcoming_max_profit,
                        'upcoming_max_stake'=>$Upcoming_max_stake,
                        'upcoming_min_stake'=>$Upcoming_min_stake,
                        'accept_unmatch_bet'=>$accept_unmatch_bet
                    );
                }
            }
            $Setting=   $SettingData;
        }

         if (!empty($Setting)) {
             return response()->json(['status' => 1,'data'=> $Setting,'message' => ' Data Found']);
         } else {
             return response()->json(['status' => 0, 'error' => ["message" => 'failed , please try again!']]);
         }
         
    }
    
    public function updateEventSetting(Request $request){
        try {
                $validate = Validator::make($request->all(),[   'eid'               =>'required|max:11',
                                                                'max_odd_limit'     =>'required|max:11',
                                                                // 'max_profit_limit'  =>'required|max:11',
                                                                // 'max_stack'         =>'required|max:11',
                                                                // 'min_stack'         =>'required|max:11',
                                                                // 'bet_delay'         =>'required|max:6',
                                                                'overall_profit_limit'    =>'required|max:11',
                                                                'upcoming_max_stake'=>'required|max:11',
                                                                'upcoming_min_stake'=>'required|max:11',
                                                            ]);
                if($validate->fails()){
                    return response()->json(['status' => 1, 'success' => ["message" => $validate->errors()]]);
                }

                $data['eid']                    = $request->get('eid');
                $data['max_odd_limit']          = (int)$request->get('max_odd_limit');
                $data['max_profit_limit']       = $request->get('max_profit_limit',0);
                $data['max_stack']              = (int)$request->get('max_stack',0);
                $data['min_stack']              = (int)$request->get('min_stack',0);
                $data['overall_profit_limit']   = (int)$request->get('overall_profit_limit');
                $sid                    = $request->get('sid',null);
                $data['upcoming_max_stake']     = (int)$request->get('upcoming_max_stake');
                $data['upcoming_min_stake']     = (int)$request->get('upcoming_min_stake');
                $data['bet_delay']              = (int)$request->get('bet_delay',0);

                $isExist = DB::connection('mysql')->table('tbl_event_limits')->where('eid',$data['eid'])->count();
                if($isExist > 0){
                    $result = DB::connection('mysql')->table('tbl_event_limits')->where('eid',$data['eid'])->update($data);
                }else{
                    $result = DB::connection('mysql')->table('tbl_event_limits')->insert($data);
                }
                $cache = Redis::connection();
                $cache->set("Admin_Event_limit_".$data['eid'], json_encode($data));
                return response()->json(['status' => 1, 'success' => ["message" => "Event settings updated successfully."]]);
        } catch (\Exception $e) {

                return response()->json(['status' => 0, 'error' => ["message" => $e->getMessage()]]);
        }
    }

    public function getMarketSetting(Request $request)
    {
        $cache = Redis::connection();
        $Setting = $cache->get("Admin_Market_".$request->mid);

        if(empty($Setting)){
          $Setting = $cache->get("Market_".$request->mid);
        }
        $Setting = json_decode($Setting);
        unset($Setting->id);
        unset($Setting->marketId);
        unset($Setting->over);
        unset($Setting->ball);
        unset($Setting->editOn);
        unset($Setting->secId);
        unset($Setting->info);
        unset($Setting->game_over);
        unset($Setting->result);
        unset($Setting->bet_allowed);
        unset($Setting->suspended);
        unset($Setting->ball_running);
        unset($Setting->status);
        unset($Setting->created_on);
        unset($Setting->updated_on);
         if (!empty($Setting)) {
             return response()->json(['status' => 1,'data'=> $Setting,'message' => ' Data Found']);
         } else {
             return response()->json(['status' => 0,'data'=> null, 'message' => 'Data Not Found']);
         }
         
     }

    public function updateMarketSetting(Request $request){
        try {
                $validate = Validator::make($request->all(),[   'eid'               =>'required|max:11',
                                                                'mid'               =>'required',
                                                                'max_odd_limit'     =>'required|max:11',
                                                                'max_profit_limit'  =>'required|max:11',
                                                                'max_stack'         =>'required|max:11',
                                                                'min_stack'         =>'required|max:11',
                                                                'bet_delay'         =>'required|max:6'
                                                            ]);
                if($validate->fails()){
                    return response()->json(['status' => 1, 'success' => ["message" => $validate->errors()]]);
                }

                $data['eid']                    = $request->get('eid');
                $data['mid']                    = $request->get('mid');
                $data['max_odd_limit']          = (int)$request->get('max_odd_limit');
                $data['max_profit_limit']       = $request->get('max_profit_limit');
                $data['max_stack']              = (int)$request->get('max_stack');
                $data['min_stack']              = (int)$request->get('min_stack');
                $data['bet_delay']              = (int)$request->get('bet_delay');

                $isExist = DB::connection('mysql')->table('tbl_market_limits')->where('mid',$data['mid'])->count();
                if($isExist > 0){
                    $result = DB::connection('mysql')->table('tbl_market_limits')->where('mid',$data['mid'])->update($data);
                }else{
                    $result = DB::connection('mysql')->table('tbl_market_limits')->insert($data);
                }
                $cache = Redis::connection();
                $cache->set("Admin_Market_".$data['mid'], json_encode($data));
                return response()->json(['status' => 1, 'success' => ["message" => "Market settings updated successfully."]]);
        } catch (\Exception $e) {
                return response()->json(['status' => 0, 'error' => ["message" => 'Failed to update.Please try again...']]);
        }
    }


}
