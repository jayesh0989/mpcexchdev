<?php

namespace App\Http\Controllers\Dashboard;
use App\Http\Controllers\Controller;
use App\Models\CommonModel;
use App\Models\Sport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;


class BookController extends Controller
{

    /**
     * action User Book Data MatchOdd
     * Action - Post
     * Created at DEC 2020
     */
    public function actionUserBookDataMatchOdd(Request $request)
    {

        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];

        try{
            $conn = DB::connection('mongodb');
            if( isset( $request->marketId ) ){
                $titleData = $userData = $totalData = [];
                if( isset( $request->userId ) ){
                    $uid = $request->userId;
                    $user = DB::table('tbl_user')->select(['id','role','systemId'])->where([['id',$uid],['status',1]])->first();
                }else{
                    $user = Auth::user(); $uid = $user->id;
                    if( $user->role == 6 ){ $uid = 1; }
                }
                $marketId = $request->marketId;
                $systemId = $user->systemId;

                $redis = Redis::connection();
                $marketDataJson = $redis->get('Market_'.$marketId);
                $marketData = json_decode($marketDataJson);

                if( $marketData != null && isset($marketData->runners)
                    && isset($marketData->slug) && $marketData->slug != 'winner'
                    && isset($marketData->slug) && $marketData->slug != 'goals'
                    && isset($marketData->slug) && $marketData->slug != 'set_market' ){
                    $runnerData = json_decode($marketData->runners);
                    $runners = [];
                    if( $runnerData != null ){
                        foreach ( $runnerData as $runner ){
                            $runners[] = [
                                'secId' => $runner->secId,
                                'name' => $runner->runner
                            ];
                        }
                    }

                    if( $runners != null ) {
                        $runnerName1 = $runnerName2 = $runnerName3 = '';
                        $titleData['0'] = 'User Name';
                        if (isset($runners[0]['name'])) {
                            $runnerName1 = $runners[0]['name'];
                            $titleData['1'] = $runnerName1;
                        }
                        if (isset($runners[1]['name'])) {
                            $runnerName2 = $runners[1]['name'];
                            $titleData['2'] = $runnerName2;
                        }
                        if (isset($runners[2]['name'])) {
                            $runnerName3 = $runners[2]['name'];
                            $titleData['3'] = $runnerName3;
                        }

                    }

                    $query = $conn->table('tbl_bet_pending_1')
                        ->where([['systemId',$systemId],['mid',$marketId],['status',1],['result','PENDING']]);

                    $clientArr = $query->distinct()->select('uid')->get();

                    if( $clientArr->isNotEmpty() ){

                        $AllClients = [];
                        foreach ( $clientArr as $cuid ){
                            $AllClients[] = $cuid;
                        }

                        $AllClients = array_unique($AllClients);

                        $client=[];
                        $cdata = DB::table('tbl_user_profit_loss')->select(['userId'])
                            ->where([['parentId',$uid]])->whereIn('clientId',$AllClients)
                            ->distinct()->select('userId')->get();

                        if ($cdata != null) {
                            foreach ($cdata as $c) {
                                $client[] = (int)$c->userId;
                            }
                        }

                    }

                    if(!empty( $client )){
                        $userList = DB::table('tbl_user')->select(['id','parentId','username','role','systemId'])
                            ->whereIn('id',$client)->where([['status',1],['systemId',$systemId]])->get();

                        if( $userList->isNotEmpty() && $runners != null ){
                            $runner1 = $runner2 = $runner3 = 0;

                            $t1 = $t2 = $t3 = null;
                            $i = 0;
                            foreach ( $userList as $user ){

                                $userData[$i] = [
                                    'username' => $user->username,
                                    'id' => $user->id,
                                    'role' => $user->role
                                ];

                                if( isset( $runners[0]['secId'] ) ){
                                    $runner1 = $this->getProfitLossMatchOddForBook($user,$marketId,$runners[0]['secId']);
                                    $t1[] = round($runner1,2);
                                    $userData[$i]['runner1'] = round($runner1);
                                }
                                if( isset( $runners[1]['secId'] ) ){
                                    $runner2 = $this->getProfitLossMatchOddForBook($user,$marketId,$runners[1]['secId']);
                                    $t2[] = round($runner2,2);
                                    $userData[$i]['runner2'] = round($runner2);
                                }
                                if( isset( $runners[2]['secId'] ) ){
                                    $runner3 = $this->getProfitLossMatchOddForBook($user,$marketId,$runners[2]['secId']);
                                    $t3[] = round($runner3,2);
                                    $userData[$i]['runner3'] = round($runner3);
                                }

                                $i++;
                            }

                        }

                    }

                }else{
                    if( $marketData->slug == 'winner' ){
                        $tbl = 'tbl_bet_pending_1';
                    }else{
                        $tbl = 'tbl_bet_pending_6';
                    }
                    $query = $conn->table($tbl)
                        ->where([['systemId',$systemId],['mid',$marketId],['status',1],['result','PENDING']]);

                    $clientArr = $query->distinct()->select('uid')->get();

                    if( $clientArr->isNotEmpty() ){

                        $AllClients = [];
                        foreach ( $clientArr as $cuid ){
                            $AllClients[] = $cuid;
                        }

                        $AllClients = array_unique($AllClients);

                        $client=[];
                        $cdata = DB::table('tbl_user_profit_loss')->select(['userId'])
                            ->where([['parentId',$uid]])->whereIn('clientId',$AllClients)
                            ->distinct()->select('userId')->get();

                        if ($cdata != null) {
                            foreach ($cdata as $c) {
                                $client[] = (int)$c->userId;
                            }
                        }

                    }

                    if(!empty( $client )){
                        $userList = DB::table('tbl_user')->select(['id','parentId','username','role','systemId'])
                            ->whereIn('id',$client)->where([['status',1],['systemId',$systemId]])->get();

                        if( $userList->isNotEmpty() ){
                            $i = 0;
                            foreach ( $userList as $user ){
                                $userData[$i] = [
                                    'username' => $user->username,
                                    'id' => $user->id,
                                    'role' => $user->role
                                ];
                                $i++;
                            }
                        }

                    }
                }

                $response = [ "status" => 1 , "data" => [ 'titleData' => $titleData , 'userData' => $userData , 'totalData' => $totalData ] ];

            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * action System Book Data MatchOdd
     * Action - Post
     * Created at DEC 2020
     */
    public function actionSystemBookDataMatchOdd(Request $request)
    {

        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];

        try{
            $conn = DB::connection('mongodb');
            if( isset( $request->marketId ) ){
                $titleData = $userData = $totalData = [];
                $user = Auth::user(); $uid = $user->id;
                if( $user->role == 6 ){ $uid = 1; }
                $marketId = $request->marketId;

                $redis = Redis::connection();
                $marketDataJson = $redis->get('Market_'.$marketId);
                $marketData = json_decode($marketDataJson);

                if( $marketData != null && isset($marketData->runners)
                    && isset($marketData->slug) && $marketData->slug != 'winner'
                    && isset($marketData->slug) && $marketData->slug != 'goals'
                    && isset($marketData->slug) && $marketData->slug != 'set_market' ){
                    $runnerData = json_decode($marketData->runners);
                    $runners = [];
                    if( $runnerData != null ){
                        foreach ( $runnerData as $runner ){
                            $runners[] = [
                                'secId' => $runner->secId,
                                'name' => $runner->runner
                            ];
                        }
                    }

                    if( $runners != null ) {
                        $runnerName1 = $runnerName2 = $runnerName3 = '';
                        $titleData['0'] = 'User Name';
                        if (isset($runners[0]['name'])) {
                            $runnerName1 = $runners[0]['name'];
                            $titleData['1'] = $runnerName1;
                        }
                        if (isset($runners[1]['name'])) {
                            $runnerName2 = $runners[1]['name'];
                            $titleData['2'] = $runnerName2;
                        }
                        if (isset($runners[2]['name'])) {
                            $runnerName3 = $runners[2]['name'];
                            $titleData['3'] = $runnerName3;
                        }

                    }

                    $userList = DB::table('tbl_user')->select(['id','parentId','username','role','systemId'])
                        ->where([['parentId',$uid],['status',1],['systemId','!=',1],['role',1]])->get();

                    if( $userList->isNotEmpty() && $runners != null ){
                        $runner1 = $runner2 = $runner3 = 0;

                        $t1 = $t2 = $t3 = null;
                        $i = 0;
                        foreach ( $userList as $user ){

                            $userData[$i] = [
                                'username' => $user->username,
                                'id' => $user->id,
                                'role' => $user->role
                            ];

                            if( isset( $runners[0]['secId'] ) ){
                                $runner1 = $this->getProfitLossMatchOddForBook($user,$marketId,$runners[0]['secId']);
                                $t1[] = round($runner1,2);
                                $userData[$i]['runner1'] = round($runner1);
                            }
                            if( isset( $runners[1]['secId'] ) ){
                                $runner2 = $this->getProfitLossMatchOddForBook($user,$marketId,$runners[1]['secId']);
                                $t2[] = round($runner2,2);
                                $userData[$i]['runner2'] = round($runner2);
                            }
                            if( isset( $runners[2]['secId'] ) ){
                                $runner3 = $this->getProfitLossMatchOddForBook($user,$marketId,$runners[2]['secId']);
                                $t3[] = round($runner3,2);
                                $userData[$i]['runner3'] = round($runner3);
                            }

                            $i++;
                        }

                    }

                }else{
                    if( $marketData->slug == 'winner' ){
                        $tbl = 'tbl_bet_pending_1';
                    }else{
                        $tbl = 'tbl_bet_pending_6';
                    }
                    $query = $conn->table($tbl)
                        ->where([['systemId','!=',1],['mid',$marketId],['status',1],['result','PENDING']]);

                    $clientArr = $query->distinct()->select('uid')->get();

                    if( $clientArr->isNotEmpty() ){

                        $AllClients = [];
                        foreach ( $clientArr as $uid ){
                            $AllClients[] = $uid;
                        }

                        $AllClients = array_unique($AllClients);

                        $client=[];
                        $cdata = DB::table('tbl_user_profit_loss')->select(['userId'])
                            ->where([['parentId',$uid]])->whereIn('clientId',$AllClients)
                            ->distinct()->select('userId')->get();

                        if ($cdata != null) {
                            foreach ($cdata as $c) {
                                $client[] = (int)$c->userId;
                            }
                        }

                    }

                    if(!empty( $client )){
                        $userList = DB::table('tbl_user')->select(['id','parentId','username','role','systemId'])
                            ->whereIn('id',$client)->where([['status',1],['systemId','!=',1]])->get();

                        if( $userList->isNotEmpty() ){
                            $i = 0;
                            foreach ( $userList as $user ){
                                $userData[$i] = [
                                    'username' => $user->username,
                                    'id' => $user->id,
                                    'role' => $user->role
                                ];
                                $i++;
                            }
                        }
                    }
                }

                $response = [ "status" => 1 , "data" => [ 'titleData' => $titleData , 'userData' => $userData , 'totalData' => $totalData ] ];

            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * action User Book Data BookMaker
     * Action - Post
     * Created at DEC 2020
     */
    public function actionUserBookDataBookMaker(Request $request)
    {

        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];

        try{
            $conn = DB::connection('mongodb');
            if( isset( $request->marketId ) ){
                $titleData = $userData = $totalData = [];
                if( isset( $request->userId ) ){
                    $uid = $request->userId;
                    $user = DB::table('tbl_user')->where([['id',$uid],['status',1]])->first();
                }else{
                    $user = Auth::user(); $uid = $user->id;
                    if( $user->role == 6 ){ $uid = 1; }
                }
                $marketId = $request->marketId;
                $systemId = $user->systemId;

                $redis = Redis::connection();
                $marketDataJson = $redis->get('Market_'.$marketId);
                $marketData = json_decode($marketDataJson);

                if( $marketData != null && isset($marketData->runners) ){
                    $runnerData = json_decode($marketData->runners);
                    $runners = [];
                    if( $runnerData != null ){
                        foreach ( $runnerData as $runner ){
                            $runners[] = [
                                'secId' => $runner->secId,
                                'name' => $runner->runner
                            ];
                        }
                    }

                    if( $runners != null ) {
                        $runnerName1 = $runnerName2 = $runnerName3 = '';
                        $titleData['0'] = 'User Name';
                        if (isset($runners[0]['name'])) {
                            $runnerName1 = $runners[0]['name'];
                            $titleData['1'] = $runnerName1;
                        }
                        if (isset($runners[1]['name'])) {
                            $runnerName2 = $runners[1]['name'];
                            $titleData['2'] = $runnerName2;
                        }
                        if (isset($runners[2]['name'])) {
                            $runnerName3 = $runners[2]['name'];
                            $titleData['3'] = $runnerName3;
                        }

                    }

                    $query = $conn->table('tbl_bet_pending_1')
                        ->where([['systemId',$systemId],['mid',$marketId],['result','PENDING'],['status',1]]);

                    $clientArr = $query->distinct()->select('uid')->get();

                    if( $clientArr->isNotEmpty() ){

                        $AllClients = [];
                        foreach ( $clientArr as $cuid ){
                            $AllClients[] = $cuid;
                        }

                        $AllClients = array_unique($AllClients);

                        $client = [];
                        $cdata = DB::table('tbl_user_profit_loss')->select(['userId'])
                            ->where([['parentId',$uid]])->whereIn('clientId',$AllClients)
                            ->distinct()->select('userId')->get();

                        if ($cdata != null) {
                            foreach ($cdata as $c) {
                                $client[] = (int)$c->userId;
                            }
                        }

                    }

                    if(!empty( $client )){
                        $userList = DB::table('tbl_user')->select(['id','parentId','username','role','systemId'])
                            ->whereIn('id',$client)->where([['status',1],['systemId',$systemId]])->get();

                        if( $userList->isNotEmpty() && $runners != null ){
                            $runner1 = $runner2 = $runner3 = 0;

                            $t1 = $t2 = $t3 = null;
                            $i = 0;
                            foreach ( $userList as $user ){

                                $userData[$i] = [
                                    'username' => $user->username,
                                    'id' => $user->id,
                                    'role' => $user->role
                                ];

                                if( isset( $runners[0]['secId'] ) ){
                                    $runner1 = $this->getProfitLossBookMakerForBook($user,$marketId,$runners[0]['secId']);
                                    $t1[] = round($runner1,2);
                                    $userData[$i]['runner1'] = round($runner1);
                                }
                                if( isset( $runners[1]['secId'] ) ){
                                    $runner2 = $this->getProfitLossBookMakerForBook($user,$marketId,$runners[1]['secId']);
                                    $t2[] = round($runner2,2);
                                    $userData[$i]['runner2'] = round($runner2);
                                }
                                if( isset( $runners[2]['secId'] ) ){

                                    $runner3 = $this->getProfitLossBookMakerForBook($user,$marketId,$runners[2]['secId']);
                                    $t3[] = round($runner3,2);
                                    $userData[$i]['runner3'] = round($runner3);
                                }

                                $i++;
                            }

                        }

                    }

                }

                $response = [ "status" => 1 , "data" => [ 'titleData' => $titleData , 'userData' => $userData , 'totalData' => $totalData ] ];

            }
            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * action System Book Data BookMaker
     * Action - Post
     * Created at DEC 2020
     */
    public function actionSystemBookDataBookMaker(Request $request)
    {

        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];

        try{

            if( isset( $request->marketId ) ){
                $titleData = $userData = $totalData = [];
                $user = Auth::user();
                $uid = $user->id; if( $user->role == 6 ){ $uid = 1; }
                $marketId = $request->marketId;

                $redis = Redis::connection();
                $marketDataJson = $redis->get('Market_'.$marketId);
                $marketData = json_decode($marketDataJson);

                if( $marketData != null && isset($marketData->runners) ){
                    $runnerData = json_decode($marketData->runners);
                    $runners = [];
                    if( $runnerData != null ){
                        foreach ( $runnerData as $runner ){
                            $runners[] = [
                                'secId' => $runner->secId,
                                'name' => $runner->runner
                            ];
                        }
                    }

                    if( $runners != null ) {
                        $runnerName1 = $runnerName2 = $runnerName3 = '';
                        $titleData['0'] = 'User Name';
                        if (isset($runners[0]['name'])) {
                            $runnerName1 = $runners[0]['name'];
                            $titleData['1'] = $runnerName1;
                        }
                        if (isset($runners[1]['name'])) {
                            $runnerName2 = $runners[1]['name'];
                            $titleData['2'] = $runnerName2;
                        }
                        if (isset($runners[2]['name'])) {
                            $runnerName3 = $runners[2]['name'];
                            $titleData['3'] = $runnerName3;
                        }

                    }

                    $userList = DB::table('tbl_user')->select(['id','parentId','username','role','systemId'])
                        ->where([['parentId',$uid],['status',1],['systemId','!=',1],['role',1]])->get();

                    if( $userList->isNotEmpty() && $runners != null ){
                        $runner1 = $runner2 = $runner3 = 0;

                        $t1 = $t2 = $t3 = null;
                        $i = 0;
                        foreach ( $userList as $user ){

                            $userData[$i] = [
                                'username' => $user->username,
                                'id' => $user->id,
                                'role' => $user->role
                            ];

                            if( isset( $runners[0]['secId'] ) ){
                                $runner1 = $this->getProfitLossBookMakerForBook($user,$marketId,$runners[0]['secId']);
                                $t1[] = round($runner1,2);
                                $userData[$i]['runner1'] = round($runner1);
                            }
                            if( isset( $runners[1]['secId'] ) ){
                                $runner2 = $this->getProfitLossBookMakerForBook($user,$marketId,$runners[1]['secId']);
                                $t2[] = round($runner2,2);
                                $userData[$i]['runner2'] = round($runner2);
                            }
                            if( isset( $runners[2]['secId'] ) ){

                                $runner3 = $this->getProfitLossBookMakerForBook($user,$marketId,$runners[2]['secId']);
                                $t3[] = round($runner3,2);
                                $userData[$i]['runner3'] = round($runner3);
                            }

                            $i++;
                        }

                    }

                }

                $response = [ "status" => 1 , "data" => [ 'titleData' => $titleData , 'userData' => $userData , 'totalData' => $totalData ] ];

            }
            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * action Book Data Fancy
     * Action - Post
     * Created at DEC 2020
     */
    public function actionBookDataFancy(Request $request)
    {
        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];

        try{
            $conn = DB::connection('mongodb');
            $bookDataFancy = [];
            if( isset( $request->marketId ) ) {
                if (isset($request->userId)) {
                    $uid = $request->userId;
                    $user = DB::table('tbl_user')->select(['id','role','systemId'])->where([['id', $uid], ['status', 1]])->first();
                } else {
                    $user = Auth::user(); $uid = $user->id;
                    if( $user->role == 6 ){ $uid = 1; }
                }
                $marketId = $request->marketId;
                $systemId = $user->systemId;

                $query = $conn->table('tbl_bet_pending_2')
                    ->where([['systemId',$systemId],['mid',$marketId],['status',1],['result','PENDING']]);

                if( $user->role != 1 && $user->role != 6 ){
                    $clientJson = DB::table('tbl_user_child_data')->select('clients')
                        ->where([['uid',$user->id]])->first();
                    if( $clientJson != null && isset( $clientJson->clients ) ){
                        $client = json_decode($clientJson->clients);
                        $query->whereIn('uid',$client);
                    }
                }

                $clientArr = $query->distinct()->select('uid')->get();

                if( $clientArr->isNotEmpty() ){

                    $client = [];
                    foreach ( $clientArr as $cuid ){
                        $client[] = $cuid;
                    }

                    $client = array_unique($client);
                    $bookDataFancy = $this->bookDataFancy($systemId,$marketId,$client,$user);

                }
                $response = [ "status" => 1 , "data" => $bookDataFancy ];
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * action Book Data Binary
     * Action - Post
     * Created at DEC 2020
     */
    public function actionBookDataBinary(Request $request)
    {
        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];

        try{
            $conn = DB::connection('mongodb');
            $bookDataBinary = [];
            if( isset( $request->marketId ) ) {
                if (isset($request->userId)) {
                    $uid = $request->userId;
                    $user = DB::table('tbl_user')->select(['id','role','systemId'])->where([['id', $uid], ['status', 1]])->first();
                } else {
                    $user = Auth::user(); $uid = $user->id;
                    if( $user->role == 6 ){ $uid = 1; }
                }
                $marketId = $request->marketId;
                $systemId = $user->systemId;

                $query = $conn->table('tbl_bet_pending_5')
                    ->where([['systemId',$systemId],['mid',$marketId],['status',1],['result','PENDING']]);

                if( $user->role != 1 && $user->role != 6 ){
                    $clientJson = DB::table('tbl_user_child_data')->select('clients')
                        ->where([['uid',$user->id]])->first();
                    if( $clientJson != null && isset( $clientJson->clients ) ){
                        $client = json_decode($clientJson->clients);
                        $query->whereIn('uid',$client);
                    }
                }

                $clientArr = $query->distinct()->select('uid')->get();

                if( $clientArr->isNotEmpty() ){

                    $client = [];
                    foreach ( $clientArr as $cuid ){
                        $client[] = $cuid;
                    }

                    $client = array_unique($client);
                    $bookDataBinary= $this->bookDataBinary($systemId,$marketId,$client,$user);

                }
                $response = [ "status" => 1 , "data" => $bookDataBinary ];
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * action System Book Data Fancy
     * Action - Post
     * Created at DEC 2020
     */
    public function actionSystemBookDataFancy(Request $request)
    {
        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];

        try{
            $conn = DB::connection('mongodb');
            $bookDataFancy = [];
            if( isset( $request->marketId ) ) {
                if (isset($request->userId)) {
                    $uid = $request->userId;
                    $user = DB::table('tbl_user')->select(['id','role','systemId'])->where([['id', $uid], ['status', 1]])->first();
                } else {
                    $user = Auth::user();
                    $uid = $user->id;
                }
                $marketId = $request->marketId;
                $systemId = $user->systemId;

                $query = $conn->table('tbl_bet_pending_2')
                    ->where([['mid',$marketId],['status',1],['result','PENDING']]);

                if( $user->role != 1 && $user->role != 6 ){
                    $clientJson = DB::table('tbl_user_child_data')->select('clients')
                        ->where([['uid',$user->id]])->first();
                    if( $clientJson != null && isset( $clientJson->clients ) ){
                        $client = json_decode($clientJson->clients);
                        $query->whereIn('uid',$client);
                    }
                }

                $clientArr = $query->distinct()->select('uid')->get();

                if( $clientArr->isNotEmpty() ){

                    $client = [];
                    foreach ( $clientArr as $cuid ){
                        $client[] = $cuid;
                    }

                    $client = array_unique($client);
                    $bookDataFancy = $this->systemBookDataFancy($marketId,$client,$user);

                }
                $response = [ "status" => 1 , "data" => $bookDataFancy ];
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * action System Book Data Binary
     * Action - Post
     * Created at DEC 2020
     */
    public function actionSystemBookDataBinary(Request $request)
    {
        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];

        try{
            $conn = DB::connection('mongodb');
            $bookDataBinary = [];
            if( isset( $request->marketId ) ) {
                if (isset($request->userId)) {
                    $uid = $request->userId;
                    $user = DB::table('tbl_user')->select(['id','role','systemId'])->where([['id', $uid], ['status', 1]])->first();
                } else {
                    $user = Auth::user();
                    $uid = $user->id;
                }
                $marketId = $request->marketId;
                $systemId = $user->systemId;

                $query = $conn->table('tbl_bet_pending_5')
                    ->where([['mid',$marketId],['status',1],['result','PENDING']]);

                if( $user->role != 1 && $user->role != 6 ){
                    $clientJson = DB::table('tbl_user_child_data')->select('clients')
                        ->where([['uid',$user->id]])->first();
                    if( $clientJson != null && isset( $clientJson->clients ) ){
                        $client = json_decode($clientJson->clients);
                        $query->whereIn('uid',$client);
                    }
                }

                $clientArr = $query->distinct()->select('uid')->get();

                if( $clientArr->isNotEmpty() ){

                    $client = [];
                    foreach ( $clientArr as $cuid ){
                        $client[] = $cuid;
                    }

                    $client = array_unique($client);
                    $bookDataBinary = $this->systemBookDataBinary($marketId,$client,$user);

                }
                $response = [ "status" => 1 , "data" => $bookDataBinary ];
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * action Book Data Fancy3
     * Action - Post
     * Created at DEC 2020
     */
    public function actionBookDataFancy3(Request $request)
    {
        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];

        try{
            $conn = DB::connection('mongodb');
            $bookDataFancy3 = [];
            if( isset( $request->marketId ) ) {
                if (isset($request->userId)) {
                    $uid = $request->userId;
                    $user = DB::table('tbl_user')->select(['id','role','systemId'])->where([['id', $uid], ['status', 1]])->first();
                } else {
                    $user = Auth::user();
                    $uid = $user->id;
                }
                $marketId = $request->marketId;
                $systemId = $user->systemId;

                $query = $conn->table('tbl_bet_pending_2')
                    ->where([['systemId',$systemId],['mid',$marketId],['status',1],['result','PENDING']]);

                if( $user->role != 1 && $user->role != 6 ){
                    $clientJson = DB::table('tbl_user_child_data')->select('clients')
                        ->where([['uid',$user->id]])->first();
                    if( $clientJson != null && isset( $clientJson->clients ) ){
                        $client = json_decode($clientJson->clients);
                        $query->whereIn('uid',$client);
                    }
                }

                $clientArr = $query->distinct()->select('uid')->get();

                if( $clientArr->isNotEmpty() ){

                    $client = [];
                    foreach ( $clientArr as $cuid ){
                        $client[] = $cuid;
                    }

                    $client = array_unique($client);

                    $bookDataFancy3 = $this->bookDataFancy3($systemId,$marketId,$client,$user);

                }

                $response = [ "status" => 1 , "data" => $bookDataFancy3 ];

            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * action System Book Data Fancy3
     * Action - Post
     * Created at DEC 2020
     */
    public function actionSystemBookDataFancy3(Request $request)
    {
        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];

        try{

            $bookDataFancy3 = [];
            if( isset( $request->marketId ) ) {
                $user = Auth::user();
                $marketId = $request->marketId;

                $query = DB::table('tbl_bet_pending_2_view')
                    ->where([['systemId','!=',1],['mid',$marketId],['status',1],['result','PENDING']]);

                $clientArr = $query->distinct()->select('uid')->get();

                if( $clientArr->isNotEmpty() ){

                    $client = [];
                    foreach ( $clientArr as $cuid ){
                        $client[] = $cuid;
                    }

                    $client = array_unique($client);

                    $bookDataFancy3 = $this->systemBookDataFancy3($marketId,$client,$user);

                }

                $response = [ "status" => 1 , "data" => $bookDataFancy3 ];

            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * action Book Data Ball Session
     * Action - Post
     * Created at DEC 2020
     */
    public function actionBookDataBallSession(Request $request)
    {
        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];

        try{
            $conn = DB::connection('mongodb');
            $bookDataFancy = [];
            if( isset( $request->marketId ) ) {
                if (isset($request->userId)) {
                    $uid = $request->userId;
                    $user = DB::table('tbl_user')->select(['id','role','systemId'])->where([['id', $uid], ['status', 1]])->first();
                } else {
                    $user = Auth::user();
                    $uid = $user->id;
                }
                $marketId = $request->marketId;
                $systemId = $user->systemId;

                $query = $conn->table('tbl_bet_pending_3')
                    ->where([['systemId',$systemId],['mid',$marketId],['status',1],['result','PENDING']]);

                if( $user->role != 1 && $user->role != 6 ){
                    $clientJson = DB::table('tbl_user_child_data')->select('clients')
                        ->where([['uid',$user->id]])->first();
                    if( $clientJson != null && isset( $clientJson->clients ) ){
                        $client = json_decode($clientJson->clients);
                        $query->whereIn('uid',$client);
                    }
                }

                $clientArr = $query->distinct()->select('uid')->get();

                if( $clientArr->isNotEmpty() ){

                    $client = [];
                    foreach ( $clientArr as $cuid ){
                        $client[] = $cuid;
                    }

                    $client = array_unique($client);
                    $bookDataFancy = $this->bookDataBallSession($systemId,$marketId,$client,$user);

                }

                $response = [ "status" => 1 , "data" => $bookDataFancy ];
            }
            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * action System Book Data Ball Session
     * Action - Post
     * Created at DEC 2020
     */
    public function actionSystemBookDataBallSession(Request $request)
    {
        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];

        try{
            $conn = DB::connection('mongodb');
            $bookDataFancy = [];
            if( isset( $request->marketId ) ) {
                $user = Auth::user();
                $marketId = $request->marketId;

                $query = $conn->table('tbl_bet_pending_3')
                    ->where([['systemId','!=',1],['mid',$marketId],['status',1],['result','PENDING']]);

                $clientArr = $query->distinct()->select('uid')->get();

                if( $clientArr->isNotEmpty() ){

                    $client = [];
                    foreach ( $clientArr as $cuid ){
                        $client[] = $cuid;
                    }

                    $client = array_unique($client);
                    $bookDataFancy = $this->systemBookDataBallSession($marketId,$client,$user);

                }

                $response = [ "status" => 1 , "data" => $bookDataFancy ];
            }
            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * action Book Data Khado Session
     * Action - Post
     * Created at DEC 2020
     */
    public function actionBookDataKhadoSession(Request $request)
    {
        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];

        try{
            $conn = DB::connection('mongodb');
            $bookDataFancy = [];
            if( isset( $request->marketId ) ) {
                if (isset($request->userId)) {
                    $uid = $request->userId;
                    $user = DB::table('tbl_user')->select(['id','role','systemId'])->where([['id', $uid], ['status', 1]])->first();
                } else {
                    $user = Auth::user();
                    $uid = $user->id;
                }

                $marketId = $request->marketId;
                $systemId = $user->systemId;

                $query = $conn->table('tbl_bet_pending_3')
                    ->where([['systemId',$systemId],['mid',$marketId],['status',1],['result','PENDING']]);

                if( $user->role != 1 && $user->role != 6 ){
                    $clientJson = DB::table('tbl_user_child_data')->select('clients')
                        ->where([['uid',$user->id]])->first();
                    if( $clientJson != null && isset( $clientJson->clients ) ){
                        $client = json_decode($clientJson->clients);
                        $query->whereIn('uid',$client);
                    }
                }

                $clientArr = $query->distinct()->select('uid')->get();

                if( $clientArr->isNotEmpty() ){

                    $client = [];
                    foreach ( $clientArr as $cuid ){
                        $client[] = $cuid;
                    }

                    $client = array_unique($client);
                    if( $user->role != 4 ){
                        $bookDataFancy = $this->BookDataKhadoSession($systemId,$marketId,$client,$user);
                    }else{
                        $bookDataFancy = $this->BookDataKhadoSessionClient($systemId,$marketId,$user);
                    }


                }

                $response = [ "status" => 1 , "data" => $bookDataFancy ];
            }
            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * action System Book Data Khado Session
     * Action - Post
     * Created at DEC 2020
     */
    public function actionSystemBookDataKhadoSession(Request $request)
    {
        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];

        try{
            $conn = DB::connection('mongodb');
            $bookDataFancy = [];
            if( isset( $request->marketId ) ) {
                $user = Auth::user();
                $marketId = $request->marketId;

                $query = $conn->table('tbl_bet_pending_3')
                    ->where([['systemId','!=',1],['mid',$marketId],['status',1],['result','PENDING']]);

                $clientArr = $query->distinct()->select('uid')->get();

                if( $clientArr->isNotEmpty() ){

                    $client = [];
                    foreach ( $clientArr as $cuid ){
                        $client[] = $cuid;
                    }

                    $client = array_unique($client);

                    $bookDataFancy = $this->systemBookDataKhadoSession($marketId,$client,$user);

                }

                $response = [ "status" => 1 , "data" => $bookDataFancy ];
            }
            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * action Book Data Casino
     * Action - Post
     * Created at DEC 2020
     */
    public function actionBookDataCasino(Request $request)
    {
        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];

        try{
            $conn = DB::connection('mongodb');
            if( isset( $request->marketId ) ) {
                $bookData = [];
                if (isset($request->userId)) {
                    $uid = $request->userId;
                    $user = DB::table('tbl_user')->select(['id', 'role', 'systemId'])->where([['id', $uid], ['status', 1]])->first();
                } else {
                    $user = Auth::user();
                    $uid = $user->id; if( $user->role == 6 ){ $uid = 1; }
                }
                $marketId = $request->marketId;
                $systemId = $user->systemId;

                $query = $conn->table('tbl_bet_pending_4')
                    ->where([['systemId',$systemId],['mid',$marketId],['status',1],['result','PENDING']]);

                if( $user->role != 1 && $user->role != 6 ){
                    $clientJson = DB::table('tbl_user_child_data')->select('clients')
                        ->where([['uid',$user->id]])->first();
                    if( $clientJson != null && isset( $clientJson->clients ) ){
                        $client = json_decode($clientJson->clients);
                        $query->whereIn('uid',$client);
                    }
                }

                $clientArr = $query->distinct()->select('uid')->get();

                if( $clientArr->isNotEmpty() ) {

                    $client = [];
                    foreach ($clientArr as $cuid) {
                        $client[] = $cuid;
                    }

                    $client = array_unique($client);

                    for( $i=0; $i<=9; $i++ ){
                        $profitLoss = $this->profitLossCasino($uid,$systemId,$marketId,$client,$i);
                        $profitLoss = (-1)*$profitLoss;
                        $bookData[] = [
                            'number' => $i,
                            'profitLoss' => round($profitLoss,0),
                        ];

                    }

                }
                $response = ["status" => 1, "data" => $bookData];

            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * action System Book Data Casino
     * Action - Post
     * Created at DEC 2020
     */
    public function actionSystemBookDataCasino(Request $request)
    {
        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];

        try{
            $conn = DB::connection('mongodb');
            if( isset( $request->marketId ) ) {
                $bookData = [];
                $user = Auth::user();
                $uid = $user->id;if( $user->role == 6 ){ $uid = 1; }
                $marketId = $request->marketId;

                $query = $conn->table('tbl_bet_pending_4')
                    ->where([['systemId','!=',1],['mid',$marketId],['status',1],['result','PENDING']]);

                $clientArr = $query->distinct()->select('uid')->get();

                if( $clientArr->isNotEmpty() ) {

                    $client = [];
                    foreach ($clientArr as $cuid) {
                        $client[] = $cuid;
                    }

                    $client = array_unique($client);

                    for( $i=0; $i<=9; $i++ ){
                        $profitLoss = $this->systemProfitLossCasino($uid,$marketId,$client,$i);
                        $profitLoss = (-1)*$profitLoss;
                        $bookData[] = [
                            'number' => $i,
                            'profitLoss' => round($profitLoss,0),
                        ];

                    }
                }
                $response = ["status" => 1, "data" => $bookData];

            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    // get Profit Loss for Match Odd New
    public function getProfitLossMatchOddForBook($user,$marketId,$secId)
    {
        $conn = DB::connection('mongodb');
        $role = $user->role;
        $parentId = $user->parentId;
        $userId  = $user->id;
        if( $user->role == 6 ){ $uid = 1; }
        $systemId  = $user->systemId;

        $clientArr = []; $book = 0;

        if( $parentId == 0 ){ return $book; }

        $query = $conn->table('tbl_bet_pending_1');

        if( ( $role != 1 && $role != 4 ) || ( $systemId != 1 && $role == 1 ) ){

            $query->where([['mid',$marketId],['status',1],['systemId',$systemId],['result','PENDING']]);

            $clientJson = DB::table('tbl_user_child_data')->select('clients')
                ->where([['uid',$userId]])->first();
            if( $clientJson != null && isset( $clientJson->clients )){
                $client = json_decode($clientJson->clients);
                $query->whereIn('uid',$client);
            }

            $clientArr = $query->distinct()->select('uid')->get();
            if( $clientArr->isNotEmpty() ){
                $total = $book = 0; $totalArr = $clientListArr = [];

                foreach ( $clientArr as $cuid ){
                    $clientListArr[] = $cuid;
                }

                $bookDataProfitLoss = DB::table('tbl_book_matchodd_view as bm')
                    ->select('bm.book','bm.uid','bm.secId','upl.actual_profit_loss')
                    ->leftjoin('tbl_user_profit_loss as upl', 'upl.clientId', '=', 'bm.uid')
                    ->where([['bm.mid',$marketId],['bm.secId',$secId],['upl.userId',$parentId]])
                    ->whereIn('upl.clientId',$clientListArr)->get();

                if( $bookDataProfitLoss->isNotEmpty() ){
                    foreach ( $bookDataProfitLoss as $book ){
                        $total = $book->book;
                        if( (float)$book->actual_profit_loss > 0 ){
                            $total = ($total*(float)$book->actual_profit_loss)/100;
                            $totalArr[] = $total;
                        }else{
                            $totalArr[] = 0;
                        }
                    }

                    if( $totalArr != null ){
                        $book = round((-1)*array_sum($totalArr),2);
                    }
                }

            }

        }else{
            $total = $book = 0;
            $clientArr = $query->where([['mid',$marketId],['status',1],['systemId',$systemId],['uid',$userId],['result','PENDING']])->first();
            if( $clientArr != null ){
                $bookData = DB::table('tbl_book_matchodd_view')->select('book')
                    ->where([['mid',$marketId],['secId',$secId],['uid',$userId]])->first();

                if( $bookData != null && $bookData->book != 0 ){
                    $total = $bookData->book;
                    $where = [['clientId',$userId],['userId',$parentId]];
                    $profitLoss = DB::table('tbl_user_profit_loss')
                        ->select(['actual_profit_loss'])->where($where)->first();

                    if( $profitLoss != null && (float)$profitLoss->actual_profit_loss > 0 ){
                        $total = ($total*(float)$profitLoss->actual_profit_loss)/100;
                    }

                    $book = round((-1)*$total,2);

                }
            }
        }

        return $book;

    }

    // get Profit Loss for Book Maker New
    public function getProfitLossBookMakerForBook($user,$marketId,$secId)
    {
        $conn = DB::connection('mongodb');
        $role = $user->role;
        $parentId = $user->parentId;
        $userId  = $user->id;
        if( $user->role == 6 ){ $userId = 1; }
        $systemId  = $user->systemId;

        $clientArr = []; $book = 0;

        if( $parentId == 0 ){ return $book; }

        $query = $conn->table('tbl_bet_pending_1');

        if( ( $role != 1 && $role != 4 ) || ( $systemId != 1 && $role == 1 )  ){
            $query->where([['mid',$marketId],['status',1],['systemId',$systemId],['result','PENDING']]);
            $clientJson = DB::table('tbl_user_child_data')->select('clients')
                ->where([['uid',$userId]])->first();
            if( $clientJson != null && isset( $clientJson->clients )){
                $client = json_decode($clientJson->clients);
                $query->whereIn('uid',$client);
            }

            $clientArr = $query->distinct()->select('uid')->get();
            if( $clientArr->isNotEmpty() ){
                $total = $book = 0; $totalArr = $clientListArr = [];

                foreach ( $clientArr as $cuid ){
                    $clientListArr[] = $cuid;
                }

                $bookDataProfitLoss = DB::table('tbl_book_bookmaker_view as bm')
                    ->select('bm.book','bm.uid','bm.secId','upl.actual_profit_loss')
                    ->leftjoin('tbl_user_profit_loss as upl', 'upl.clientId', '=', 'bm.uid')
                    ->where([['bm.mid',$marketId],['bm.secId',$secId],['upl.userId',$parentId]])
                    ->whereIn('bm.uid',$clientListArr)->get();

                if( $bookDataProfitLoss->isNotEmpty() ){
                    foreach ( $bookDataProfitLoss as $book ){
                        $total = $book->book;
                        if( (float)$book->actual_profit_loss > 0 ){
                            $total = ($total*(float)$book->actual_profit_loss)/100;
                            $totalArr[] = $total;
                        }else{
                            $totalArr[] = 0;
                        }
                    }
                }

                if( $totalArr != null ){
                    $book = round((-1)*array_sum($totalArr),2);
                }

            }

        }else{
            $total = $book = 0;
            $clientArr = $query->where([['mid',$marketId],['status',1],['systemId',$systemId],['uid',$userId],['result','PENDING']])->first();
            if( $clientArr != null ){
                $bookData = DB::table('tbl_book_bookmaker_view')->select('book')
                    ->where([['mid',$marketId],['secId',$secId],['uid',$userId]])->first();

                if( $bookData != null && $bookData->book != 0 ){
                    $total = $bookData->book;
                    $profitLoss = DB::table('tbl_user_profit_loss')
                        ->select(['actual_profit_loss'])
                        ->where([['clientId',$userId],['userId',$parentId]])->first();

                    if( $profitLoss != null && (float)$profitLoss->actual_profit_loss > 0 ){
                        $total = ($total*(float)$profitLoss->actual_profit_loss)/100;
                    }

                    $book = round((-1)*$total,2);

                }
            }
        }

        return $book;

    }

    // book Data Fancy
    public function bookDataFancy($systemId,$marketId,$client,$user){
        $conn = DB::connection('mongodb');
        $dataReturn = []; $userId = $user->id; if( $user->role == 6 ){ $userId = 1; }
        $query = $conn->table('tbl_bet_pending_2');

        if( $user->role != 4 ){
            $query->where([['mid',$marketId],['status', 1],['systemId',$systemId],['result','PENDING']]);
            $query->whereIn('uid',$client);
        }else{
            $query->where([['uid',(int)$userId],['mid',$marketId],['status', 1],['systemId',$systemId],['result','PENDING']]);
        }
        $betList = $query->select('price')->get();

        if( $betList != null ){
            $min = $max = 0;
            foreach ($betList as $index => $bet) {
                $bet = (object)$bet;
                if ($index == 0) {
                    $min = $bet->price;
                    $max = $bet->price;
                }
                if ($min > $bet->price)
                    $min = $bet->price;
                if ($max < $bet->price)
                    $max = $bet->price;
            }

            $min = $min-1; $max = $max+1;

            $dataReturn['min'] = $min;
            $dataReturn['max'] = $max;
            $dataReturn['betList'] = [];
            $dataReturn['userRole'] = $user->role;
            if( $user->role != 4 ){

                $userProfitLoss = DB::table('tbl_user_profit_loss')
                    ->select(['actual_profit_loss as apl','clientId'])
                    ->where([['userId',$userId]])->whereIn('clientId',$client)->get();
                if( $userProfitLoss->isNotEmpty() ){

                    $betList11 = $conn->table('tbl_bet_pending_2')
                        ->select(['bType','win','loss','price','uid'])
                        ->where([['mid',$marketId],['status',1],['result','PENDING'],['systemId',$systemId]])
                        ->whereIn('uid',$client)->get();

                    if( $betList11->isNotEmpty() ){
                        $dataReturn['betList'] = [];
                        foreach ( $betList11 as $betData ){
                            $betData = (object)$betData;
                            foreach ( $userProfitLoss as $upl ){
                                if( $upl->clientId == $betData->uid ){
                                    $dataReturn['betList'][] = [
                                        'bType' => $betData->bType,
                                        'win' => $betData->win,
                                        'loss' => $betData->loss,
                                        'price' => $betData->price,
                                        'apl' => $upl->apl
                                    ];
                                }
                            }
                        }
                    }
                }

                /*$betList11 = DB::table('mongodb.tbl_bet_pending_2 as pb')
                    ->select('bType','pb.win as win','pb.loss as loss','upl.actual_profit_loss as apl','pb.price as price')
                    ->leftjoin('mysql.tbl_user_profit_loss as upl', 'upl.clientId', '=', 'pb.uid')
                    ->where([['pb.mid',$marketId],['pb.status',1],['result','PENDING'],['pb.systemId',$systemId],['upl.userId',$userId]])
                    ->whereIn('pb.uid',$client)->get();

                if( $betList11->isNotEmpty() ){
                    $dataReturn['betList'] = $betList11;
                }*/

            }else{
                $betList11 = $conn->table('tbl_bet_pending_2')
                    ->where([['mid',$marketId],['status',1],['result','PENDING'],['systemId',$systemId],['uid',(int)$userId]])
                    ->select('bType','win','loss','price')->get();

                if( $betList11->isNotEmpty() ){
                    $dataReturn['betList'] = $betList11;
                }

            }

        }

        return $dataReturn;
    }

    // book Data Binary
    public function bookDataBinary($systemId,$marketId,$client,$user){
        $conn = DB::connection('mongodb');
        $dataReturn = []; $userId = $user->id; if( $user->role == 6 ){ $userId = 1; }
        $query = $conn->table('tbl_bet_pending_5');

        if( $user->role != 4 ){
            $query->where([['mid',$marketId],['status', 1],['systemId',$systemId],['result','PENDING']]);
            $query->whereIn('uid',$client);
        }else{
            $query->where([['uid',(int)$userId],['mid',$marketId],['status', 1],['systemId',$systemId],['result','PENDING']]);
        }
        $betList = $query->select('price')->get();

        if( $betList != null ){
            $min = $max = 0;
            foreach ($betList as $index => $bet) {
                $bet = (object)$bet;
                if ($index == 0) {
                    $min = $bet->price;
                    $max = $bet->price;
                }
                if ($min > $bet->price)
                    $min = $bet->price;
                if ($max < $bet->price)
                    $max = $bet->price;
            }

            $min = $min-1; $max = $max+1;

            $dataReturn['min'] = $min;
            $dataReturn['max'] = $max;
            $dataReturn['betList'] = [];
            $dataReturn['userRole'] = $user->role;
            if( $user->role != 4 ){

                $userProfitLoss = DB::table('tbl_user_profit_loss')
                    ->select(['actual_profit_loss as apl','clientId'])
                    ->where([['userId',$userId]])->whereIn('clientId',$client)->get();
                if( $userProfitLoss->isNotEmpty() ){

                    $betList11 = $conn->table('tbl_bet_pending_5')
                        ->select(['bType','win','loss','price','uid'])
                        ->where([['mid',$marketId],['status',1],['result','PENDING'],['systemId',$systemId]])
                        ->whereIn('uid',$client)->get();

                    if( $betList11->isNotEmpty() ){
                        $dataReturn['betList'] = [];
                        foreach ( $betList11 as $betData ){
                            $betData = (object)$betData;
                            foreach ( $userProfitLoss as $upl ){
                                if( $upl->clientId == $betData->uid ){
                                    $dataReturn['betList'][] = [
                                        'bType' => $betData->bType,
                                        'win' => $betData->win,
                                        'loss' => $betData->loss,
                                        'price' => $betData->price,
                                        'apl' => $upl->apl
                                    ];
                                }
                            }
                        }
                    }
                }

                /*$betList11 = DB::table('mongodb.tbl_bet_pending_5 as pb')
                    ->select('bType','pb.win as win','pb.loss as loss','upl.actual_profit_loss as apl','pb.price as price')
                    ->leftjoin('mysql.tbl_user_profit_loss as upl', 'upl.clientId', '=', 'pb.uid')
                    ->where([['pb.mid',$marketId],['pb.status',1],['result','PENDING'],['pb.systemId',$systemId],['upl.userId',$userId]])
                    ->whereIn('pb.uid',$client)->get();

                if( $betList11->isNotEmpty() ){
                    $dataReturn['betList'] = $betList11;
                }*/

            }else{
                $betList11 = $conn->table('tbl_bet_pending_5')
                    ->where([['mid',$marketId],['status',1],['result','PENDING'],['systemId',$systemId],['uid',(int)$userId]])
                    ->select('bType','win','loss','price')->get();

                if( $betList11->isNotEmpty() ){
                    $dataReturn['betList'] = $betList11;
                }

            }

        }

        return $dataReturn;
    }


    // system book Data Fancy
    public function systemBookDataFancy($marketId,$client,$user){
        $conn = DB::connection('mongodb');
        $dataReturn = []; $userId = $user->id;
        if( $user->role == 6 ){ $userId = 1; }
        $query = $conn->table('tbl_bet_pending_2')
            ->where([['systemId','!=',1],['mid',$marketId],['status', 1],['result','PENDING']])
            ->whereIn('uid',$client);
        $betList = $query->select('price')->get();

        if( $betList != null ){
            $min = $max = 0;
            foreach ($betList as $index => $bet) {
                $bet = (object)$bet;
                if ($index == 0) {
                    $min = $bet->price;
                    $max = $bet->price;
                }
                if ($min > $bet->price)
                    $min = $bet->price;
                if ($max < $bet->price)
                    $max = $bet->price;
            }

            $min = $min-1; $max = $max+1;
            $dataReturn['min'] = $min;
            $dataReturn['max'] = $max;

            $userProfitLoss = DB::table('tbl_user_profit_loss')
                ->select(['actual_profit_loss as apl','clientId'])
                ->where([['userId',$userId]])->whereIn('clientId',$client)->get();
            if( $userProfitLoss->isNotEmpty() ){

                $betList11 = $conn->table('tbl_bet_pending_2')
                    ->select(['bType','win','loss','price','uid'])
                    ->where([['mid',$marketId],['status',1],['result','PENDING'],['systemId','!=',1]])
                    ->whereIn('uid',$client)->get();

                if( $betList11->isNotEmpty() ){
                    $dataReturn['betList'] = [];
                    foreach ( $betList11 as $betData ){
                        $betData = (object)$betData;
                        foreach ( $userProfitLoss as $upl ){
                            if( $upl->clientId == $betData->uid ){
                                $dataReturn['betList'][] = [
                                    'bType' => $betData->bType,
                                    'win' => $betData->win,
                                    'loss' => $betData->loss,
                                    'price' => $betData->price,
                                    'apl' => $upl->apl
                                ];
                            }
                        }
                    }
                }
            }

            /*$betList11 = DB::table('mongodb.tbl_bet_pending_2 as pb')
                ->select('bType','pb.win as win','pb.loss as loss','upl.actual_profit_loss as apl','pb.price as price')
                ->leftjoin('mysql.tbl_user_profit_loss as upl', 'upl.clientId', '=', 'pb.uid')
                ->where([['pb.mid',$marketId],['pb.status',1],['result','PENDING'],['upl.userId',$userId],['pb.systemId','!=',1]])
                ->whereIn('pb.uid',$client)->get();

            if( $betList11->isNotEmpty() ){
                $dataReturn['betList'] = $betList11;
            }*/

        }

        return $dataReturn;
    }

    // system book Data Binary
    public function systemBookDataBinary($marketId,$client,$user){
        $conn = DB::connection('mongodb');
        $dataReturn = []; $userId = $user->id;
        if( $user->role == 6 ){ $userId = 1; }
        $query = $conn->table('tbl_bet_pending_5')
            ->where([['systemId','!=',1],['mid',$marketId],['status', 1],['result','PENDING']])
            ->whereIn('uid',$client);
        $betList = $query->select('price')->get();

        if( $betList != null ){
            $min = $max = 0;
            foreach ($betList as $index => $bet) {
                $bet = (object)$bet;
                if ($index == 0) {
                    $min = $bet->price;
                    $max = $bet->price;
                }
                if ($min > $bet->price)
                    $min = $bet->price;
                if ($max < $bet->price)
                    $max = $bet->price;
            }

            $min = $min-1; $max = $max+1;
            $dataReturn['min'] = $min;
            $dataReturn['max'] = $max;

            $userProfitLoss = DB::table('tbl_user_profit_loss')
                ->select(['actual_profit_loss as apl','clientId'])
                ->where([['userId',$userId]])->whereIn('clientId',$client)->get();
            if( $userProfitLoss->isNotEmpty() ){

                $betList11 = $conn->table('tbl_bet_pending_5')
                    ->select(['bType','win','loss','price','uid'])
                    ->where([['mid',$marketId],['status',1],['result','PENDING'],['systemId','!=',1]])
                    ->whereIn('uid',$client)->get();

                if( $betList11->isNotEmpty() ){
                    $dataReturn['betList'] = [];
                    foreach ( $betList11 as $betData ){
                        $betData = (object)$betData;
                        foreach ( $userProfitLoss as $upl ){
                            if( $upl->clientId == $betData->uid ){
                                $dataReturn['betList'][] = [
                                    'bType' => $betData->bType,
                                    'win' => $betData->win,
                                    'loss' => $betData->loss,
                                    'price' => $betData->price,
                                    'apl' => $upl->apl
                                ];
                            }
                        }
                    }
                }
            }

            /*$betList11 = DB::table('mongodb.tbl_bet_pending_5 as pb')
                ->select('bType','pb.win as win','pb.loss as loss','upl.actual_profit_loss as apl','pb.price as price')
                ->leftjoin('mysql.tbl_user_profit_loss as upl', 'upl.clientId', '=', 'pb.uid')
                ->where([['pb.mid',$marketId],['pb.status',1],['result','PENDING'],['upl.userId',$userId],['pb.systemId','!=',1]])
                ->whereIn('pb.uid',$client)->get();

            if( $betList11->isNotEmpty() ){
                $dataReturn['betList'] = $betList11;
            }*/

        }

        return $dataReturn;
    }

    // book Data Fancy3
    public function bookDataFancy3($systemId,$marketId,$client,$user){
        $conn = DB::connection('mongodb');
        $userId = $user->id; if( $user->role == 6 ){ $userId = 1; }
        $yesWinVal = $yesLossVal = $noWinVal = $noLossVal = 0;
        $dataReturn['userRole'] = $user->role;
        if( $user->role != 4 ){

            $userProfitLoss = DB::table('tbl_user_profit_loss')
                ->select(['actual_profit_loss as apl','clientId'])
                ->where([['userId',$userId]])->whereIn('clientId',$client)->get();
            if( $userProfitLoss->isNotEmpty() ){

                $betList11 = $conn->table('tbl_bet_pending_2')
                    ->select(['bType','win','loss','price','uid'])
                    ->where([['mid',$marketId],['status',1],['result','PENDING'],['systemId',$systemId]])
                    ->whereIn('uid',$client)->get();

                if( $betList11->isNotEmpty() ){
                    $dataReturn['betList'] = [];
                    foreach ( $betList11 as $betData ){
                        $betData = (object)$betData;
                        foreach ( $userProfitLoss as $upl ){
                            if( $upl->clientId == $betData->uid ){
                                $dataReturn['betList'][] = [
                                    'bType' => $betData->bType,
                                    'win' => $betData->win,
                                    'loss' => $betData->loss,
                                    'price' => $betData->price,
                                    'apl' => $upl->apl
                                ];
                            }
                        }
                    }
                }
            }

            /*$betList11 = DB::table('mongodb.tbl_bet_pending_2 as pb')
                ->select('bType','pb.win as win','pb.loss as loss','upl.actual_profit_loss as apl')
                ->leftjoin('mysql.tbl_user_profit_loss as upl', 'upl.clientId', '=', 'pb.uid')
                ->where([['pb.mid',$marketId],['upl.userId',$userId],['pb.status',1],['result','PENDING'],['pb.systemId',$systemId]])
                ->whereIn('pb.uid',$client)->get();

            if( $betList11->isNotEmpty() ){
                $dataReturn['betList'] = $betList11;
            }*/

        }else{
            $betList11 = $conn->table('tbl_bet_pending_2')->select(['win','loss','bType'])
                ->where([['mid',$marketId],['uid',(int)$userId],['status',1],['result','PENDING'],['systemId',$systemId]])
                ->get();

            if( $betList11->isNotEmpty() ){
                $dataReturn['betList'] = $betList11;
            }
        }
        return $dataReturn;
    }

    // system book Data Fancy3
    public function systemBookDataFancy3($marketId,$client,$user){
        $conn = DB::connection('mongodb');
        $dataReturn = []; $userId = $user->id;
        if( $user->role == 6 ){ $userId = 1; }

        $userProfitLoss = DB::table('tbl_user_profit_loss')
            ->select(['actual_profit_loss as apl','clientId'])
            ->where([['userId',$userId]])->whereIn('clientId',$client)->get();
        if( $userProfitLoss->isNotEmpty() ){

            $betList11 = $conn->table('tbl_bet_pending_2')
                ->select(['bType','win','loss','price','uid'])
                ->where([['mid',$marketId],['status',1],['result','PENDING'],['systemId','!=',1]])
                ->whereIn('uid',$client)->get();

            if( $betList11->isNotEmpty() ){
                $dataReturn['betList'] = [];
                foreach ( $betList11 as $betData ){
                    $betData = (object)$betData;
                    foreach ( $userProfitLoss as $upl ){
                        if( $upl->clientId == $betData->uid ){
                            $dataReturn['betList'][] = [
                                'bType' => $betData->bType,
                                'win' => $betData->win,
                                'loss' => $betData->loss,
                                'price' => $betData->price,
                                'apl' => $upl->apl
                            ];
                        }
                    }
                }
            }
        }

        /*$betList11 = DB::table('mongodb.tbl_bet_pending_2 as pb')
            ->select('bType','pb.win as win','pb.loss as loss','upl.actual_profit_loss as apl')
            ->leftjoin('mysql.tbl_user_profit_loss as upl', 'upl.clientId', '=', 'pb.uid')
            ->where([['pb.mid',$marketId],['upl.userId',$userId],['pb.status',1],['result','PENDING'],['pb.systemId','!=',1]])
            ->whereIn('pb.uid',$client)->get();

        if( $betList11->isNotEmpty() ){
            $dataReturn['betList'] = $betList11;
        }*/
        return $dataReturn;
    }

    // book Data BallSession
    public function bookDataBallSession($systemId,$marketId,$client,$user){
        $conn = DB::connection('mongodb');
        $dataReturn = []; $userId = $user->id;
        if( $user->role == 6 ){ $userId = 1; }
        $query = $conn->table('tbl_bet_pending_3');
        if( $user->role != 4 ){
            $query->where([['mid',$marketId],['status', 1],['systemId',$systemId],['result','PENDING']])
                ->whereIn('uid',$client);
        }else{
            $query->where([['uid',(int)$userId],['mid',$marketId],['status', 1],['systemId',$systemId],['result','PENDING']]);
        }

        $betList = $query->select('price')->get();

        if( $betList != null ){
            $min = $max = 0;
            foreach ($betList as $index => $bet) {
                $bet = (object)$bet;
                if ($index == 0) {
                    $min = $bet->price;
                    $max = $bet->price;
                }
                if ($min > $bet->price)
                    $min = $bet->price;
                if ($max < $bet->price)
                    $max = $bet->price;
            }

            $min = $min-1; $max = $max+1;
            $dataReturn['min'] = $min;
            $dataReturn['max'] = $max;
            $dataReturn['userRole'] = $user->role;
            if( $user->role != 4 ){
                $userProfitLoss = DB::table('tbl_user_profit_loss')
                    ->select(['actual_profit_loss as apl','clientId'])
                    ->where([['userId',$userId]])->whereIn('clientId',$client)->get();
                if( $userProfitLoss->isNotEmpty() ){

                    $betList11 = $conn->table('tbl_bet_pending_3')
                        ->select(['bType','win','loss','price','uid'])
                        ->where([['mid',$marketId],['status',1],['result','PENDING'],['systemId',$systemId]])
                        ->whereIn('uid',$client)->get();

                    if( $betList11->isNotEmpty() ){
                        $dataReturn['betList'] = [];
                        foreach ( $betList11 as $betData ){
                            $betData = (object)$betData;
                            foreach ( $userProfitLoss as $upl ){
                                if( $upl->clientId == $betData->uid ){
                                    $dataReturn['betList'][] = [
                                        'bType' => $betData->bType,
                                        'win' => $betData->win,
                                        'loss' => $betData->loss,
                                        'price' => $betData->price,
                                        'apl' => $upl->apl
                                    ];
                                }
                            }
                        }
                    }
                }

                /*$betList11 = DB::table('mongodb.tbl_bet_pending_3 as pb')
                    ->select('bType','pb.win as win','pb.loss as loss','upl.actual_profit_loss as apl','pb.price as price')
                    ->leftjoin('mysql.tbl_user_profit_loss as upl', 'upl.clientId', '=', 'pb.uid')
                    ->where([['pb.mid',$marketId],['pb.status',1],['result','PENDING'],['pb.systemId',$systemId],['upl.userId',$userId]])
                    ->whereIn('pb.uid',$client)->get();

                if( $betList11->isNotEmpty() ){
                    $dataReturn['betList'] = $betList11;
                }*/
            }else{
                $betList11 = $conn->table('tbl_bet_pending_3')
                    ->where([['mid',$marketId],['status',1],['result','PENDING'],['systemId',$systemId],['uid',(int)$userId]])
                    ->select('bType','win','loss','price')->get();

                if( $betList11->isNotEmpty() ){
                    $dataReturn['betList'] = $betList11;
                }
            }
        }

        return $dataReturn;
    }

    // system Book Data BallSession
    public function systemBookDataBallSession($marketId,$client,$user){
        $conn = DB::connection('mongodb');
        $dataReturn = []; $userId = $user->id;
        if( $user->role == 6 ){ $userId = 1; }
        $query = $conn->table('tbl_bet_pending_3')
            ->where([['mid',$marketId],['status', 1],['systemId','!=',1],['result','PENDING']])->whereIn('uid',$client);
        $betList = $query->select('price')->get();

        if( $betList != null ){
            $min = $max = 0;
            foreach ($betList as $index => $bet) {
                $bet = (object)$bet;
                if ($index == 0) {
                    $min = $bet->price;
                    $max = $bet->price;
                }
                if ($min > $bet->price)
                    $min = $bet->price;
                if ($max < $bet->price)
                    $max = $bet->price;
            }

            $min = $min-1; $max = $max+1;
            $dataReturn['min'] = $min;
            $dataReturn['max'] = $max;

            $userProfitLoss = DB::table('tbl_user_profit_loss')
                ->select(['actual_profit_loss as apl','clientId'])
                ->where([['userId',$userId]])->whereIn('clientId',$client)->get();
            if( $userProfitLoss->isNotEmpty() ){

                $betList11 = $conn->table('tbl_bet_pending_3')
                    ->select(['bType','win','loss','price','uid'])
                    ->where([['mid',$marketId],['status',1],['result','PENDING'],['systemId','!=',1]])
                    ->whereIn('uid',$client)->get();

                if( $betList11->isNotEmpty() ){
                    $dataReturn['betList'] = [];
                    foreach ( $betList11 as $betData ){
                        $betData = (object)$betData;
                        foreach ( $userProfitLoss as $upl ){
                            if( $upl->clientId == $betData->uid ){
                                $dataReturn['betList'][] = [
                                    'bType' => $betData->bType,
                                    'win' => $betData->win,
                                    'loss' => $betData->loss,
                                    'price' => $betData->price,
                                    'apl' => $upl->apl
                                ];
                            }
                        }
                    }
                }
            }

            /*$betList11 = DB::table('mongodb.tbl_bet_pending_3 as pb')
                ->select('bType','pb.win as win','pb.loss as loss','upl.actual_profit_loss as apl','pb.price as price')
                ->leftjoin('mysql.tbl_user_profit_loss as upl', 'upl.clientId', '=', 'pb.uid')
                ->where([['pb.mid',$marketId],['pb.status',1],['result','PENDING'],['upl.userId',$userId],['pb.systemId','!=',1]])
                ->whereIn('pb.uid',$client)->get();

            if( $betList11->isNotEmpty() ){
                $dataReturn['betList'] = $betList11;
            }*/

        }

        return $dataReturn;
    }

    // Book Data KhadoSession
    public function BookDataKhadoSession($systemId,$marketId,$client,$user){
        $conn = DB::connection('mongodb');
        $dataReturn = []; $userId = $user->id;
        if( $user->role == 6 ){ $userId = 1; }
        $betList = $conn->table('tbl_bet_pending_3')
            ->select('bType', 'price', 'win', 'loss','mid','uid','diff')
            ->where([['result', 'PENDING'],['mid', $marketId] , ['status', 1],['systemId',$systemId]])
            ->whereIn('uid',$client)->get();
        if( $betList != null ){
            $min = 0; $max = 0;
            foreach ($betList as $index => $bet) {
                $bet = (object)$bet;
                if ($index == 0) {
                    $min = $bet->price;
                    $max = $bet->diff;
                }
                if ($min > $bet->price)
                    $min = $bet->price;
                if ($max < $bet->diff)
                    $max = $bet->diff;
            }

            $min = $min-1; $max = $max+1;
            $dataReturn['min'] = $min;
            $dataReturn['max'] = $max;
            $dataReturn['userRole'] = $user->role;

            $userProfitLoss = DB::table('tbl_user_profit_loss')
                ->select(['actual_profit_loss as apl','clientId'])
                ->where([['userId',$userId]])->whereIn('clientId',$client)->get();
            if( $userProfitLoss->isNotEmpty() ){

                $betList11 = $conn->table('tbl_bet_pending_3')
                    ->select(['diff','win','loss','price','uid'])
                    ->where([['mid',$marketId],['status',1],['result','PENDING'],['systemId',$systemId]])
                    ->whereIn('uid',$client)->get();

                if( $betList11->isNotEmpty() ){
                    $dataReturn['betList'] = [];
                    foreach ( $betList11 as $betData ){
                        $betData = (object)$betData;
                        foreach ( $userProfitLoss as $upl ){
                            if( $upl->clientId == $betData->uid ){
                                $dataReturn['betList'][] = [
                                    'diff' => $betData->diff,
                                    'win' => $betData->win,
                                    'loss' => $betData->loss,
                                    'price' => $betData->price,
                                    'apl' => $upl->apl
                                ];
                            }
                        }
                    }
                }
            }

            /*$betList11 = DB::table('mongodb.tbl_bet_pending_3 as pb')
                ->select('pb.diff as diff','pb.price as price','pb.win as win','pb.loss as loss','upl.actual_profit_loss as apl')
                ->leftjoin('mysql.tbl_user_profit_loss as upl', 'upl.clientId', '=', 'pb.uid')
                ->where([['pb.status',1],['pb.mid',$marketId],['result','PENDING'],['pb.systemId',$systemId],['upl.userId',$userId]])
                ->whereIn('pb.uid',$client)->get();

            if( $betList11->isNotEmpty() ){
                $dataReturn['betList'] = $betList11;
            }*/

        }

        return $dataReturn;

    }

    // Book Data KhadoSession for client
    public function BookDataKhadoSessionClient($systemId,$marketId,$user){
            $conn = DB::connection('mongodb');
            $userId = $user->id;
            $where = [ ['systemId',$systemId],['result', 'PENDING'],['mid', $marketId], ['uid', $userId], ['status',1] ];
            $betList = $conn->table('tbl_bet_pending_3')->select('price','win','loss','bType','diff')
                ->where($where)->get();

            $dataReturn = null;
            if( $betList != null ){
                $min = $max = 0;

                foreach ($betList as $index => $bet) {
                    $bet = (object)$bet;
                    if ($index == 0) {
                        $min = $bet->price;
                        $max = $bet->diff;
                    }
                    if ($min > $bet->price)
                        $min = $bet->price;
                    if ($max < $bet->diff)
                        $max = $bet->diff;
                }

                $min = $min-1;
                if( $min < 0 ){ $min = 0; }
                $max =  $max+1;
                $dataReturn['min'] = $min;
                $dataReturn['max'] = $max;
                $dataReturn['userRole'] = $user->role;
                $betList11 = $conn->table('tbl_bet_pending_3')
                    ->select('win','loss','diff','price')
                    ->where([['mid',$marketId],['uid', $userId],['result','PENDING'],['status' , 1] ])->get();

                if( $betList11->isNotEmpty() ){
                    $dataReturn['betList'] = $betList11;
                }

            }

            return $dataReturn;

    }

    // system Book Data KhadoSession
    public function systemBookDataKhadoSession($marketId,$client,$user){
        $conn = DB::connection('mongodb');
        $dataReturn = []; $userId = $user->id;
        if( $user->role == 6 ){ $userId = 1; }
        $betList = $conn->table('tbl_bet_pending_3')
            ->select('bType', 'price', 'win', 'loss','mid','uid','diff')
            ->where([['systemId','!=',1],['result', 'PENDING'],['mid', $marketId] , ['status', 1]])
            ->whereIn('uid',$client)->get();
        if( $betList != null ){
            $min = 0; $max = 0;
            foreach ($betList as $index => $bet) {
                $bet = (object)$bet;
                if ($index == 0) {
                    $min = $bet->price;
                    $max = $bet->diff;
                }
                if ($min > $bet->price)
                    $min = $bet->price;
                if ($max < $bet->diff)
                    $max = $bet->diff;
            }

            $min = $min-1; $max = $max+1;
            $dataReturn['min'] = $min;
            $dataReturn['max'] = $max;

            $userProfitLoss = DB::table('tbl_user_profit_loss')
                ->select(['actual_profit_loss as apl','clientId'])
                ->where([['userId',$userId]])->whereIn('clientId',$client)->get();
            if( $userProfitLoss->isNotEmpty() ){

                $betList11 = $conn->table('tbl_bet_pending_3')
                    ->select(['diff','win','loss','price','uid'])
                    ->where([['mid',$marketId],['status',1],['result','PENDING'],['systemId','!=',1]])
                    ->whereIn('uid',$client)->get();

                if( $betList11->isNotEmpty() ){
                    $dataReturn['betList'] = [];
                    foreach ( $betList11 as $betData ){
                        $betData = (object)$betData;
                        foreach ( $userProfitLoss as $upl ){
                            if( $upl->clientId == $betData->uid ){
                                $dataReturn['betList'][] = [
                                    'diff' => $betData->diff,
                                    'win' => $betData->win,
                                    'loss' => $betData->loss,
                                    'price' => $betData->price,
                                    'apl' => $upl->apl
                                ];
                            }
                        }
                    }
                }
            }

            /*$betList11 = DB::table('mongodb.tbl_bet_pending_3 as pb')
                ->select('pb.diff as diff','pb.price as price','pb.win as win','pb.loss as loss','upl.actual_profit_loss as apl')
                ->leftjoin('mysql.tbl_user_profit_loss as upl', 'upl.clientId', '=', 'pb.uid')
                ->where([['pb.status',1],['pb.mid',$marketId],['result','PENDING'],['upl.userId',$userId],['pb.systemId','!=',1]])
                ->whereIn('pb.uid',$client)->get();

            if( $betList11->isNotEmpty() ){
                $dataReturn['betList'] = $betList11;
            }*/
        }

        return $dataReturn;

    }

    // profitLossCasino
    public function profitLossCasino($uid,$systemId,$marketId,$client,$secId){
        $conn = DB::connection('mongodb');
        $win = $loss = $total = 0; $totalArr = [];
        if( $client != null ){

            $betData = $conn->table('tbl_bet_pending_4')->select(['uid','win','loss','secId'])
                ->where([['result','PENDING'],['status',1],['mid',$marketId],['systemId',$systemId]])
                ->whereIn('uid',$client)->get();

            if( $betData->isNotEmpty() ){
                $newUser = [];
                foreach ( $betData as $bets ){
                    $bets = (object)$bets;
                    if( !isset($newUser[$bets->uid]['total']) ){
                        $newUser[$bets->uid]['total'] = 0;
                    }

                    if( $bets->secId == $secId ){
                        $newUser[$bets->uid]['total'] = ( $newUser[$bets->uid]['total'] + $bets->win );
                    }else{
                        $newUser[$bets->uid]['total'] = ( $newUser[$bets->uid]['total'] - $bets->loss );
                    }
                }

                if( $newUser != null ){
                    $totalArr = [];
                    foreach ( $newUser as $usr=>$data ){
                        $profitLoss = DB::table('tbl_user_profit_loss')->select(['actual_profit_loss'])
                            ->where([['clientId',$usr],['userId',$uid]])->first();

                        if( $data['total'] != 0 && $profitLoss != null && (float)$profitLoss->actual_profit_loss > 0 ){
                            $totalArr[] = ($data['total']*(float)$profitLoss->actual_profit_loss)/100;
                        }
                    }
                }

                if( $totalArr != null && array_sum($totalArr) != 0 ){
                    return round( array_sum($totalArr) );
                }
            }

        }

        return $total;

    }

    // profitLossCasino
    public function systemProfitLossCasino($uid,$marketId,$client,$secId){
        $conn = DB::connection('mongodb');
        $win = $loss = $total = 0; $totalArr = [];
        if( $client != null ){

            $betData = $conn->table('tbl_bet_pending_4')->select(['uid','win','loss','secId'])
                ->where([['result','PENDING'],['status',1],['mid',$marketId],['systemId','!=',1]])
                ->whereIn('uid',$client)->get();

            if( $betData->isNotEmpty() ){
                $newUser = [];
                foreach ( $betData as $bets ){
                    $bets = (object)$bets;
                    if( !isset($newUser[$bets->uid]['total']) ){
                        $newUser[$bets->uid]['total'] = 0;
                    }

                    if( $bets->secId == $secId ){
                        $newUser[$bets->uid]['total'] = ( $newUser[$bets->uid]['total'] + $bets->win );
                    }else{
                        $newUser[$bets->uid]['total'] = ( $newUser[$bets->uid]['total'] - $bets->loss );
                    }
                }

                if( $newUser != null ){
                    $totalArr = [];
                    foreach ( $newUser as $usr=>$data ){
                        $profitLoss = DB::table('tbl_user_profit_loss')->select(['actual_profit_loss'])
                            ->where([['clientId',$usr],['userId',$uid]])->first();

                        if( $data['total'] != 0 && $profitLoss != null && (float)$profitLoss->actual_profit_loss > 0 ){
                            $totalArr[] = ($data['total']*(float)$profitLoss->actual_profit_loss)/100;
                        }
                    }
                }

                if( $totalArr != null && array_sum($totalArr) != 0 ){
                    return round( array_sum($totalArr) );
                }
            }

        }

        return $total;

    }

}
