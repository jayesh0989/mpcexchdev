<?php
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Casino API Route
Route::get('casino/game-list-add', 'Casino\GameController@actionGameListAdd');
Route::post('wallet/balance', 'Casino\WalletController@actionBalance');
Route::post('wallet/bet', 'Casino\WalletController@actionBet');
Route::post('wallet/win', 'Casino\WalletController@actionWin');
Route::post('wallet/rollback', 'Casino\WalletController@actionRollback');

//Test API
Route::get('category/list', 'Test\CategoryController@list');
Route::post('category/add', 'Test\CategoryController@add');
Route::post('category/update', 'Test\CategoryController@update');

//Poker Live Game 1
Route::post('poker/app-user-auth', 'Poker\AuthController@actionAppUserAuth');
Route::post('poker/auth', 'Poker\AuthController@actionLogin');
Route::post('poker/exposure', 'Poker\ExposureController@actionBetPlace');
Route::post('poker/results', 'Poker\ResultsController@actionGameResult');

//Poker Live Game 2
Route::post('poker/user-auth', 'Poker\AuthliveController@actionLogin');
Route::post('poker/place-bet', 'LiveGames\GameexposureController@actionBetPlace');
Route::post('poker/game-exposure', 'LiveGames\GameexposureController@actionBetPlaceConfirm');
Route::post('poker/getbalance', 'LiveGames\GameexposureController@actionGetBalance');
Route::post('poker/profitloss', 'Poker\ProfitLossController@actionGameResult');
Route::post('poker/rollback-profitloss', 'Poker\ProfitLossController@actionGameRecall');

//Cron
Route::get('common-jobs', 'Event\EventCronJobController@allCronJobs');
Route::get('event-list', 'Event\EventCronJobController@eventList');
Route::get('bet-migration', 'BetHistory\BetMigrationController@betMigration');
Route::get('clean-data','DeleteController@deleteData');

Route::get('site-mode', 'AuthController@siteMode');
Route::get('get-captcha', 'AuthController@getCaptcha');
Route::post('login', 'AuthController@login');
Route::post('register', 'AuthController@register');
Route::get('operator-detail', 'Operator\OperatorDetailController@detail');
Route::get('android-version', 'Dashboard\AndroidController@androidVersion');

//landing page
Route::get('landing-detail', 'Dashboard\HomeListController@list');
Route::get('get-markets', 'Redis\RedisDataController@getMarketList');
Route::get('get-cricket-markets', 'Redis\RedisDataController@getCricketMarketList');
Route::get('get-jackpot-markets', 'Redis\RedisDataController@getMarketListJackpot');
Route::get('get-game-sportdata', 'Redis\RedisDataController@getGameSportdata');
Route::get('store-game-cricketdata', 'Redis\RedisDataController@storeGameCricketdata');
Route::get('store-game-sportdata', 'Redis\RedisDataController@storeGameSportdata');
Route::get('store-game-jackpotdata', 'Redis\RedisDataController@storeGameJackpotdata');


Route::middleware('auth:api')->group(function () {
    //Logout and change pass
    Route::get('logout', 'AuthController@logout')->name('logout');
    Route::post('change-password', 'AuthController@changePassword');

    //User and data
    Route::get('user', 'PassportController@details');
    Route::post('user-data', 'UserData\UserDataController@userData');
    Route::post('user-data/{id}', 'UserData\UserDataController@userData');
    Route::get('user-match-unmatch-data', 'UserData\UserDataController@UsermatchUnmatchData');
    Route::get('user-match-unmatch-data/{id}', 'UserData\UserDataController@UsermatchUnmatchData');

    // Event
    Route::get('event-election/{id}', 'Event\ElectionController@election');
    Route::get('event-binary/{id}', 'Event\BinaryController@binary');
    Route::get('event-lottery/{id}', 'Event\LotteryController@lottery');
    Route::get('event-jackpot-dynamic-list/{id}', 'Event\JackpotController@jackpotDynamicList');
    Route::post('event-jackpot-list', 'Event\JackpotController@jackpotList');
    Route::post('event-jackpot-detail', 'Event\JackpotController@jackpotDetail');
    Route::get('event-jackpot-fancy/{id}', 'Event\JackpotController@jackpotFancy');
    Route::get('event-jackpot-khado/{id}', 'Event\JackpotController@jackpotKhado');
    Route::get('dashboard-front-menu-list', 'Dashboard\MenuController@frontMenuList');
    Route::post('market-result', 'Market\MarketResultController@marketResult');
    Route::get('market-list', 'Market\MarketController@marketList');
    Route::post('market-profit-loss', 'Market\MarketProfitLossController@marketProfitLoss');
    Route::post('market-sports-list', 'Market\MarketProfitLossController@marketSportsList');
    Route::post('market-bet-list', 'Market\MarketProfitLossController@marketBetList');
    Route::post('market-get-event-list', 'Market\MarketProfitLossController@getEventList');

    //Account Statement
    Route::post('transaction-account-statement', 'Transaction\AccountController@accountStatement');
    Route::post('account-bet-list', 'Transaction\AccountController@accountBetList');
    Route::post('export-statement', 'Transaction\AccountController@exportStatement');

    Route::post('account-bet-list-test', 'Transaction\AccountController@accountBetListTest');

    //Detail API
    Route::get('dashboard-detail/{id}', 'Dashboard\DetailController@detail');
    Route::get('event-inplay-today-tomorrow', 'Event\InplayTodayTomorrow@inplayTodayTomorrow');
    Route::get('rules', 'Rule\RulesController@rules');
    Route::get('rules1', 'Rule\RulesController@rules1');
    Route::post('set-bet-options', 'Setting\BetStackSettingController@setBetOptions');
    Route::post('newRule', 'Rule\RulesController@newRule');

    //Bet History
    Route::post('current-bets', 'CurrentBet\CurrentBetsController@list');
    Route::post('bet-history', 'BetHistory\BetHistoryController@betHistory');
    Route::post('casino-bet-history', 'BetHistory\CasinoBetHistoryController@casinoBetHistory');
    Route::post('teenpatti-bet-history', 'BetHistory\TeenpattiBetHistoryController@TeenpattiBetHistory');

    //Bet place api
    Route::post('bet-place-test', 'Event\AppBetPlaceTestController@index');
    Route::post('bet-place-new', 'Event\BetPlaceController@requestBetPlace');
    Route::post('bet-place', 'BetPlace\BetPlaceController@requestBetPlace');
    //Route::post('bet-place', 'Event\AppBetPlaceController@index');
    Route::get('request-generate', 'Event\AppBetPlaceController@requestGenerate');

    //Profit Loss
    Route::get('dashboard-list', 'Dashboard\ListController@list');
    Route::get('event-app-list', 'Event\AppListController@applist');
    Route::post('profit-loss-fancy', 'Market\ProfitLossFancyController@profitLossFancy');
    Route::post('profit-loss-lottery-book', 'Market\ProfitLossLotteryBookController@profitLossLotteryBook');
    Route::post('get-betlist', 'Market\ProfitLossFancyController@getBetList');
    Route::post('profit-loss-matchodd-bookmaker', 'Market\ProfitLossMatchOddBookmakerController@ProfitLossMatchOddBookmaker');
    Route::post('profit-loss-khado', 'Market\ProfitLossKhadoController@profitLossKhado');

    Route::get('user-profile','UserProfile\UserProfileController@userProfile');

    //casino api
    Route::get('casino/game-list', 'Casino\GameController@actionGameList');
    Route::post('casino/game-url', 'Casino\GameController@actionGameUrl');
    
    Route::post('casino/transaction', 'Casino\DepositWithdrawController@casinoTransaction');
});

//View Log
Route::get('mpc-log','LogController@mpcLog');
Route::get('mpc-activity-log','LogController@mpcActivityLog');
Route::get('mpc-casino-log','LogController@mpcCasinoLog');

Route::get('log/{id?}','LogController@viewLog')->name('viewlog');
Route::get('view-activity-log/{id?}','LogController@viewActivityLog')->name('activityLog');
Route::get('view-casino-log/{id?}','LogController@viewCasinoLog')->name('activityLog');