<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class TeenpattiResult extends Model
{
    public $timestamps=false;
    protected $table = 'tbl_teenpatti_result';
    protected $connection = 'mysql3';
    
}
