<?php

namespace App\Http\Controllers\Event;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redis;



class BinaryController extends Controller
{
    private $bookMarketArr = [];

    public function binary(Request $request)
    {

        try {
            $response = ["status" => 0, "code" => 400, "data" => null, "message" => "Bad request!"];
            $key = $this->haskKey();
            $requestId = $request->tnp;
            $generatehashkey = base64_encode(hash_hmac("sha256", $requestId, $key, true));
            $hashKey = $request->header('hash');
            $hashAuth = $this->HashAuth($generatehashkey, $hashKey);
            if ($hashAuth == 1) {

                $response = ['status' => 0, 'message' => 'Bad request !!!', 'data' => null];
                $eventArr = [];

                if (null != $request->route('id')) {
                    $uid = Auth::user()->id;
                    $eventId = $request->route('id');
                    $sportId = 6;
                    $a = true;
                    $updatedOn = $this->updatedOn($uid);

                    $cache = Redis::connection();
                    $eventDataCache = $cache->get("Event_" . $eventId);
                    $eventData = json_decode($eventDataCache);

                    //$eventDataRunner = json_decode($eventData->runners, 16);

                    if ($eventData != null) {
                        $sportId = $eventData->sportId;
                        $title = $eventData->name;
                        $marketId = null;
                        $binaryData = $this->getDataBinary($uid, $eventId);

                        $eventArr = [
                            'title' => $title,
                            'sport' => 'Binary',
                            'eventId' => $eventId,
                            'sportId' => $sportId,
                            'binary' => $binaryData
                        ];

                        if ((null == $binaryData && null == $binaryData && null == $binaryData)) {
                            $this->marketIdsArr = null;
                        }

                        $response = ["status" => 1, 'code' => 200, "updatedOn" => strtotime($updatedOn), "data" => ["items" => $eventArr, 'marketIdsArr' => $this->marketIdsArr, "count" => 0], 'message' => "Data Found !!!"];
                    } else {
                        $response = ["status" => 1, 'code' => 200, "updatedOn" => null, "data" => null, "message" => "This event is closed !!"];
                    }
                }
                return $response;
            } else {
                return $response;
            }
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }


    }


//Event: getDataFancy
    public function getDataBinary($uid, $eventId)
    {

        $items = null;
        $cache = Redis::connection();

        if (isset($cache)) {
            $eventData = $cache->get("Binary_" . $eventId);
            $marketList = json_decode($eventData);
        }

        $this->bookMarketArr = $this->getBookMarketArr($uid, $eventId);


        if ($marketList != null) {

            foreach ($marketList as $market) {

                if ($market->game_over == 0 && $market->status == 1) {
                    $suspended = $ballRunning = 0;
                    if (isset($market->suspended)) {
                        $suspended = $market->suspended;
                    }
                    if (isset($market->ball_running)) {
                        $ballRunning = $market->ball_running;
                    }
                    if (in_array($market->marketId, $this->bookMarketArr)) {
                        $isBook = '1';
                    } else {
                        $isBook = '0';
                    }

                    /*$minStack = $market->min_stack;
                    $maxStack = $market->max_stack;
                    $maxProfitLimit = $market->max_profit_limit;*/

                    $min_stack=$market->min_stack;
                    $max_stack=$market->max_stack;
                    $max_profit_limit=$market->max_profit_limit;

                    $marketSettingStatus = $cache->get('MARKET_SETTING_STATUS');
                    if($marketSettingStatus == 1){
                        $Setting = $cache->get('BINARY_SETTING');
                        if(!empty($Setting)){
                        $Setting = json_decode($Setting);
                        $min_stack = $Setting->Min_stack;
                        $max_stack = $Setting->Max_stack;
                        $max_profit_limit = $Setting->Max_profit_limit;
                    }
                    }

                    $userkey="Admin_Market_".$market->marketId; 
                   if (!empty($userkey)) {
                     $Setting = $cache->get("Admin_Market_".$market->marketId);
                     if (!empty($Setting)) {
                        $Setting = json_decode($Setting);
                        $min_stack=$Setting->min_stack;
                        $max_stack=$Setting->max_stack;
                        $max_profit_limit=$Setting->max_profit_limit;
                     }
                    }

                    $betDelay = 0;


                    $items[] = [
                        'marketId' => $market->marketId,
                        'eventId' => $market->eid,
                        'title' => $market->title,
                        'info' => $market->info,
                        'suspended' => $suspended,
                        'ballrunning' => $ballRunning,
                        'mType' => $market->mType,
                        'sportId' => 7,
                        'slug' => 'binary',
                        'is_book' => $isBook,
                        'minStack' => $min_stack,
                        'maxStack' => $max_stack,
                        'maxProfitLimit' => $max_profit_limit,
                        //'betDelay' => $betDelay,
                        'no' => 0,
                        'no_rate' => 0,
                        'yes' => 0,
                        'yes_rate' => 0,
                    ];

                    $this->marketIdsArr[] = $market->marketId;
                }
            }

        }


        return $items;
    }

    //getBookMarketArr
    public function getBookMarketArr($uid,$eventId)
    {
         $marketsOnBet = DB::connection('mongodb')->table('tbl_user_market_expose')
            ->where([['eid',(int)$eventId],['uid',$uid],['status',1],['sid','!=',99]])->distinct()->select('mid')->get();
        $marketIds = [];
        if( !empty($marketsOnBet) ){
            foreach ( $marketsOnBet as $key => $market ){
                $marketIds[] = $market;
            }
        }
        return $marketIds;
    }


//get updatedOn function
    public function updatedOn($uid)
    {

        $user = DB::table('tbl_user_info')->select('updated_on')->where('uid', $uid)->first();

        if (!empty($user) && $user != null) {

            return $user->updated_on;

        } else {

            return $user->updated_on;
        }

    }


}