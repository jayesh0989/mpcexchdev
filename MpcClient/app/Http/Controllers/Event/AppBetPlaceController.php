<?php

namespace App\Http\Controllers\Event;
use App\Model\FancyFunction;
use App\Model\GoalsMakerFunction;
use App\Model\LotteryFunction;
use App\Model\JackpotFunction;
use App\Model\BinaryFunction;
use App\Model\KhadoBallbyballFunction;
use App\Model\MatchOddFunction;
use App\Model\BetFairFunction;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;



class AppBetPlaceController extends Controller
{
    public function requestGenerate()
    {
        $response =  [ "status" => 0, "code" => 400, "message" => "Bad request!"];
        try {
            $userId = Auth::id();
            $token = md5(uniqid(rand(), true));
          /*$redis = Redis::connection();
            $redisDataKey = $userId . '-BetRequestToken';
            $redisData = ['token' => $token];
            $redisDataJson = json_encode($redisData);
            $redis->set($redisDataKey, $redisDataJson);
*/
            $response = ["status" => 1, "code" => 200, "token" => $token];
            return $response;
        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }

    public function index(Request $request)
    {
        // print_r($request->input()); exit;

        $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" => "Bad request!"];
        //print_r($request->input()); exit;
        //$mTypeArr = ['TEST_TEENPATTI','LUDO','TEENPATTI','POKER','ANDAR_BAHAR','7UPDOWN','MATKA','SIX_POKER','TEENPATTI20','32CASINO','HILOW','DRAGON_TIGER'];
        //if( !$this->checkOrigin() ){
        //  return response()->json($response);
        //}

        // check postman or etc
        if( !isset( $_SERVER['HTTP_ACCEPT'] ) || isset( $_SERVER['HTTP_POSTMAN_TOKEN'] ) || isset( $_SERVER['HTTP_ACCEPT'] ) && trim($_SERVER['HTTP_ACCEPT']) != 'application/json' ){
            $response =  [ "status" => 0 ,"code" => 400 ,"data" => null, "message" => "Bet Place Call Invalid !!"];
            return $response; exit;
        }

        // new csrf_token check
       /* if( isset($request->csrf_token) ){
            $userId = Auth::id();
            $redis = Redis::connection();
            $redisDataKey = $userId . '-BetRequestToken';
            $redisDataJson = $redis->get($redisDataKey);
            $redisData = json_decode($redisDataJson);

            if( !empty($redisData) && isset($redisData->token) ){
                if( $redisData->token != $request->csrf_token ){
                    $response =  [ "status" => 0 ,"code" => 400 ,"data" => null, "message" => "Bet Place Call Invalid !!1"];
                    return $response; exit;
                }else{
                    $redis->del($redisDataKey);
                }

            }else{
                $response =  [ "status" => 0 ,"code" => 400 ,"data" => null, "message" => "Bet Place Call Invalid !!2"];
                return $response; exit;
            }

        }else{
            $response =  [ "status" => 0 ,"code" => 400 ,"data" => null, "message" => "Bet Place Call Invalid !!3"];
            return $response; exit;
        }*/


        try{
            $eventname='';
            $key=$this->haskKey();
            $requestId=$request->tnp;
            $generatehashkey = base64_encode(hash_hmac("sha256", $requestId, $key, true));
            $hashKey = $request->header('hash');
            $hashAuth=  $this->HashAuth($generatehashkey,$hashKey);
            $currentOdds = null;
            if( $hashAuth == 1 ) {

                if ( !empty($request->size) && !empty($request->runner) && !empty($request->bet_type)
                    && !empty($request->market_id) && !empty($request->event_id) && !empty($request->m_type)
                    && !empty($request->market_name) && !empty($request->sport_id) && !empty($request->rate) ) {

                    // check last bet time
                    $userInfo = Auth::user(); $userId = $userInfo->id;
                    $redis = Redis::connection();
                    $redisDataKey = $userId . '-LastBetRequestTime';

                    $redisDataJson = $redis->get($redisDataKey);
                    $redisData = json_decode($redisDataJson);

                    if( !empty($redisData) && isset($redisData->lastBetRequestTime) ){
                        $cTime = time(); $lastBetRequestTime = $redisData->lastBetRequestTime;
                        $timeDiff = $cTime-$lastBetRequestTime;
                        $redisData = ['lastBetRequestTime' => time() ];
                        $redisDataJson = json_encode($redisData);
                        $redis->set($redisDataKey, $redisDataJson);
                        $timeDiffLimit = 3;
                        if( isset($request->m_type) && $request->m_type == 'match_odd' ){
                            $timeDiffLimit = 6;
                        }
                        if( $timeDiff < $timeDiffLimit ){
                            $response =  [ "status" => 0 ,"code" => 400 ,"data" => null, "message" => "Bet Place Call Invalid ! Plz Try Later !"];
                            return $response; exit;
                        }
                    }else{
                        $redisData = ['lastBetRequestTime' => time() ];
                        $redisDataJson = json_encode($redisData);
                        $redis->set($redisDataKey, $redisDataJson);
                    }

                    $bTypeArr = ['back','lay','yes','no'];
                    if(!in_array(trim($request->bet_type), $bTypeArr)) {
                        return $response; exit;
                    }
                    if( $request->m_type == 'khado' && $request->bet_type != 'yes' ) {
                        return $response; exit;
                    }
                    if( $request->m_type == 'virtual_cricket' && $request->bet_type != 'back' ) {
                        return $response; exit;
                    }
                    if ( $request->m_type == 'jackpot' && $request->bet_type != 'back' ){
                        return $response; exit;
                    }
                    if ( ( $request->m_type == 'cricket_casino' || $request->sport_id == 11 ) && $request->bet_type != 'back' ){
                        return $response; exit;
                    }
                    if( $request->m_type != 'cricket_casino' ){
                        if( empty($request->price) || empty($request->sec_id) ){
                            return $response; exit;
                        }
                    }

                    $marketId = $request->market_id;
                    $eventId = $request->event_id;
                    $sportId = $request->sport_id;
                    $mType = $request->m_type;

                    $cache = Redis::connection();
                    // check market
                    $marketDataJsonCheck = $cache->get("Market_" . $marketId);
                    $marketDataCheck = json_decode($marketDataJsonCheck);

                    if( $mType != 'jackpot' && empty($marketDataCheck) ){
                        return $response; exit;
                    }

                    if( !empty($marketDataCheck) ) {
                        if (isset($marketDataCheck->event_id) && $marketDataCheck->event_id != $eventId) {
                            return $response; exit;
                        }
                        if (isset($marketDataCheck->eventId) && $marketDataCheck->eventId != $eventId) {
                            return $response; exit;
                        }
                        if (isset($marketDataCheck->eid) && $marketDataCheck->eid != $eventId) {
                            return $response; exit;
                        }
                        if (isset($marketDataCheck->sportId) && $marketDataCheck->sportId != $sportId) {
                            return $response; exit;
                        }
                        if (isset($marketDataCheck->sid) && $marketDataCheck->sid != $sportId) {
                            return $response; exit;
                        }
                        if (isset($marketDataCheck->mType) && $marketDataCheck->mType != $mType) {
                            return $response; exit;
                        }

                        if (!isset($marketDataCheck->mType)) {
                            if (isset($marketDataCheck->slug) && $marketDataCheck->slug != $mType) {
                                return $response; exit;
                            }
                        }
                    }

                    // print_r($request->input()); exit;
                    /* check event status for CLOSED and bet_allowed */

                    $eventData = $cache->get("Event_" . $request->event_id);
                    $event = json_decode($eventData);
                    $eventname = $request->eventname;
                    if(isset($event->name)){
                        $eventname = $event->name;
                    }

                    //print_r($event);
                    if ($request->m_type == 'jackpot') {
                        $ctime = time();
                        $closeTime = ($event->time) - $ctime;
                        if ($closeTime <= 0) {
                            $response = ["status" => 0, "code" => 400, "data" => null, "message" => "Bet can not place due to event closed or bet is not allowed!!"];
                            return $response; exit;
                        }
                    }

                    if (empty($event) || $event->bet_allowed != 1 || $event->type == 'CLOSED' || $event->is_block == 1) {
                        $response = ["status" => 0, "code" => 400, "data" => null, "message" => "Bet can not place due to event closed or bet is not allowed!!"];
                        return $response; exit;
                    }

                    $sport_List = $cache->get('Sport_' . $request->sport_id);

                    if (!empty($sport_List)) {
                        $sport_List = json_decode($sport_List);
                        //echo "<pre>"; print_r($sport_List); exit;
                        if ($sport_List->is_block == 1) {
                            $response = ["status" => 0, "code" => 400, "data" => null, "message" => "Bet can not place due to sport blocked or bet is not allowed!!"];
                            return $response; exit;
                        }

                        if ($sport_List->status != 1) {
                            $response = ["status" => 0, "code" => 400, "data" => null, "message" => "Bet can not place due to sport In Active or bet is not allowed!!"];
                            return $response; exit;
                        }
                    }

                    /* get user info */

                    if (empty($userInfo)) {
                        $response = ["status" => 0, "code" => 400, "data" => null, "message" => "User does not exist so bet can not place!"];
                        return $response; exit;
                    }

                    $uid = $userInfo->id;
                    $parentId = $userInfo->parentId;
                    /* check user status for block unblock and active Inactive */
                    $query = DB::table('tbl_user_block_status')->select('*')->where('uid', $uid);
                    $userStatusCheck = $query->first();
                    if (!empty($userStatusCheck)) {
                        $response = ["status" => 0, "code" => 400, "data" => null, "message" => "Your account is blocked by parent! Plz contact administrator!!"];
                        return $response; exit;
                    }
                    if (!empty($userStatusCheck) && $userStatusCheck->type == 2) {
                        $response = ["status" => 0, "code" => 400, "data" => null, "message" => "Your bet is locked by parent! Plz contact administrator!!"];
                        return $response; exit;
                    }

                    if ($userInfo->status != 1) {
                        $response = ["status" => 0, "code" => 400, "data" => null, "message" => "Bet can not place due to user In active !!!!"];
                        return $response; exit;
                    }

                    if ($userInfo->role != 4) {
                        $response = ["status" => 0, "code" => 400, "data" => null, "message" => "Bet can not place due to wrong user role !!!!"];
                        return $response; exit;
                    }

                    if ($userInfo->is_login != 1) {
                        $response = ["status" => 0, "code" => 400, "data" => null, "message" => "Bet can not place due to login issue !!!!"];
                        return $response; exit;
                    }

                    // check profit loss data
                    $profitLossClient = DB::table('tbl_user_profit_loss')
                        ->where([['clientId', $uid],['userId', 1]])->first();
                    if( $profitLossClient == null ){
                        $response = ["status" => 0, "code" => 400, "data" => null, "message" => "Bet can not place! Plz contact admin!!"];
                        return $response; exit;
                    }

                    /* check user check balance */
                    $userAvailableBalance = $userBalance = $userExposeBalance = $userProfitLossBalance = 0;
                    $query = DB::table('tbl_user_info')->select(['balance', 'pl_balance', 'expose', 'pName'])
                        ->where('uid', $uid);
                    $userBalArray = $query->first();
                    if (!empty($userBalArray)) {

                        $userBalance = isset($userBalArray->balance) ? (int)$userBalArray->balance : 0;
                        $userExposeBalance = isset($userBalArray->expose) ? (int)$userBalArray->expose : 0;
                        $userProfitLossBalance = isset($userBalArray->pl_balance) ? (int)$userBalArray->pl_balance : 0;

                        $userAvailableBalance = ($userBalance - $userExposeBalance + $userProfitLossBalance);

                        if (round($userAvailableBalance) < 0) {
                            $response = ["status" => 0, "code" => 400, "data" => null, "message" => "Insufficient funds!"];
                            return $response; exit;
                        }

                        if (round($userAvailableBalance) == 0 && round($userExposeBalance) == 0) {
                            $response = ["status" => 0, "code" => 400, "data" => null, "message" => "Insufficient funds!!"];
                            return $response; exit;
                        }
                    } else {

                        $response = ["status" => 0, "code" => 400, "data" => null, "message" => "Insufficient funds!!!"];
                        return $response; exit;
                    }

                    $t1 = round(microtime(true) * 1000);
                    //$redismarketData=null;
                    if ($request->m_type == 'cricket_casino') {
                        $marketData = $cache->get("Market_" . $request->market_id);
                        $marketDataArray = json_decode($marketData);
                    } else {
                        $marketData = $cache->get($request->market_id);
                        $marketDataArray = json_decode($marketData);
                        //$redismarketData = $cache->get("Market_" . $request->market_id);
                        //$redismarketData = json_decode($redismarketData);
                    }

                    /* suspend check */
                    if ($request->m_type != 'jackpot' && isset($marketDataArray->suspended)) {
                        if ($marketDataArray->suspended == 1 && $request->m_type != 'cricket_casino') {
                            $response = ["status" => 1, "code" => 200, "data" => null, "message" => 'Bet can not place due to market suspended!'];
                            return $response; exit;
                        }
                    }

                    /* Ballrunning check */
                    if ( $request->m_type == 'set_market' || $request->m_type == 'goals' || $request->m_type == 'winner' || $request->m_type == 'match_odd' || $request->m_type == 'completed_match' || $request->m_type == 'tied_match' || $request->m_type == 'BANKNIFTY' || $request->m_type == 'NIFTY'
                        || $request->m_type == 'bookmaker' || $request->m_type == 'virtual_cricket') {
                        if (isset($marketDataArray->ballrunning) && $marketDataArray->ballrunning == 1) {
                            $response = ["status" => 1, "code" => 200, "data" => null, "message" => 'Bet cancelled can not place!!'];
                            return $response; exit;
                        }
                    }

                    if ($request->m_type == 'fancy') {
                        if (isset($marketDataArray->ballRunning) && $marketDataArray->ballRunning == 1) {
                            $response = ["status" => 1, "code" => 200, "data" => null, "message" => 'Bet can not place in fancy due to ball running!!'];
                            return $response; exit;
                        }
                     }

                    if ( $request->m_type == 'fancy2' || $request->m_type == 'fancy3' || $request->m_type == 'oddeven' || $request->m_type == 'khado' || $request->m_type == 'ballbyball') {
                        if ( isset($marketDataArray->ball_running) && $marketDataArray->ball_running == 1) {
                            $response = ["status" => 1, "code" => 200, "data" => null, "message" => 'Bet can not place due to ball running!!'];
                            return $response; exit;
                        }
                     }

                    if($request->m_type == 'USDINR' || $request->m_type == 'GOLD'|| $request->m_type == 'SILVER'|| $request->m_type == 'EURINR'|| $request->m_type == 'GBPINR'|| $request->m_type == 'ALUMINIUM' || $request->m_type == 'COPPER' || $request->m_type == 'CRUDEOIL' || $request->m_type == 'ZINC'|| $request->m_type == 'BANKNIFTY' || $request->m_type == 'NIFTY'){
                       if (isset($marketDataArray->ball_running) && $marketDataArray->ball_running == 1) {
                            $response = ["status" => 1, "code" => 200, "data" => null, "message" => 'Bet can not place due to ball running!!'];
                            return $response; exit;
                        }
                    }


                    /* ballrunning suspend check function end */

                    $sportMarketData = [];
                    $sportMarket = $this->sportMarketLimits($uid);
                    if (!empty($sportMarket)) {
                        if ($request['m_type'] == 'bookmaker') {
                            if ($request->sport_id == 4) {
                                $sportMarketData = json_decode($sportMarket->cricket_bookmaker);
                            } elseif ($request->sport_id == 1) {
                                $sportMarketData = json_decode($sportMarket->football_bookmaker);
                            } else {
                                $sportMarketData = json_decode($sportMarket->tennis_bookmaker);
                            }
                        } else if ( $request['m_type'] == 'set_market' || $request['m_type'] == 'goals' || $request['m_type'] == 'winner' || $request['m_type'] == 'match_odd' || $request['m_type'] == 'tied_match' || $request['m_type'] == 'completed_match') {
                            if ($request->sport_id == 4) {
                                $sportMarketData = json_decode($sportMarket->cricket_matchodd);
                            } elseif ($request->sport_id == 1) {
                                $sportMarketData = json_decode($sportMarket->football_matchodd);
                            } else {
                                $sportMarketData = json_decode($sportMarket->tennis_matchodd);
                            }
                        } else if ( $request['m_type'] == 'fancy' || $request['m_type'] == 'fancy2' || $request['m_type'] == 'fancy3' || $request->m_type == 'oddeven') {
                            if ($request->sport_id == 4) {
                                $sportMarketData = json_decode($sportMarket->cricket_fancy);
                            } elseif ($request->sport_id == 1) {
                                $sportMarketData = json_decode($sportMarket->football_fancy);
                            } else {
                                $sportMarketData = json_decode($sportMarket->tennis_fancy);
                            }
                        } else if ( $request['m_type'] == 'meter' || $request['m_type'] == 'khado' || $request['m_type'] == 'ballbyball') {
                            if ($request->sport_id == 4) {
                                $sportMarketData = json_decode($sportMarket->cricket_fancy);
                            }
                        }
                    }


                    $odds_rate = $request->rate; $currentOdds = null;
                    /* checkBetAccepted for all market */
                    if ( $request->m_type == 'meter' || $request->m_type == 'set_market' || $request->m_type == 'goals' || $request->m_type == 'winner' || $request->m_type == 'match_odd' || $request->m_type == 'completed_match' || $request->m_type == 'tied_match' || $request->m_type == 'bookmaker' || $request->m_type == 'virtual_cricket' || $request->m_type == 'ballbyball' || $request->m_type == 'khado' || $request->m_type == 'fancy' || $request->m_type == 'fancy2' || $request->m_type == 'fancy3' || $request->m_type == 'oddeven'
                     || $request->m_type == 'USDINR' || $request->m_type == 'GOLD'|| $request->m_type == 'SILVER'|| $request->m_type == 'EURINR'|| $request->m_type == 'GBPINR'|| $request->m_type == 'ALUMINIUM' || $request->m_type == 'COPPER' || $request->m_type == 'CRUDEOIL' || $request->m_type == 'ZINC'
                    ) {

                        $checkBetAccepted = $this->checkBetAccepted($request->input(), $uid, $t1, $marketDataArray, $sportMarketData);

                        if ( isset($checkBetAccepted['currentOdds']) && $checkBetAccepted['currentOdds'] != null) {
                            $currentOdds = $checkBetAccepted['currentOdds'];
                        }

                        if ($checkBetAccepted['is_true'] == false) {
                            $response = ["status" => 1, "code" => 200, "data" => null, "message" => $checkBetAccepted['msg']];
                            return $response; exit;
                        } else {
                            if (isset($checkBetAccepted['rate']) && $checkBetAccepted['rate'] != 0 && $checkBetAccepted['rate'] != '-') {
                                $odds_rate = $checkBetAccepted['rate'];
                            }else{
                                $response = ["status" => 1, "code" => 200, "data" => null, "message" => 'Bet can not place due to rate not match!'];
                                return $response; exit;
                            }
                        }
                    }

                    $marketDataArrayjson = $cache->get("Market_" . $request->market_id);
                    if ($request->m_type == 'jackpot') {
                        $marketDataArrayjson = $cache->get("Jackpot_market_" . $request->gameId . "_" . $request->event_id . "_" . $request->market_id);
                    }

                    $marketDataArray1 = json_decode($marketDataArrayjson);

                    if ($request->m_type != 'jackpot' && isset($marketDataArray1->bet_allowed)) {
                        if ($marketDataArray1->bet_allowed == 0) {
                            $response = ["status" => 1, "code" => 200, "data" => null, "message" => 'Bet is not allowed so can not place bet!'];
                            return $response; exit;
                        }
                    }

                    if ($request->m_type == 'jackpot' && isset($marketDataArray1->bet_allowed)) {
                        if ($marketDataArray1->bet_allowed == 0) {
                            $response = ["status" => 1, "code" => 200, "data" => null, "message" => 'Bet is not allowed so can not place bet!'];
                            return $response; exit;
                        }
                    }


                   /* $event= $cache->get("Event_limit_".$request['event_id']);
                    $event = json_decode($event);
                    //print_r($event); exit;
                    if( $event != null ){
                        if($odds_rate > $event->max_odd_limit ){
                            $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" => "Your max odd limit is ".$event->max_odd_limit."! Bet can not placed!!"];
                            return $response; exit;
                        }
                    }*/

                    if (!empty($marketDataArray1)) {
                        if ($marketDataArray1->game_over == 1) {
                            $response = ["status" => 1, "code" => 200, "data" => null, "message" => 'This session is already closed!'];
                            return $response; exit;
                        }

                        if ($request->m_type == 'set_market' || $request->m_type == 'goals' || $request->m_type == 'winner' || $request->m_type == 'match_odd' || $request->m_type == 'completed_match' || $request->m_type == 'tied_match') {
                            if ($marketDataArray1->suspended == 1) {
                                $response = ["status" => 1, "code" => 200, "data" => null, "message" => 'Bet cannot place in this betfair due to market suspended !'];
                                return $response; exit;
                            }
                        }
                    } else {
                        $response = ["status" => 1, "code" => 200, "data" => null, "message" => 'Bet can not place due to empty market!'];
                        return $response; exit;
                    }

                    if ($request->m_type != 'cricket_casino') {
                        $marketData = $cache->get($request->market_id);
                        $marketDataArray = json_decode($marketData);
                    }

                    if ($request->m_type != 'jackpot' && isset($marketDataArray->suspended)) {
                        if ($marketDataArray->suspended == 1 && $request->m_type != 'cricket_casino') {
                            $response = ["status" => 1, "code" => 200, "data" => null, "message" => 'Bet can not place due to market suspended!'];
                            return $response; exit;
                        }
                    }

                    $userLimitArray = null;
                    /* place bet match and bookmaker function */
                    if ( $request->m_type == 'set_market' || $request->m_type == 'goals' || $request->m_type == 'winner' || $request->m_type == 'match_odd' || $request->m_type == 'bookmaker' || $request->m_type == 'virtual_cricket') {
                        if (isset($marketDataArray->ballrunning) && $marketDataArray->ballrunning == 1) {
                            $response = ["status" => 1, "code" => 200, "data" => null, "message" => 'Bet cancelled can not place!!'];
                            return $response; exit;
                        }
                        $response = $this->placeBetMatchOdd($request->input(), $userInfo, $userBalArray->pName, $userBalance, $userProfitLossBalance, $userLimitArray, $odds_rate, $sportMarketData,$eventname);
                    }

                    if ($request->m_type == 'completed_match' || $request->m_type == 'tied_match') {
                        $response = $this->placeBetcompletedtiedMatch($request->input(), $userInfo, $userBalArray->pName, $userBalance, $userProfitLossBalance, $userLimitArray, $odds_rate, $sportMarketData,$eventname);
                    }


                    /* place bet fancy function */
                    if ($request->m_type == 'fancy') {
                        if ($marketDataArray->ballRunning == 1) {
                            $response = ["status" => 1, "code" => 200, "data" => null, "message" => 'Bet can not place in fancy due to ball running!!'];
                            return $response; exit;
                        }

                        $response = $this->placeBetFancy2($request->input(), $userInfo, $userBalArray->pName, $userBalance, $userProfitLossBalance, $userLimitArray, $sportMarketData,$odds_rate,$eventname);
                    }

                    if ( $request->m_type == 'fancy2' || $request->m_type == 'fancy3' || $request->m_type == 'oddeven') {
                        if ($marketDataArray->ball_running == 1) {
                            $response = ["status" => 1, "code" => 200, "data" => null, "message" => 'Bet can not place in fancy 2 & 3 due to ball running!!'];
                            return $response; exit;
                        }

                        $response = $this->placeBetFancy2($request->input(), $userInfo, $userBalArray->pName, $userBalance, $userProfitLossBalance, $userLimitArray, $sportMarketData,$odds_rate,$eventname);
                    }

                    if($request->m_type == 'USDINR' || $request->m_type == 'GOLD'|| $request->m_type == 'SILVER'|| $request->m_type == 'EURINR'|| $request->m_type == 'GBPINR'|| $request->m_type == 'ALUMINIUM' || $request->m_type == 'COPPER' || $request->m_type == 'CRUDEOIL' || $request->m_type == 'ZINC'|| $request->m_type == 'BANKNIFTY' || $request->m_type == 'NIFTY'){
                        if ($marketDataArray->ball_running == 1) {
                            $response = ["status" => 1, "code" => 200, "data" => null, "message" => 'Bet can not place in fancy 2 & 3 due to ball running!!'];
                            return $response; exit;
                        }

                        $response = $this->placeBetBinary($request->input(), $userInfo, $userBalArray->pName, $userBalance, $userProfitLossBalance, $userLimitArray, $sportMarketData,$odds_rate,$eventname);
                    }

                    /* place bet khado and ballbyball function */
                    if ( $request->m_type == 'meter' || $request->m_type == 'khado' || $request->m_type == 'ballbyball') {
                        // print_r($$request->m_type); die('jwwkjwje');
                        if ($marketDataArray->ball_running == 1) {
                            $response = ["status" => 1, "code" => 200, "data" => null, "message" => 'Bet can not place due to ball running!!'];
                            return $response; exit;
                        }

                        $response = $this->placeBetKhadoAndBallbyball($request->input(), $userInfo, $userBalArray->pName, $userBalance, $userProfitLossBalance, $userLimitArray, $sportMarketData,$eventname);
                    }

                    /* place bet Lottery */
                    if ($request->m_type == 'cricket_casino') {
                        $response = $this->placeBetLottery($request->input(), $userInfo, $userBalArray->pName, $userBalance, $userProfitLossBalance, $userLimitArray, $sportMarketData,$eventname);
                    }

                    /* place bet Jackpot */
                    if ($request->m_type == 'jackpot') {
                        $jackpotMarketData1 = $cache->get("Jackpot_" . $request->gameId . "_" . $request->market_id);
                        $jackpotMarketData1 = json_decode($jackpotMarketData1);
                        $secAyy1 = [];
                        if( !empty($jackpotMarketData1) ){
                            foreach ($jackpotMarketData1 as $jData1){
                                $secAyy1[] = $jData1->secId;
                                if( $jData1->secId == $request->sec_id ){
                                    if( ( (int)$request->rate-(int)$jData1->rate ) != 0 ){
                                        $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" => "Bed Request ! Bet can not placed !!"];
                                        return $response; exit;
                                    }
                                }
                            }
                        }

                        if( $secAyy1 != null && !in_array($request->sec_id,$secAyy1) ){
                            $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" => "Bed Request ! Bet can not placed !!"];
                            return $response; exit;
                        }
                        $response = $this->placeBetJackpot($request, $userInfo, $userBalArray->pName, $userBalance, $userProfitLossBalance, $userLimitArray, $sportMarketData,$eventname);
                    }

                }

                if( isset( $response ) && ( $response['status'] == 1 )
                    && ( $response['code'] == 200 ) && ( $response['data'] != null ) ){
                    $logdata['title'] = 'Bet Place';
                    $logdata['description'] = 'Bet Place Activity';
                    $logdata['request'] = $request->input();
                    $logdata['response'] = $response;
                    if( $currentOdds != null ){
                        $logdata['currentOdds'] = $currentOdds;
                    }
                    $this->addNotificationBetPlace($logdata);
                }

                return $response;
            }else{
                return $response;
            }
      }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }   
    }


    /* checkBetAccepted for all market */
    public function checkBetAccepted($request,$uid,$t1,$data,$sportMarket)
    {
      try{

        $request = (object) $request;
        $cache = Redis::connection();
        $maxDelay = 1000000;
        $maxDelay1 = 10;
        $t2 = round(microtime(true) * 1000);
        //check sport wise and market wise bet delay
        $betDelay = 0;
        $market = $cache->get("Market_".$request->market_id);
        if(!empty($market)){
            $market = json_decode($market);
            $betDelay = $market->bet_delay;

            /*$ctime = round(microtime(true) * 1000);
            if( $request->m_type == 'match_odd' || $request->m_type == 'completed_match' || $request->m_type == 'tied_match'){
                $sport_List= $cache->get('Sport_'.$request->sport_id);
                if(!empty($sport_List)){
                    $sport_List = json_decode($sport_List);
                   // $betDelay=$sport_List->bet_delay;
                }
            }*/

            if(!empty($sportMarket)) {
                if (!empty($sportMarket->bet_delay)) {
                    if($sportMarket->bet_delay > $betDelay){
                        $betDelay=$sportMarket->bet_delay;
                    }
                }
            }

            if( $betDelay != 0 ){
                $betDelay = ( $betDelay*1000000 - ( $t2-$t1 ) );
                usleep($betDelay);
            }else{
                $betDelay = 0;
            }
            $currentOdds = null;
            if ($request->m_type != 'cricket_casino') {
                $currentOdds = $marketData = $cache->get($request->market_id);
                $data = json_decode($marketData);
            }

            $ctime = time();
            if( $request->m_type == 'set_market' || $request->m_type == 'goals' || $request->m_type == 'winner' || $request->m_type == 'match_odd' || $request->m_type == 'completed_match' || $request->m_type == 'tied_match' ) {
                $dff = $ctime-$data->lastUpdatedTime;
                //echo "dff==".$dff."==ctime==>".$ctime."==maxDelay==".$maxDelay;
                if( $dff < $maxDelay1 ){
                    $rate = $diff = 0;
                    if (isset($data->lastUpdatedTime)) {
                        $diff = time() - $data->lastUpdatedTime;
                    }

                   // echo "testcdsfd555--->".$data->status;
                    if ($diff <= $betDelay && $data->status != 'SUSPENDED') {
                        // echo $diff."<---betdelay-->".$betDelay;
                        foreach ($data->odds as $odds) {
                            if ($odds->selectionId == $request->sec_id) {
                                if ($request->bet_type == 'back') {
                                    $rate = $odds->backPrice1;
                                }
                                if ($request->bet_type == 'lay') {
                                    $rate = $odds->layPrice1;
                                }
                            }
                        }

                        //if( $request->m_type != 'goals' ){
                            // rate not match 10-20
                            if( $request->sport_id != 4 ){ $rateDiff = 20; }else{ $rateDiff = 10; }
                            if( $request->bet_type == 'back' && ( trim($rate) - trim($request->rate) )*100 > $rateDiff ){
                                return [ 'is_true' => false , 'msg' => 'Bet can not place due to rate not match!!' ];  exit;
                            }
                            if( $request->bet_type == 'lay' && ( trim($request->rate) - trim($rate) )*100 > $rateDiff ){
                                return [ 'is_true' => false , 'msg' => 'Bet can not place due to rate not match!!' ];  exit;
                            }
                        //}

                        return ['is_true' => true, 'rate' => $rate, 'currentOdds' => $currentOdds];  exit;
                    }
                }else{
                    return [ 'is_true' => false , 'msg' => 'Bet can not place due to rate stuck for long time!' ];  exit;
                }
            }

            if( $request->m_type == 'fancy' ){
                $dff = $ctime-$data->time;
                if( $dff < $maxDelay ){
                    $rate = 0;
                    if( $request->bet_type == 'no' ){
                        $price = $data->data->no;
                        $rate = $data->data->no_rate;
                    }
                    if( $request->bet_type == 'yes' ){
                        $price = $data->data->yes;
                        $rate = $data->data->yes_rate;
                    }

                    if( ( $request->bet_type == 'no' && $request->rate == $rate && $request->price <= $price )
                        || ( $request->bet_type == 'yes' && $request->rate == $rate && $request->price >= $price ) ){
                        return [ 'is_true' => true , 'rate' => $rate , 'price' => $price, 'currentOdds' => $currentOdds ];  exit;
                    }else{
                        return [ 'is_true' => false , 'msg' => 'Bet can not placed !!1' ];  exit;
                    }
                }else{
                    return [ 'is_true' => false , 'msg' => 'Bet can not placed !!2' ];   exit;
                }
            }


            if($request->m_type == 'USDINR' || $request->m_type == 'GOLD'|| $request->m_type == 'SILVER'|| $request->m_type == 'EURINR'|| $request->m_type == 'GBPINR'|| $request->m_type == 'ALUMINIUM' || $request->m_type == 'COPPER' || $request->m_type == 'CRUDEOIL' || $request->m_type == 'ZINC'|| $request->m_type == 'BANKNIFTY' || $request->m_type == 'NIFTY'){
                $dff = $ctime-$data->time;
                if( $dff < $maxDelay ){
                    $rate = 0;
                    if( $request->bet_type == 'no' ){
                        $price = $data->no;
                        $rate = $data->no_rate;
                    }
                    if( $request->bet_type == 'yes' ){
                        $price = $data->yes;
                        $rate = $data->yes_rate;
                    }

                    //old no => $request->price <= $price  yes => $request->price >= $price

                    if( ( $request->bet_type == 'no' && $request->rate == $rate && $request->price == $price )
                        || ( $request->bet_type == 'yes' && $request->rate == $rate && $request->price == $price ) ){
                        return [ 'is_true' => true , 'rate' => $rate , 'price' => $price, 'currentOdds' => $currentOdds ];  exit;
                    }else{
                        return [ 'is_true' => false , 'msg' => 'Bet can not placed !!3' ];  exit;
                    }
                }else{
                    return [ 'is_true' => false , 'msg' => 'Bet can not placed !!4' ];   exit;
                }
            }

            if( $request->m_type == 'bookmaker' || $request->m_type == 'virtual_cricket' ){
                $runnersData = $data->runners;

                //$runnersData = json_decode($runnersData, True);
                $runners=null;
                if( $runnersData != null ){
                    foreach ( $runnersData as $runnersdata ){
                        $runners[$runnersdata->secId] = [
                            'lay' =>$runnersdata->lay,
                            'back' => $runnersdata->back,
                            'suspended' => $runnersdata->suspended,
                            //'ballrunning' => $runnersdata->ballrunning,
                        ];
                    }
                }
                if($runners && $request->m_type == 'bookmaker') {
                    if ( isset($runners[$request->sec_id])
                        && ( $runners[$request->sec_id]['suspended'] == 1 ) ) {
                        return ['is_true' => false, 'msg' => 'Bet can not place due to market suspended!'];
                    }
                    if ($request->bet_type == 'lay') {
                        // old && trim((int)$runners[$request->sec_id]['lay']) <= trim((int)$request->price)
                        if (isset($runners[$request->sec_id])
                            && trim((int)$runners[$request->sec_id]['lay']) != 0
                            && trim((int)$runners[$request->sec_id]['lay']) == trim((int)$request->price)) {
                            return ['is_true' => true, 'rate' => $request->rate, 'price' => $request->price, 'currentOdds' => $currentOdds];
                        } else {
                            return ['is_true' => false, 'msg' => 'Rate changed! Bet can not placed !!'];
                        }
                    } else {
                        // old && trim((int)$runners[$request->sec_id]['back']) >= trim((int)$request->price)
                        if (isset($runners[$request->sec_id])
                            && trim((int)$runners[$request->sec_id]['back']) != 0
                            && trim((int)$runners[$request->sec_id]['back']) == trim((int)$request->price)) {
                            return ['is_true' => true, 'rate' => $request->rate, 'price' => $request->price, 'currentOdds' => $currentOdds];
                        } else {
                            return ['is_true' => false, 'msg' => 'Rate changed! Bet can not placed !!'];
                        }
                    }
                }

                if($runners && $request->m_type == 'virtual_cricket') {
                    if ( isset($runners[$request->sec_id])
                        && ( $runners[$request->sec_id]['suspended'] == 1 ) ) {
                        return ['is_true' => false, 'msg' => 'Bet can not place due to market suspended!'];
                    }
                    if ($request->bet_type == 'lay') {
                        if (isset($runners[$request->sec_id])
                            && trim($runners[$request->sec_id]['lay']) == trim($request->price)) {
                            return ['is_true' => true, 'rate' => $request->rate, 'price' => $request->price, 'currentOdds' => $currentOdds];
                        } else {
                            return ['is_true' => false, 'msg' => 'Rate changed! Bet can not placed !!'];
                        }
                    } else {
                        if (isset($runners[$request->sec_id])
                            && trim($runners[$request->sec_id]['back']) == trim($request->price)) {
                            return ['is_true' => true, 'rate' => $request->rate, 'price' => $request->price, 'currentOdds' => $currentOdds];
                        } else {
                            return ['is_true' => false, 'msg' => 'Rate changed! Bet can not placed !!'];
                        }
                    }
                }
            }

            if($request->m_type == 'meter' || $request->m_type == 'fancy2'){
                if( $data->suspended == 1 || $data->ball_running == 1 ){
                    return [ 'is_true' => false , 'msg' => 'Bet can not place due to market suspended or ball running!' ];  exit;
                }

                if ($request->bet_type == 'no') {
                    // old condition ( trim( (int)$data->no ) >= trim( (int)$request->price )
                    if( ( trim( (int)$data->no ) == trim( (int)$request->price ) )
                        && ( trim( (int)$data->no_rate ) == trim( (int)$request->rate ) ) ){
                        return [ 'is_true' => true , 'rate' => $request->rate , 'price' => $request->price, 'currentOdds' => $currentOdds ];
                    }else{
                        return [ 'is_true' => false , 'msg' => 'Rate changed! Bet can not placed !!' ];
                    }
                }else{
                    // old condition ( trim( (int)$data->yes ) <= trim( (int)$request->price )
                     if( ( trim( (int)$data->yes ) == trim( (int)$request->price ) )
                        && ( trim( (int)$data->yes_rate ) == trim( (int)$request->rate ) ) ){
                        return [ 'is_true' => true , 'rate' => $request->rate , 'price' => $request->price, 'currentOdds' => $currentOdds ];
                    }else{
                        return [ 'is_true' => false , 'msg' => 'Rate changed! Bet can not placed !!' ];
                    }

                }
            }

            if($request->m_type == 'ballbyball'){
                if( $data->suspended == 1 || $data->ball_running == 1 ){
                    return [ 'is_true' => false , 'msg' => 'Bet can not place due to market suspended or ball running!' ];  exit;
                }

                if ($request->bet_type == 'no') {
                    if( ( trim( (int)$data->no ) == trim( (int)$request->price ) )
                        && ( trim( (int)$data->no_rate ) == trim( (int)$request->rate ) ) ){
                        return [ 'is_true' => true , 'rate' => $request->rate , 'price' => $request->price, 'currentOdds' => $currentOdds ];
                    }else{
                        return [ 'is_true' => false , 'msg' => 'Rate changed! Bet can not placed !!' ];
                    }
                }else{
                    if( ( trim( (int)$data->yes ) == trim( (int)$request->price ) )
                        && ( trim( (int)$data->yes_rate ) == trim( (int)$request->rate ) ) ){
                        return [ 'is_true' => true , 'rate' => $request->rate , 'price' => $request->price, 'currentOdds' => $currentOdds ];
                    }else{
                        return [ 'is_true' => false , 'msg' => 'Rate changed! Bet can not placed !!' ];
                    }

                }
            }

              if($request->m_type == 'khado'){
                  if( $data->suspended == 1 || $data->ball_running == 1 ){
                      return [ 'is_true' => false , 'msg' => 'Bet can not place due to market suspended or ball running!' ];  exit;
                  }
                if ($request->bet_type == 'no') {
                    if(  trim( (int)$data->no_rate ) == trim( (int)$request->rate ) ){
                        return [ 'is_true' => true , 'rate' => $request->rate , 'price' => $request->price, 'currentOdds' => $currentOdds ];
                    }else{
                        return [ 'is_true' => false , 'msg' => 'Rate changed! Bet can not placed !!' ];
                    }
                }else{
                     if(  trim( (int)$data->yes_rate ) == trim( (int)$request->rate ) ){
                        return [ 'is_true' => true , 'rate' => $request->rate , 'price' => $request->price, 'currentOdds' => $currentOdds ];
                    }else{
                        return [ 'is_true' => false , 'msg' => 'Rate changed! Bet can not placed !!' ];
                    }

                }
            }

            if($request->m_type == 'oddeven' ){
                if( $data->suspended == 1 || $data->ball_running == 1 ){
                    return [ 'is_true' => false , 'msg' => 'Bet can not place due to market suspended or ball running!' ];  exit;
                }
                if ($request->bet_type == 'back') {
                    if( ( trim( $data->odd ) == trim( $request->price ) ) ){
                        return [ 'is_true' => true , 'rate' =>$request->price , 'price' => $request->price, 'currentOdds' => $currentOdds ];
                    }else{
                        return [ 'is_true' => false , 'msg' => 'Rate changed! Bet can not placed !!' ];
                    }
                }else{
                    if( ( trim( $data->even ) == trim( $request->price ) ) ){
                        return [ 'is_true' => true , 'rate' => $request->price , 'price' => $request->price, 'currentOdds' => $currentOdds ];
                    }else{
                        return [ 'is_true' => false , 'msg' => 'Rate changed! Bet can not placed !!' ];
                    }
                }
            }

            if( $request->m_type == 'fancy3' ){
                if( $data->suspended == 1 || $data->ball_running == 1 ){
                    return [ 'is_true' => false , 'msg' => 'Bet can not place due to market suspended or ball running!' ];  exit;
                }

                if ($request->bet_type == 'back') {
                    if( ( trim( $data->back ) == trim( $request->price ) ) ){
                        return [ 'is_true' => true , 'rate' =>$request->price , 'price' => $request->price, 'currentOdds' => $currentOdds ];
                    }else{
                        return [ 'is_true' => false , 'msg' => 'Rate changed! Bet can not placed !!' ];
                    }
                }else{
                    if( ( trim( $data->lay ) == trim( $request->price ) ) ){
                        return [ 'is_true' => true , 'rate' => $request->price , 'price' => $request->price, 'currentOdds' => $currentOdds ];
                    }else{
                        return [ 'is_true' => false , 'msg' => 'Rate changed! Bet can not placed !!' ];
                    }
                }
            }


            //if( $request->m_type == 'jackpot' ){ }

            if($request->m_type == 'cricket_casino'){
                return [ 'is_true' => true , 'rate' => $request->rate , 'price' => $request->price ];
            }

            return [ 'is_true' => false , 'msg' => 'Bet can not placed !!5' ];

        }else{
            return [ 'is_true' => false , 'msg' => 'Bet can not placed !!5' ];
        }

      }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }   


    }


    /* bet place function for match Odd and bookmaker */
    public function placeBetMatchOdd($request,$userInfo,$parentName,$userBalance,$userProfitLossBalance,$userLimitArray, $odds_rate,$sportMarket,$eventname)
    {

        try{
        
        $sportArr = ['1' => 'Football' , '2' => 'Tennis' , '4' => 'Cricket' , '6' => 'Election' , '10' => 'Jackpot' ,'11' => 'lottery',
        '998917' => 'Volleyball' , '7522' => 'basketball' , '3503' => 'Dart' , '22' => 'Table tennis' , '13' => 'badminton' ,'14' => 'Kabaddi','16' => 'Boxing'];
        $uid=$userInfo->id;
    //  print_r($uid); die();
         $cache = Redis::connection();
        $eventData=$cache->get("Market_".$request['market_id']);
        $rsArray = json_decode($eventData);
        $t2 = round(microtime(true) * 1000);
            $redisMarketName=$request['market_name'];
            if(!empty($rsArray)) {
                $redisMarketName= $rsArray->title;
            }
        $runnername=$request['runner'];
        if(!empty($rsArray)) {
            $runnerssArray = json_decode($rsArray->runners);
            foreach ($runnerssArray as $redismarket_Data) {
                if ($redismarket_Data->secId == $request['sec_id']) {
                    $runnername = $redismarket_Data->runner;
                }
            }
        }

         if(!empty($sportMarket)) {
             if (!empty($sportMarket)) {
                 if ($sportMarket->min_stack > $request['size'] && $sportMarket->min_stack != 0) {
                     $response = ["status" => 1, "code" => 200, "data" => null, "message" => 'Minimum stack value is ' . $sportMarket->min_stack];
                     return $response; exit;
                 }

                 if ($sportMarket->max_stack < $request['size'] && $sportMarket->max_stack != 0) {
                     $response = ["status" => 1, "code" => 200, "data" => null, "message" => 'Maximum stack value is ' . $sportMarket->max_stack];
                     return $response; exit;
                 }

             }
         }

        //echo $updated_on; exit();
        if( $request['m_type'] == 'goals' || $request['m_type'] == 'set_market' ){
            $deleteQuery = DB::table('tbl_bet_pending_6')
                ->where([['uid',$uid],['status',5]])->delete();
            $model= new GoalsMakerFunction();
        }else{
            $deleteQuery = DB::table('tbl_bet_pending_1')
                ->where([['uid',$uid],['status',5]])->delete();
            $model= new MatchOddFunction();
        }

        $model->systemId = $userInfo->systemId;
        $model->uid = $uid;
        $model->sid = $request['sport_id'];
        $model->eid = $request['event_id'];
        $model->mid = $request['market_id'];
        $model->price = $request['price'];
        $model->size = $request['size'];
        $model->bType = $request['bet_type'];
        $model->rate =  $odds_rate;
        $model->mType = $request['m_type'];
        $model->runner = $runnername;
        $model->market = $redisMarketName;
        $model->event = $eventname;
        $model->ip_address =  $this->get_client_ip();
        $game_type = $sportArr[$request['sport_id']];
        $model->client = $userInfo->username;
        $model->master = $parentName;
        $model->description = $game_type.' > '.$model->event.' > '.$redisMarketName.' > '.$model->runner;
        $model->sport = $game_type;
        $model->secId = $request['sec_id'];
        $model->diff = 0;
        $model->is_match=0;
       

        if( $request['m_type'] == 'bookmaker' ){
            if( $request['bet_type'] == 'back' ){
               // $model->win = ( $model->size * $model->price )/100;
                $model->win = ($request['size'] * $request['price'] ) / 100;

                $model->loss = $request['size'];
            }else{
                $model->win = $request['size'];
                $model->loss = ($request['size'] * $request['price'] ) / 100;
               // $model->loss = ($request['price'] * $request['size'])/100;
            }
            $model->is_match = 1;
        }else  if($request['m_type'] == 'virtual_cricket' ){

                if( $request['price'] > 1 ){
                    $model->win = ($model->price-1) * $request['size'];
                }else{
                    $model->win = 0;
                }

                $model->loss = $request['size'];
                $model->is_match = 1;
        }
        else{
            if( $request['bet_type']== 'back' && trim($model->rate) >= trim($request['price']) ){
                $model->is_match = 1;
                $model->price = $model->rate;
            }
            if( $request['bet_type'] == 'lay' && trim($model->rate) <= trim($request['price']) ){
                $model->is_match = 1;
                $model->price = $model->rate;
            }

            if( $request['bet_type'] == 'back' ){
                if( $request['price'] > 1 ){
                    $model->win = ($model->price-1) * $request['size'];
                }else{
                    $model->win = 0;
                }
                $model->loss = $request['size'];
            }else{
                $model->win = $request['size'];
                if( $request['price'] > 1 ){
                    $model->loss = ($model->price-1) * $request['size'];
                }else{
                    $model->loss = $request['size'];
                }
            }


            if( $model->is_match != 1){
                $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" => "Odd change, unmatch bet can not accepted."];
                return $response; exit;
            }

            // check accept_unmatch_bet and max_odd_limit
            $eventData = $cache->get("Event_".$request['event_id']);
            $eventCheck = json_decode($eventData);
            if( $eventCheck != null ){
                if( $model->is_match != 1 && $eventCheck->unmatch_bet_allowed != 1 ){
                    $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" => "Odd change, unmatch bet can not accepted."];
                    return $response; exit;
                }
            }


           /* $query = DB::table('tbl_event_limits')->select('*')
                ->where('eid',$request['event_id']);
            $event = $query->first();*/
            $event= $cache->get("Event_limit_".$request['event_id']);
            $event = json_decode($event);
            //print_r($event); exit;
            if( $event != null ){
                if($odds_rate > $event->max_odd_limit ){
                    $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" => "Your max odd limit is ".$event->max_odd_limit."! Bet can not placed!!"];
                    return $response; exit;
                }
            }

            if( $request['m_type'] == 'set_market' || $request['m_type'] == 'goals' || $request['m_type'] == 'winner' || $request['m_type'] == 'match_odd' ){
                if( $eventCheck->type == 'IN_PLAY' ){
                    if ($rsArray->min_stack > $request['size']) {
                        $response = ["status" => 1, "code" => 200, "data" => null, "message" => 'Minimum stack value is ' . $rsArray->min_stack];
                        return $response; exit;
                    }
                    if ($rsArray->max_stack < $request['size']) {
                        $response = ["status" => 1, "code" => 200, "data" => null, "message" => 'Maximum stack value is ' . $rsArray->max_stack];
                        return $response; exit;
                    }
                  /*  if( $event->max_profit < $model->win ){
                        $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" =>  'Maximum profit value is'.$event->max_profit];
                        return $response; exit;
                    }
                    if( $event->min_stack < $model->size ){
                        $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" =>  'Minimum stack value is '.$event->min_stack];
                        return $response; exit;
                    }
                    if( $event->max_stack < $model->size ){
                        $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" =>  'Maximum stack value is'.$event->max_stack];
                        return $response; exit;
                    }*/
                    if( $event->overall_profit_limit < $model->win ){
                        $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" =>  'Maximum profit value is '.$event->overall_profit_limit];
                        return $response; exit;
                    }
                }else{

                    if( $event->overall_profit_limit < $model->win ){
                        $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" =>  'Maximum profit value is '.$event->overall_profit_limit];
                        return $response; exit;
                    }

                    if( $event->upcoming_min_stake > $model->size ){
                        $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" =>  'Minimum stack value is '.$event->upcoming_min_stake];
                        return $response; exit;
                    }
                    if( $event->upcoming_max_stake < $model->size ){
                        $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" =>  'Maximum stack value is '.$event->upcoming_max_stake];
                        return $response; exit;
                    }
                   // echo "test".$event->upcoming_min_stake; exit;
                }
            }
        }

        if(!empty($rsArray) ){
            if( $request['m_type'] == 'bookmaker' || $request['m_type'] == 'virtual_cricket') {
                if ($rsArray->min_stack > $request['size']) {
                    $response = ["status" => 1, "code" => 200, "data" => null, "message" => 'Minimum stack value is ' . $rsArray->min_stack];
                    return $response; exit;
                }
                if ($rsArray->max_stack < $request['size']) {
                    $response = ["status" => 1, "code" => 200, "data" => null, "message" => 'Maximum stack value is ' . $rsArray->max_stack];
                    return $response; exit;
                }
            }
        }else{
            $response =  [ "status" => 0 ,"code" => 400 ,"data" => null, "message" => "Bet can not placed ss !!"];
            return $response; exit;
        }

        if( $request['m_type'] == 'match_odd' || $request['m_type'] == 'goals' || $request['m_type'] == 'set_market' ){
            if( $request['bet_type'] == 'back' ){
                $model->ccr = round ( ( ($model->win) * $this->clientCommissionRate() )/100 );
            }else{
                $model->ccr = round ( ( ($model->size) * $this->clientCommissionRate() )/100 );
            }
        }else{
            $model->ccr = 0;
        }


        $model->save();


        $betId = $model->id;
        $oldMarketExpose = $newMarketExpose = $pendingMarketExpose = $exposeBalance = $newMarketProfit = 0;

        //OLD EXPOSE CODE
        /*$userExpose = DB::table('tbl_user_market_expose')
            ->where([['uid',$uid],['status',1],['mid','!=',$request['market_id']]])->sum("expose");

        if(!empty($userExpose>0)){
            $balExpose = (-1)*($userExpose);
            if( $balExpose != null ){
                $oldMarketExpose = $balExpose;
            }
        }*/

        $oldMarketExpose = $this->getOldMarketExpose($uid, $request['market_id']);
        $getUserData = $this->checkExposeMatchOddBalance($uid,$request);
        $newMarketExpose = $getUserData['expose'];
        $newMarketProfit = $getUserData['profit'];
        $newRunnersExpose = $getUserData['runnersExpose'];

        $exposeBalance = $oldMarketExpose+$newMarketExpose;

        $userAvailableBalance = 0;
        if($userProfitLossBalance > 0){
            $userAvailableBalance = ( $userBalance + $userProfitLossBalance );
        }else{
            $userAvailableBalance = ( $userBalance -(-1)*$userProfitLossBalance );
        }

        if($userAvailableBalance > 0){
            $userAvailableBalance = ( $userAvailableBalance -(-1)*$exposeBalance);
        }

        if ( round($userAvailableBalance) < 0 ) {
            if( $request['m_type'] == 'set_market' || $request['m_type'] == 'goals'){
                $deleteQuery = DB::table('tbl_bet_pending_6')->where([['id',$betId]])->delete();
            }else{
                $deleteQuery = DB::table('tbl_bet_pending_1')->where([['id',$betId]])->delete();
            }
            $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" => 'Insufficient funds!!!'];
            return $response; exit;

        }else{
            $checkProfitLimit = $this->checkMaxProfitLimitNew2($uid,$newMarketProfit,$request['m_type'],$request['event_id'],$request['market_id'],$request['sport_id'],$userLimitArray,$newMarketExpose);
            if( ( $checkProfitLimit['is_true'] == false ) ){
                if( $request['m_type'] == 'set_market' || $request['m_type'] == 'goals' ){
                    $deleteQuery = DB::table('tbl_bet_pending_6')->where([['id',$betId]])->delete();
                }else{
                    $deleteQuery = DB::table('tbl_bet_pending_1')->where([['id',$betId]])->delete();
                }
                $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" =>$checkProfitLimit['msg']];
                return $response; exit;
            }else{

                $betDataBook = $this->setBookData($uid,$model);
                $updated_on = date('Y-m-d H:i:s');
                $dataArr = []; $updatedTime = time();
                $exposeBalance = (-1)*($exposeBalance);
                //   echo "exposeBalance==>".  $exposeBalance exit;
                if( $request['m_type'] == 'set_market' || $request['m_type'] == 'goals' ){
                    DB::table('tbl_bet_pending_6')->where('id',$betId)->update(['status' => 1, 'created_on' => $updated_on, 'updated_on' => $updated_on]);
                }else{
                    DB::table('tbl_bet_pending_1')->where('id',$betId)->update(['status' => 1, 'created_on' => $updated_on, 'updated_on' => $updated_on]);
                }
                $dataArr = [ "balance" => round($userAvailableBalance) , "expose" =>  round($exposeBalance) , "mywallet" => round($userBalance) , "updated_time" => $updatedTime ];

                DB::table('tbl_user_info')->where('uid',$uid)->update(['expose'=>$exposeBalance,'updated_on'=>$updated_on]);
                if(!empty($newRunnersExpose)){
                    foreach ( $newRunnersExpose as $key => $rnvalue) {
                        $this->updateUserBetExpose( $uid,$request['event_id'], $request['market_id'],$rnvalue['totalExpose'],$rnvalue['totalWin'],$rnvalue['totalLossEx'],$rnvalue['sec_id'] ,$request['m_type'],$request['sport_id'] );
                    }
                }


                $market_id=$request['market_id'];
                $this->updateUserExposeAndProfit( $uid,$request['event_id'],$market_id,$newMarketExpose,$newMarketProfit,$request['sport_id'],$request['m_type'],$request['sec_id'],$userInfo->systemId);

                $msg = "Bet ".$request['bet_type']." RUN,</br>Placed ".$request['size']." @ ".$model->price." Odds </br> Matched ".$request['size']." @ ".$request['rate']." Odds";
                $data=array('balance'=>$dataArr,'betId'=>md5($betId));
                $response =  [ "status" => 1 ,"code" => 200 ,"data"=>$data, "message" =>$msg];

                return $response; exit;
            }


            return $response; exit;
        }

      }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }   

    }

     /* bet place function for completed and Tied Match */
    public function placeBetcompletedtiedMatch($request,$userInfo,$parentName,$userBalance,$userProfitLossBalance,$userLimitArray, $odds_rate,$sportMarket,$eventname)
    {
        try{
        
        $sportArr = ['1' => 'Football' , '2' => 'Tennis' , '4' => 'Cricket' , '6' => 'Election' , '10' => 'Jackpot' ,'11' => 'lottery',
            '998917' => 'Volleyball' , '7522' => 'basketball' , '3503' => 'Dart' , '22' => 'Table tennis' , '13' => 'badminton' ,'14' => 'Kabaddi','16' => 'Boxing'];
        $uid=$userInfo->id;
      
         $cache = Redis::connection();
        $eventData=$cache->get("Market_".$request['market_id']);
        $rsArray = json_decode($eventData);
            $redisMarketName=$request['market_name'];
            if(!empty($rsArray)) {
                $redisMarketName= $rsArray->title;
            }
            $runnername=$request['runner'];
            if(!empty($rsArray)) {
                $runnerssArray = json_decode($rsArray->runners);
                foreach ($runnerssArray as $redismarket_Data) {
                    if ($redismarket_Data->secId == $request['sec_id']) {
                        $runnername = $redismarket_Data->runner;
                    }
                }
            }

        if(!empty($sportMarket)) {
            if (!empty($sportMarket)) {
                if ($sportMarket->min_stack > $request['size'] && $sportMarket->min_stack != 0) {
                    $response = ["status" => 1, "code" => 200, "data" => null, "message" => 'Minimum stack value is.. ' . $sportMarket->min_stack];
                    return $response; exit;
                }

                if ($sportMarket->max_stack < $request['size'] && $sportMarket->max_stack != 0) {
                    $response = ["status" => 1, "code" => 200, "data" => null, "message" => 'Maximum stack value is ' . $sportMarket->max_stack];
                    return $response; exit;
                }
            }
        }


        if(!empty($rsArray)){   
            if( $rsArray->min_stack > $request['size'] ){
                $response =  [ "status" => 1 ,"code" => 200 ,"data"=>null, "message" => 'Minimum stack value is '.$rsArray->min_stack];
                return $response; exit;
            }
            if( $rsArray->max_stack < $request['size']){
                $response =  [ "status" => 1 ,"code" => 200 ,"data"=>null, "message" => 'Maximum stack value is '.$rsArray->max_stack ];
                return $response; exit;
            }

        }else{

            $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" => "Bet can not placed ss !!"];
            return $response; exit;
        }
 
        $deleteQuery = DB::table('tbl_bet_pending_1')->where([['uid',$uid],['status',5]])->delete();
        $model= new BetFairFunction();
        $model->systemId = $userInfo->systemId;
        $model->uid = $uid;
        $model->sid = $request['sport_id'];
        $model->eid = $request['event_id'];
        $model->mid = $request['market_id'];
        $model->price = $request['price'];
        $model->size = $request['size'];
        $model->bType = $request['bet_type'];
        $model->rate = $odds_rate;
        $model->mType = $request['m_type'];
        $model->market =$redisMarketName;
        $model->event = $eventname;
        $model->ip_address =  $this->get_client_ip();
        $game_type = $sportArr[$request['sport_id']];
        $model->client = $userInfo->username;
        $model->master = $parentName;
        $model->description = $game_type.' > '.$model->event.' > '.$redisMarketName.' > '.$model->runner;
        $model->sport = $game_type;
        $model->secId = $request['sec_id'];
        $model->diff = 0;
        $model->is_match=0;
        if($request['m_type'] == 'completed_match'){
             $model->runner = $runnername.'-completed';
        }else{
           $model->runner = $runnername.'-tied';
        }

        
            if( $request['bet_type']== 'back' && trim( $model->rate) >= trim($request['price']) ){
                $model->is_match = 1;
                $model->price =  $model->rate;
            }
            if( $request['bet_type'] == 'lay' && trim( $model->rate) <= trim($request['price']) ){
                $model->is_match = 1;
                $model->price =  $model->rate;
            }

            if( $request['bet_type'] == 'back' ){
                if( $request['price'] > 1 ){
                    $model->win = ($model->price-1) * $request['size'];
                }else{
                    $model->win = 0;
                }
                $model->loss = $request['size'];
            }else{
                $model->win = $request['size'];
                if( $request['price'] > 1 ){
                    $model->loss = ($model->price-1) * $request['size'];
                }else{
                    $model->loss = $request['size'];
                }
            }

        if( $model->is_match != 1){
            $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" => "Odd change, unmatch bet can not accepted."];
            return $response; exit;
        }

            $eventData=$cache->get("Event_".$request['event_id']);
            $eventCheck = json_decode($eventData);
            if( $eventCheck != null ){
                if( $model->is_match != 1 && $eventCheck->unmatch_bet_allowed != 1 ){
                    $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" => "Odd change, unmatch bet can not accepted."];
                    return $response; exit;
                }
            }

            $event= $cache->get("Event_limit_".$request['event_id']);
            $event = json_decode($event);
            //print_r($event); exit;
            if( $event != null ){
                if($odds_rate > $event->max_odd_limit ){
                    $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" => "Your max odd limit is" .$event->max_odd_limit." Bet can not placed!!"];
                    return $response; exit;
                }
            }

            if( $eventCheck->type == 'IN_PLAY' ){
                if( $event->overall_profit_limit < $model->win ){
                    $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" =>  'Maximum profit value is'.$event->upcoming_max_profit];
                    return $response; exit;
                }
              /*  if( $event->max_profit < $model->win ){
                    $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" =>  'Maximum profit value is'.$event->max_profit];
                    return $response; exit;
                }
                if( $event->min_stack < $model->size ){
                    $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" =>  'Minimum stack value is '.$event->min_stack];
                    return $response; exit;
                }
                if( $event->max_stack < $model->size ){
                    $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" =>  'Maximum stack value is'.$event->max_stack];
                    return $response; exit;
                }*/
            }else{

                if( $event->overall_profit_limit < $model->win ){
                    $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" =>  'Maximum profit value is '.$event->upcoming_max_profit];
                    return $response; exit;
                }

                if( $event->upcoming_min_stake > $model->size ){
                    $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" =>  'Minimum stack value is '.$event->upcoming_min_stake];
                    return $response; exit;
                }
                if( $event->upcoming_max_stake < $model->size ){
                    $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" =>  'Maximum stack value is '.$event->upcoming_max_stake];
                    return $response; exit;
                }

            }

        if( $request['m_type'] == 'completed_match' || $request['m_type'] == 'tied_match' ){
            if( $request['bet_type'] == 'back' ){

                $model->ccr = round ( ( ($model->win) * $this->clientCommissionRate() )/100 );
            }else{
                $model->ccr = round ( ( ($model->size) * $this->clientCommissionRate() )/100 );
            }
        }else{
            $model->ccr = 0;
        }


        $model->save();


        $betId = $model->id;
        $oldMarketExpose = $newMarketExpose = $pendingMarketExpose = $exposeBalance = $newMarketProfit = 0;

        // OLD EXPOSE CODE
        /*$userExpose = DB::table('tbl_user_market_expose')
            ->where([['uid',$uid],['status',1],['mid','!=',$request['market_id']]])->sum("expose");

        if(!empty($userExpose>0)){
            $balExpose = (-1)*($userExpose);
            if( $balExpose != null ){
                $oldMarketExpose = $balExpose;
            }
        }*/

        $oldMarketExpose = $this->getOldMarketExpose($uid, $request['market_id']);
        $getUserData = $this->checkExposeMatchOddBalance($uid,$request);
        $newMarketExpose = $getUserData['expose'];
        $newMarketProfit = $getUserData['profit'];
        $newRunnersExpose = $getUserData['runnersExpose'];
        $exposeBalance = $oldMarketExpose+$newMarketExpose;

        $userAvailableBalance = 0;
        if($userProfitLossBalance > 0){
            $userAvailableBalance = ( $userBalance + $userProfitLossBalance );
        }else{
            $userAvailableBalance = ( $userBalance -(-1)*$userProfitLossBalance );
        }

        if($userAvailableBalance > 0){
            $userAvailableBalance = ( $userAvailableBalance -(-1)*$exposeBalance);
        }

        if ( round($userAvailableBalance) < 0 ) {
            $deleteQuery = DB::table('tbl_bet_pending_1')->where([['id',$betId]])->delete();
            $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" => 'Insufficient funds!!!'];
            return $response; exit;

        }else{
            $checkProfitLimit = $this->checkMaxProfitLimitNew2($uid,$newMarketProfit,$request['m_type'],$request['event_id'],$request['market_id'],$request['sport_id'],$userLimitArray,$newMarketExpose);
            if( ( $checkProfitLimit['is_true'] == false ) ){
                $deleteQuery = DB::table('tbl_bet_pending_1')->where([['id',$betId]])->delete();
                $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" =>$checkProfitLimit['msg']];
                return $response; exit;
            }else{
                $betDataBook = $this->setBookData($uid,$model);
                $updated_on = date('Y-m-d H:i:s');
                $dataArr = []; $updatedTime = time();
                $exposeBalance = (-1)*($exposeBalance);
                //   echo "exposeBalance==>".  $exposeBalance exit;
                DB::table('tbl_bet_pending_1')->where('id',$betId)->update(['status' => 1, 'created_on' => $updated_on, 'updated_on' => $updated_on]);
                $dataArr = [ "balance" => round($userAvailableBalance) , "expose" =>  round($exposeBalance) , "mywallet" => round($userBalance) , "updated_time" => $updatedTime ];
                $updated_on = date('Y-m-d H:i:s');
                DB::table('tbl_user_info')->where('uid',$uid)->update(['expose'=>$exposeBalance,'updated_on'=>$updated_on]);
                if(!empty($newRunnersExpose)){
                    foreach ( $newRunnersExpose as $key => $rnvalue) {
                        $this->updateUserBetExpose( $uid,$request['event_id'], $request['market_id'],$rnvalue['totalExpose'],$rnvalue['totalWin'],$rnvalue['totalLossEx'],$rnvalue['sec_id'] ,$request['m_type'],$request['sport_id'] );
                    }
                }


                $market_id=$request['market_id'];
                $this->updateUserExposeAndProfit( $uid,$request['event_id'],$market_id,$newMarketExpose,$newMarketProfit,$request['sport_id'],$request['m_type'],$request['sec_id'],$userInfo->systemId);

                $msg = "Bet ".$request['bet_type']." RUN,</br>Placed ".$request['size']." @ ".$model->price." Odds </br> Matched ".$request['size']." @ ".$request['rate']." Odds";
                $data=array('balance'=>$dataArr,'betId'=>md5($betId));
                $response =  [ "status" => 1 ,"code" => 200 ,"data"=>$data, "message" =>$msg];
                return $response; exit;

            }

            return $response; exit;
        }
      }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }   

    }


    // setBookData
  public function setBookData($uid,$betData)
  {

    try{
            $redis = Redis::connection();
            $bookMakerJson = $redis->get("Market_" . $betData->mid);
            $bookMaker = json_decode($bookMakerJson);

            if($betData->mType == 'bookmaker' || $betData->mType == 'virtual_cricket'){
                $table = 'tbl_book_bookmaker';
            }else{
                $table = 'tbl_book_matchodd';
            }

            if( $bookMaker != null && isset( $bookMaker->runners )){

            $eventId = $bookMaker->eid;
            $marketId = $bookMaker->mid;

            $runners = json_decode($bookMaker->runners);

            if( $runners != null ){
            $i = 0; $bookDataArr = [];
            foreach ( $runners as $runner ){

            $bookDataArr[$i] = [
            'uid' => $betData->uid,
            'eid' => $eventId,
            'mid' => $marketId,
            ];

     if($betData->mType == 'bookmaker'){
            if( $betData->bType == 'lay' ){

            if( $runner->secId == $betData->secId ){
            $book = ( $betData->rate*$betData->size )/100;
            $bookDataArr[$i]['secId'] = $runner->secId;
            $bookDataArr[$i]['book'] = round( (-1)*$book , 2);
            }else{
            $book = $betData->size;
            $bookDataArr[$i]['secId'] = $runner->secId;
            $bookDataArr[$i]['book'] = round( $book, 2);
            }

            }else{

            if( $runner->secId == $betData->secId ){
            $book = ( $betData->rate*$betData->size )/100;
            $bookDataArr[$i]['secId'] = $runner->secId;
            $bookDataArr[$i]['book'] = round( $book, 2);
            }else{
            $book = $betData->size;
            $bookDataArr[$i]['secId'] = $runner->secId;
            $bookDataArr[$i]['book'] = round( (-1)*$book , 2);
            }

            }
        }else{
             if( $betData->bType == 'lay' ){

            if( $runner->secId == $betData->secId ){
            $book = ( $betData->rate - 1 )*$betData->size;
            $bookDataArr[$i]['secId'] = $runner->secId;
            $bookDataArr[$i]['book'] = round( (-1)*$book , 2);
            }else{
            $book = $betData->size;
            $bookDataArr[$i]['secId'] = $runner->secId;
            $bookDataArr[$i]['book'] = round( $book, 2);
            }

            }else{

            if( $runner->secId == $betData->secId ){
            $book = ( $betData->rate - 1 )*$betData->size;
            $bookDataArr[$i]['secId'] = $runner->secId;
            $bookDataArr[$i]['book'] = round( $book, 2);
            }else{
            $book = $betData->size;
            $bookDataArr[$i]['secId'] = $runner->secId;
            $bookDataArr[$i]['book'] = round( (-1)*$book , 2);
            }

            }
        }


            $i++;
            }

            if( $bookDataArr != null ){

            $book = DB::table($table)->select(['book'])
            ->where([['uid',$uid],['eid',$eventId],['mid',$marketId]])->first();

            if( $book != null ){

            foreach ( $bookDataArr as $bookData ){
            $where = [['uid',$uid],['eid',$eventId],['mid',$marketId],['secId',$bookData['secId']]];
            $bookCheck = DB::table($table)->select(['book'])->where($where)->first();
            if( $bookCheck != null ){
            $newBook = round( ($bookCheck->book+$bookData['book']) , 2 );
            DB::table($table)->where($where)->update(['book'=>$newBook]);
            }else{
            DB::table($table)->insert($bookData);
            }
            }

            }else{
            DB::table($table)->insert($bookDataArr);
            }


            }


            }
            }
        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }   

}


    //getBalanceVal
    public function checkExposeMatchOddBalance($uid, $request)
    {
      try{

        $m_type = $request['m_type'];
        $marketId = $request['market_id'];
        $eventId = $request['event_id'];
        $resultArray = [];
        $dataExpose = $maxBal['expose'] = $maxBal['plus'] = $balPlus = $balPlus1 = $balExpose = $balExpose2 = $profitLossNew = [];
        $minExpose = 0;   $maxProfit = 0;

        $cache = Redis::connection();
        $runnersData = $cache->get("Market_".$marketId);
        $runnersData = json_decode($runnersData);
        $runnersData = json_decode(json_encode($runnersData), True);

        $runnersData = $runnersData['runners'];
        $runnersData = json_decode($runnersData, True);
       
        if( $runnersData != null ){
            $balExpose = $balPlus = $runnersExposeData=[];
            foreach ( $runnersData as $runners ){
           // echo $runners['secId'];
               $profitLossData = $this->getProfitLossMatchOddsNewAllFinal($uid,$marketId,$eventId,$runners['secId'],$m_type);
               $profitLoss = $profitLossData['total'];
               $runnersExposeData[] = $profitLossData['runnersExpose'];
               if( $profitLoss < 0 ){
                   $balExpose[] = $profitLoss;
               }else{
                   $balPlus[] = $profitLoss;
               }

            }
        }


        if( $balExpose != null ){
            $minExpose = min($balExpose);
        }

        if( $balPlus != null ){
            $maxProfit = max($balPlus);
        }

        $resultArray = [
            'expose' => $minExpose,
            'profit' => $maxProfit,
            'runnersExpose' => $runnersExposeData];
        return $resultArray;

      }catch (\Exception $e) {
          $response = $this->errorLog($e);
          return response()->json($response, 501);
      }
    }

    public function getProfitLossMatchOddsNewAllFinalOLD($userId,$marketId,$eventId,$selId,$m_type)
    {

        try{

        $total = 0;
        // IF RUNNER WIN
        if( null != $userId && $marketId != null && $eventId != null && $selId != null){

            $where = [ ['is_match', 1],['mid', $marketId] , ['uid', $userId], ['result','PENDING'], ['secId' , $selId] , ['eid', $eventId] , ['bType','back'] ,['mType', $m_type] ];
            $backWin = DB::table('tbl_bet_pending_1')
                ->where($where)->whereIn('status',[1,5])->sum("win");

                //echo $backWin;
            if( $backWin == null || !isset($backWin) || $backWin == '' ){
                $backWin = 0;
            }else{ $backWin = $backWin; }

            $where = [ ['is_match', 1],['mid', $marketId], ['uid', $userId], ['result','PENDING'], ['eid', $eventId] , ['bType','lay'] ,['mType', $m_type],['secId','!=',$selId] ];
            $layWin = DB::table('tbl_bet_pending_1')
                ->where($where)->whereIn('status',[1,5])->sum("win");
            if( $layWin == null || !isset($layWin) || $layWin == '' ){
                $layWin = 0;
            }else{ $layWin = $layWin; }

            $totalWin = $backWin + $layWin;

            // IF RUNNER LOSS
            $where = [ ['is_match', 1],['mid', $marketId] , ['uid', $userId], ['result','PENDING'], ['secId', $selId] , ['eid', $eventId] , ['bType','lay'] ,['mType', $m_type] ];
           $layLoss = DB::table('tbl_bet_pending_1')
                ->where($where)->whereIn('status',[1,5])->sum("loss");
            if( $layLoss == null || !isset($layLoss) || $layLoss == '' ){
                $layLoss = 0;
            }else{ $layLoss = $layLoss; }

              $backLoss = DB::table('tbl_bet_pending_1')
             ->where([['is_match', 1],['mid', $marketId], ['uid', $userId], ['result','PENDING'], ['eid', $eventId] , ['bType','back'] ,['mType', $m_type],['secId','!=',$selId] ])
             ->whereIn('status',[1,5])->sum("loss");
            if( $backLoss == null || !isset($backLoss) || $backLoss == '' ){
                $backLoss = 0;
            }else{ $backLoss = $backLoss; }

            // IF UNMATCH BET LOSS
            $where = [ ['is_match', 0],['mid', $marketId] , ['uid', $userId], ['result','PENDING'], ['eid', $eventId] , ['bType',['back','lay']] ,['mType', $m_type] ];
            $unmatchLoss = DB::table('tbl_bet_pending_1')->where($where)->whereIn('status',[1,5])->sum("loss");
            if( $unmatchLoss == null || !isset($unmatchLoss) || $unmatchLoss == '' ){
                $unmatchLoss = 0;
            }else{ $unmatchLoss = $unmatchLoss; }

            $totalLoss = $backLoss + $layLoss + $unmatchLoss;
            $total = $totalWin-$totalLoss;
            //echo "selId==>".$selId."==total==>".$total."==totalWin==>".$totalWin."==totalLoss==>".$totalLoss; echo "<br>";
            $totalLossEx=0;    $totalExpose=0;
            $totalLossEx = $backLoss + $layLoss;
            $totalExpose = $totalWin-$totalLossEx;
            $bookarray=$runnerarray=[];
            $runnerarray=array('totalExpose'=>$totalExpose,'totalWin'=>$totalWin,'totalLossEx'=>$totalLossEx,'sec_id'=>$selId);
            $bookarray=array('total'=> $total,'runnersExpose'=>$runnerarray);
           // echo $total; echo "<br>";
        }


        return $bookarray;

      }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }   

    }

    public function getProfitLossMatchOddsNewAllFinal($userId,$marketId,$eventId,$selId,$m_type)
    {

        try{

            $total = 0;
            // IF RUNNER WIN
            if( null != $userId && $marketId != null && $eventId != null && $selId != null){

                // COMMON CODE
                if( $m_type == 'set_market' || $m_type == 'goals' ){
                    $tbl = 'tbl_bet_pending_6';
                }else{
                    $tbl = 'tbl_bet_pending_1';
                }
                $where = [ ['mid', $marketId] , ['uid', $userId], ['result','PENDING'], ['eid', $eventId],['mType', $m_type] ];
                $allList = DB::table($tbl)->select(['win','loss','secId','bType','is_match'])
                    ->where($where)->whereIn('status',[1,5])->get();
                $backWin = $layWin = $backLoss = $layLoss = $unMatchLoss = 0;
                if( $allList->isNotEmpty() ){
                    foreach ( $allList as $list ){
                        if( $list->is_match == 1){
                            if( $list->secId == $selId && $list->bType == 'back'){
                                $backWin = $backWin+$list->win;
                            }elseif( $list->secId != $selId && $list->bType == 'lay'){
                                $layWin = $layWin+$list->win;
                            }elseif( $list->secId == $selId && $list->bType == 'lay'){
                                $layLoss = $layLoss+$list->loss;
                            }elseif( $list->secId != $selId && $list->bType == 'back'){
                                $backLoss = $backLoss+$list->loss;
                            }else{
                                $unMatchLoss = $unMatchLoss+$list->loss;
                            }
                        }else{
                            $unMatchLoss = $unMatchLoss+$list->loss;
                        }
                    }
                }

                /*$where = [ ['is_match', 1],['mid', $marketId] , ['uid', $userId], ['result','PENDING'], ['secId' , $selId] , ['eid', $eventId] , ['bType','back'] ,['mType', $m_type] ];
                $backWinList = DB::table('tbl_bet_pending_1')->select('win')
                    ->where($where)->whereIn('status',[1,5])->get();
                $backWin = 0;
                if( $backWinList->isNotEmpty() ){
                    foreach ( $backWinList as $winList ){
                        $backWin = $backWin+$winList->win;
                    }
                }

                $where = [ ['is_match', 1],['mid', $marketId], ['uid', $userId], ['result','PENDING'], ['eid', $eventId] , ['bType','lay'] ,['mType', $m_type],['secId','!=',$selId] ];
                $layWinList = DB::table('tbl_bet_pending_1')->select('win')
                    ->where($where)->whereIn('status',[1,5])->get();
                $layWin = 0;
                if( $layWinList->isNotEmpty() ){
                    foreach ( $layWinList as $winList ){
                        $layWin = $layWin+$winList->win;
                    }
                }

                $totalWin = $backWin + $layWin;

                // IF RUNNER LOSS
                $where = [ ['is_match', 1],['mid', $marketId] , ['uid', $userId], ['result','PENDING'], ['secId', $selId] , ['eid', $eventId] , ['bType','lay'] ,['mType', $m_type] ];
                $layLossList = DB::table('tbl_bet_pending_1')->select('loss')
                    ->where($where)->whereIn('status',[1,5])->get();
                $layLoss = 0;
                if( $layLossList->isNotEmpty() ){
                    foreach ( $layLossList as $lossList ){
                        $layLoss = $layLoss+$lossList->loss;
                    }
                }

                $where = [['is_match', 1],['mid', $marketId], ['uid', $userId], ['result','PENDING'], ['eid', $eventId] , ['bType','back'] ,['mType', $m_type],['secId','!=',$selId] ];
                $backLossList = DB::table('tbl_bet_pending_1')->select('loss')
                    ->where($where)->whereIn('status',[1,5])->get();
                $backLoss = 0;
                if( $backLossList->isNotEmpty() ){
                    foreach ( $backLossList as $lossList ){
                        $backLoss = $backLoss+$lossList->loss;
                    }
                }



                // IF UNMATCH BET LOSS
                $where = [ ['is_match', 0],['mid', $marketId] , ['uid', $userId], ['result','PENDING'], ['eid', $eventId] , ['bType',['back','lay']] ,['mType', $m_type] ];
                $unmatchLossList = DB::table('tbl_bet_pending_1')->select('loss')
                    ->where($where)->whereIn('status',[1,5])->get();
                $unmatchLoss = 0;
                if( $unmatchLossList->isNotEmpty() ){
                    foreach ( $unmatchLossList as $lossList ){
                        $unmatchLoss = $unmatchLoss+$lossList->loss;
                    }
                }
                */

                $totalWin = $backWin + $layWin;
                $totalLoss = $backLoss+$layLoss+$unMatchLoss;
                $total = $totalWin-$totalLoss;
                $totalLossEx = $totalExpose = 0;
                $totalLossEx = $backLoss+$layLoss;
                $totalExpose = $totalWin-$totalLossEx;
                $bookarray = $runnerarray = [];
                $runnerarray = ['totalExpose' => $totalExpose, 'totalWin' => $totalWin, 'totalLossEx' => $totalLossEx, 'sec_id' => $selId];
                $bookarray = ['total'=> $total,'runnersExpose' => $runnerarray];
            }

            return $bookarray;

        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    // Function to get the client Commission Rate
    public function clientCommissionRate()
    {
        try{

        $CCR = 1;//$CCR = Client Commission Rate
        $query = DB::table('tbl_common_setting')->select(['value'])
            ->where([['key_name', 'CLIENT_COMMISSION_RATE'],['status',1]]);
        $setting = $query->first();
         
        if( $setting != null ){
            $CCR = $setting->value;
            return $CCR;
        }else{
            return $CCR;
        }
      }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }   

    }

    /* bet place function for fancy(Online), fancy2(Manual Session), Fancy3(Single fancy) */
    public function placeBetFancy2($request,$userInfo,$parentName,$userBalance,$userProfitLossBalance,$userLimitArray,$sportMarket,$odds_rate,$eventname)
    {
        try{
      
        $sportArr = ['1' => 'Football' , '2' => 'Tennis' , '4' => 'Cricket' , '6' => 'Election' , '10' => 'Jackpot' ,'11' => 'lottery',
            '998917' => 'Volleyball' , '7522' => 'basketball' , '3503' => 'Dart' , '22' => 'Table tennis' , '13' => 'badminton' ,'14' => 'Kabaddi','16' => 'Boxing'];
          $uid=$userInfo->id;
      

        $cache = Redis::connection();
        $eventData=$cache->get("Market_".$request['market_id']);
        $fancyArray = json_decode($eventData);
        //$event = json_decode(json_encode($eventData1), True);
        $redisMarketName=$request['market_name'];
        if(!empty($fancyArray)) {
            $redisMarketName = $fancyArray->title;
        }
           // echo "<pre>"; print_r($fancyArray); exit;


        // print_r($sportMarket); die('rtrtr');

         if(!empty($sportMarket)) {
             if (!empty($sportMarket)) {
                 if ($sportMarket->min_stack > $request['size'] && $sportMarket->min_stack != 0) {
                     $response = ["status" => 1, "code" => 200, "data" => null, "message" => 'Minimum stack value is ' . $sportMarket->min_stack];
                     return $response; exit;
                 }

                 if ($sportMarket->max_stack < $request['size'] && $sportMarket->max_stack != 0) {
                     $response = ["status" => 1, "code" => 200, "data" => null, "message" => 'Maximum stack value is ' . $sportMarket->max_stack];
                     return $response; exit;
                 }
             }
         }
      

        if(!empty($fancyArray)){
            if( $fancyArray->min_stack > $request['size'] ){
                $response =  [ "status" => 1 ,"code" => 200 ,"data"=>null, "message" => 'Minimum stack value is '.$fancyArray->min_stack];
                return $response; exit;

            }
            if( $fancyArray->max_stack < $request['size']){
                $response =  [ "status" => 1 ,"code" => 200 ,"data"=>null, "message" => 'Maximum stack value is '.$fancyArray->max_stack ];
                return $response; exit;
            }


        }else{
            $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" => "Bet can not placed  !!"];
            return $response; exit;
        }

        $deleteQuery = DB::table('tbl_bet_pending_2')->where([['uid',$uid],['status',5]])->delete();
        $model= new FancyFunction();
        $model->systemId = $userInfo->systemId;
        $model->uid = $uid;
        $model->sid = $request['sport_id'];
        $model->eid = $request['event_id'];
        $model->mid = $request['market_id'];
        $model->price = $request['price'];
        $model->size = $request['size'];
        $model->bType = $request['bet_type'];
        $model->rate = $request['rate'];
        $model->mType = $request['m_type'];
        $model->runner = $redisMarketName;
        $model->market =$redisMarketName;
        $model->event = $eventname;
        $model->ip_address =  $this->get_client_ip();
        $game_type = $sportArr[$request['sport_id']];
        $model->client = $userInfo->username;
        $model->master = $parentName;
        $model->description = $game_type.' > '.$model->event.' > '.$redisMarketName;
        $model->sport = $game_type;
        $model->secId = 0;
        $model->diff = 0;
        $model->is_match = 1;

        if($request['m_type'] == 'fancy3') {

            if ($request['bet_type'] == 'back') {

                if ($request['price'] > 1) {
                    $model->win = ($request['price'] - 1) * $request['size'];
                } else {
                    $model->win = 0;
                }
                $model->loss = $request['size'];
            } else {
                $model->win = $request['size'];
                if ($request['price'] > 1) {
                    $model->loss = ($request['price'] - 1) * $request['size'];
                } else {
                    $model->loss = $request['size'];
                }
            }
        }else if($request['m_type'] == 'oddeven') {

                if ($request['bet_type'] == 'back') {

                    if ($request['price'] > 1) {
                        $model->win = ($request['price'] - 1) * $request['size'];
                    } else {
                        $model->win = 0;
                    }
                    $model->loss = $request['size'];
                } else {
                    $model->win = $request['size'];
                    if ($request['price'] > 1) {
                        $model->loss = ($request['price'] - 1) * $request['size'];
                    } else {
                        $model->loss = $request['size'];
                    }
                }
        }else {
            if ($request['bet_type'] == 'yes' && $request['rate'] != null) {
                $model->win = round(($request['size'] * $request['rate']) / 100);
                $model->loss = $request['size'];
            } elseif ($model->bType == 'no' && $request['rate'] != null) {
                $model->win = $request['size'];
                $model->loss = round(($request['size'] * $request['rate']) / 100);
            } else {
                $model->win = $request['size'];
                $model->loss = $request['size'];
            }
        }

        $model->save();
        $betId = $model->id;
        $oldMarketExpose = $newMarketExpose = $pendingMarketExpose = $exposeBalance = $newMarketProfit = 0;

        // OLD EXPOSE CODE
        /*$userExpose = DB::table('tbl_user_market_expose')
            ->where([['uid',$uid],['status',1],['mid','!=',$request['market_id']]])->sum("expose");
        if(!empty($userExpose>0)){
            $balExpose = (-1)*($userExpose);
            if( $balExpose != null ){
                $oldMarketExpose = $balExpose;
            }
        }*/

        $oldMarketExpose = $this->getOldMarketExpose($uid, $request['market_id']);
        $getUserFancy = $this->checkExposeBalance($uid,$request);
        $newMarketExpose = $getUserFancy['expose'];
        $newMarketProfit = $getUserFancy['profit'];
        $exposeBalance = $oldMarketExpose+$newMarketExpose;

 
        $userAvailableBalance = 0;
        if($userProfitLossBalance > 0){
            $userAvailableBalance = ( $userBalance + $userProfitLossBalance );
        }else{
            $userAvailableBalance = ( $userBalance -(-1)*$userProfitLossBalance );
        }

        if($userAvailableBalance > 0){
            $userAvailableBalance = ( $userAvailableBalance -(-1)*$exposeBalance);
        }

        if ( round($userAvailableBalance) < 0 ) {
            $deleteQuery = DB::table('tbl_bet_pending_2')->where([['id',$betId]])->delete();
            $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" => 'Insufficient funds!!!'];
            return $response; exit;

        }else{
            $checkProfitLimit = $this->checkMaxProfitLimitNew2($uid,$newMarketProfit,$request['m_type'],$request['event_id'],$request['market_id'],$request['sport_id'],$userLimitArray,$newMarketExpose);
            if( ( $checkProfitLimit['is_true'] == false ) ){
                $deleteQuery = DB::table('tbl_bet_pending_2')->where([['id',$betId]])->delete();
                $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" =>$checkProfitLimit['msg']];
                return $response; exit;
            }

              $updated_on = date('Y-m-d H:i:s');
            $dataArr = []; $updatedTime = time();
            $exposeBalance = (-1)*($exposeBalance);

            DB::table('tbl_bet_pending_2')->where('id',$betId)->update(['status' => 1,'created_on' => $updated_on ,'updated_on' => $updated_on]);
            $dataArr = [ "balance" => round($userAvailableBalance) , "expose" =>  round($exposeBalance) , "mywallet" => round($userBalance) , "updated_time" => $updatedTime ];
            $updated_on = date('Y-m-d H:i:s');
           DB::table('tbl_user_info')->where('uid',$uid)->update(['expose'=>$exposeBalance,'updated_on'=>$updated_on]);
            $market_id=$request['market_id'];
            $this->updateUserExposeAndProfit( $uid,$request['event_id'],$market_id,$newMarketExpose,$newMarketProfit,$request['sport_id'],$request['m_type'] , null,$userInfo->systemId);

            $msg = "Bet ".$request['bet_type']." RUN,</br>Placed ".$request['size']." @ ".$request['price']." Odds </br> Matched ".$request['size']." @ ".$request['rate']." Odds";
            $data=array('balance'=>$dataArr,'betId'=>md5($betId));
            $response =  [ "status" => 1 ,"code" => 200 ,"data"=>$data, "message" =>$msg];

            return $response; exit;
        }
      }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }   

    }


    public function placeBetBinary($request,$userInfo,$parentName,$userBalance,$userProfitLossBalance,$userLimitArray,$sportMarket,$odds_rate,$eventname)
    {
        try{

            $sportArr = ['1' => 'Football' , '2' => 'Tennis' , '4' => 'Cricket' , '6' => 'Election','7'=> 'Binary' , '10' => 'Jackpot' ,'11' => 'lottery'];
            $uid=$userInfo->id;


            $cache = Redis::connection();
            $eventData=$cache->get("Market_".$request['market_id']);
            $fancyArray = json_decode($eventData);
            //$event = json_decode(json_encode($eventData1), True);
            $redisMarketName=$request['market_name'];
            if(!empty($fancyArray)) {
                $redisMarketName= $fancyArray->title;
            }

            if(!empty($sportMarket)) {
                if (!empty($sportMarket)) {
                    if ($sportMarket->min_stack > $request['size'] && $sportMarket->min_stack != 0) {
                        $response = ["status" => 1, "code" => 200, "data" => null, "message" => 'Minimum stack value is ' . $sportMarket->min_stack];
                        return $response; exit;
                    }

                    if ($sportMarket->max_stack < $request['size'] && $sportMarket->max_stack != 0) {
                        $response = ["status" => 1, "code" => 200, "data" => null, "message" => 'Maximum stack value is ' . $sportMarket->max_stack];
                        return $response; exit;
                    }
                }
            }


            if(!empty($fancyArray)){
                if( $fancyArray->min_stack > $request['size'] ){
                    $response =  [ "status" => 1 ,"code" => 200 ,"data"=>null, "message" => 'Minimum stack value is '.$fancyArray->min_stack];
                    return $response; exit;

                }
                if( $fancyArray->max_stack < $request['size']){
                    $response =  [ "status" => 1 ,"code" => 200 ,"data"=>null, "message" => 'Maximum stack value is '.$fancyArray->max_stack ];
                    return $response; exit;
                }


            }else{
                $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" => "Bet can not placed  !!"];
                return $response; exit;
            }

            $deleteQuery = DB::table('tbl_bet_pending_5')->where([['uid',$uid],['status',5]])->delete();
            $model= new BinaryFunction();
            $model->systemId = $userInfo->systemId;
            $model->uid = $uid;
            $model->sid = $request['sport_id'];
            $model->eid = $request['event_id'];
            $model->mid = $request['market_id'];
            $model->price = $request['price'];
            $model->size = $request['size'];
            $model->bType = $request['bet_type'];
            $model->rate = $request['rate'];
            $model->mType = $request['m_type'];
            $model->runner = $redisMarketName;
            $model->market =$redisMarketName;
            $model->event = $eventname;
            $model->ip_address =  $this->get_client_ip();
            $game_type = $sportArr[$request['sport_id']];
            $model->client = $userInfo->username;
            $model->master = $parentName;
            $model->description = $game_type.' > '.$model->event.' > '.$redisMarketName;
            $model->sport = $game_type;
            $model->secId = 0;
            $model->diff = 0;
            $model->is_match=1;


            if ($request['bet_type'] == 'yes' && $request['rate'] != null) {
                $model->win = round(($request['size'] * $request['rate']) / 100);
                $model->loss = $request['size'];
            } elseif ($model->bType == 'no' && $request['rate'] != null) {
                $model->win = $request['size'];
                $model->loss = round(($request['size'] * $request['rate']) / 100);
            } else {
                $model->win = $request['size'];
                $model->loss = $request['size'];
            }


            $model->save();
            $betId = $model->id;
            $oldMarketExpose = $newMarketExpose = $pendingMarketExpose = $exposeBalance = $newMarketProfit = 0;

            //OLD EXPOSE CODE
            /*$userExpose = DB::table('tbl_user_market_expose')
                ->where([['uid',$uid],['status',1],['mid','!=',$request['market_id']]])->sum("expose");
            if(!empty($userExpose>0)){
                $balExpose = (-1)*($userExpose);
                if( $balExpose != null ){
                    $oldMarketExpose = $balExpose;
                }
            }*/

            $oldMarketExpose = $this->getOldMarketExpose($uid, $request['market_id']);
            $getUserFancy = $this->checkExposeBalance($uid,$request);
            $newMarketExpose = $getUserFancy['expose'];
            $newMarketProfit = $getUserFancy['profit'];
            $exposeBalance = $oldMarketExpose+$newMarketExpose;

            $userAvailableBalance = 0;
            if($userProfitLossBalance > 0){
                $userAvailableBalance = ( $userBalance + $userProfitLossBalance );
            }else{
                $userAvailableBalance = ( $userBalance -(-1)*$userProfitLossBalance );
            }

            if($userAvailableBalance > 0){
                $userAvailableBalance = ( $userAvailableBalance -(-1)*$exposeBalance);
            }

            if ( round($userAvailableBalance) < 0 ) {
                $deleteQuery = DB::table('tbl_bet_pending_5')->where([['id',$betId]])->delete();
                $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" => 'Insufficient funds!!!'];
                return $response; exit;

            }else{
                $checkProfitLimit = $this->checkMaxProfitLimitNew2($uid,$newMarketProfit,$request['m_type'],$request['event_id'],$request['market_id'],$request['sport_id'],$userLimitArray,$newMarketExpose);
                if( ( $checkProfitLimit['is_true'] == false ) ){
                    $deleteQuery = DB::table('tbl_bet_pending_5')->where([['id',$betId]])->delete();
                    $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" =>$checkProfitLimit['msg']];
                    return $response; exit;
                }

                $updated_on = date('Y-m-d H:i:s');
                $dataArr = []; $updatedTime = time();
                $exposeBalance = (-1)*($exposeBalance);

                DB::table('tbl_bet_pending_5')->where('id',$betId)->update(['status' => 1,'created_on' => $updated_on ,'updated_on' => $updated_on]);
                $dataArr = [ "balance" => round($userAvailableBalance) , "expose" =>  round($exposeBalance) , "mywallet" => round($userBalance) , "updated_time" => $updatedTime ];
                $updated_on = date('Y-m-d H:i:s');
                DB::table('tbl_user_info')->where('uid',$uid)->update(['expose'=>$exposeBalance,'updated_on'=>$updated_on]);
                $market_id=$request['market_id'];
                $this->updateUserExposeAndProfit( $uid,$request['event_id'],$market_id,$newMarketExpose,$newMarketProfit,$request['sport_id'],$request['m_type'] , null,$userInfo->systemId);

                $msg = "Bet ".$request['bet_type']." RUN,</br>Placed ".$request['size']." @ ".$request['price']." Odds </br> Matched ".$request['size']." @ ".$request['rate']." Odds";
                $data=array('balance'=>$dataArr,'betId'=>md5($betId));
                $response =  [ "status" => 1 ,"code" => 200 ,"data"=>$data, "message" =>$msg];

                return $response; exit;
            }
        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }


  /* function for Lottery */
    public function placeBetLottery($request,$userInfo,$parentName,$userBalance,$userProfitLossBalance,$userLimitArray,$sportMarket,$eventname)
    {

        try{

         //  print_r($request);
        // $sportArr = ['1' => 'Football' , '2' => 'Tennis' , '4' => 'Cricket' , '6' => 'Election' , '10' => 'Jackpot' ,'11' => 'cricket casino'];
          $uid=$userInfo->id;

            
         $cache = Redis::connection();
        $eventData=$cache->get("Market_".$request['market_id']);
        $lotteryArray = json_decode($eventData);
            $redisMarketName=$request['market_name'];
            if(!empty($lotteryArray)) {
                $redisMarketName= $lotteryArray->title;
            }
            //print_r($lotteryArray);

            if( $request['bet_type'] != 'back' || trim($request['rate']) != trim($lotteryArray->rate)
                || !in_array(trim($request['price']) , [0,1,2,3,4,5,6,7,8,9]) ){
                $response =  [ "status" => 1 ,"code" => 200 ,"data"=>null, "message" => 'Invalid data! Bet can not placed !!'];
                return $response; exit;
            }

            if( $request['event_id'] != $lotteryArray->eid ){
                $response =  [ "status" => 1 ,"code" => 200 ,"data"=>null, "message" => 'Invalid data! Bet can not placed !!'];
                return $response; exit;
            }


       /*  if(!empty($sportMarket)){
         if($request['m_type'] == 'cricket_casino'){

             if($lotteryArray->sid == 4){
                    $sportMarket = json_decode($sportMarket->cricket_fancy);
                }elseif($lotteryArray->sid == 1){
                    $sportMarket = json_decode($sportMarket->football_fancy);
                }else{
                    $sportMarket = json_decode($sportMarket->tennis_fancy);
                }
         }

        if(!empty($sportMarket)){
       
            if($sportMarket->min_stack > $request['size'] && $sportMarket->min_stack != 0 ){
             $response =  [ "status" => 1 ,"code" => 200 ,"data"=>null, "message" => 'Minimum stack value is '.$sportMarket->min_stack];
                return $response; exit;
            }    

            if($sportMarket->max_stack < $request['size'] && $sportMarket->max_stack != 0){

             $response =  [ "status" => 1 ,"code" => 200 ,"data"=>null, "message" => 'Maximum stack value is '.$sportMarket->max_stack];
                return $response; exit;
            }
          }
      }*/

        if(!empty($lotteryArray)){
            if( $lotteryArray->min_stack > $request['size'] ){
                $response =  [ "status" => 1 ,"code" => 200 ,"data"=>null, "message" => 'Minimum stack value is '.$lotteryArray->min_stack];
                return $response; exit;

            }
            if( $lotteryArray->max_stack < $request['size']){
                $response =  [ "status" => 1 ,"code" => 200 ,"data"=>null, "message" => 'Maximum stack value is '.$lotteryArray->max_stack ];
                return $response; exit;
            }

        }else{

            $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" => "Bet can not placed !!"];
            return $response; exit;
        }

        $deleteQuery = DB::table('tbl_bet_pending_4')->where([['uid',$uid],['status',5]])->delete();
        $model= new LotteryFunction();
        $model->systemId = $userInfo->systemId;
        $model->uid = $uid;
        $model->sid = $request['sport_id'];
        $model->eid = $request['event_id'];
        $model->mid = $request['market_id'];
        $model->price = $request['price'];
        $model->size = $request['size'];
        $model->bType = $request['bet_type'];
        $model->rate = $request['rate'];
        $model->mType = $request['m_type'];
        $model->runner =$redisMarketName;
        $model->market = $redisMarketName;
        $model->event = $eventname;
        $model->ip_address =  $this->get_client_ip();
        $game_type = 'cricket casino';
        $model->client = $userInfo->username;
        $model->master = $parentName;
        $model->description = $game_type.' > '.$model->event.' > '.$model->runner;
        $model->sport = $game_type;
        $model->secId = $request['sec_id'];
        $model->diff = 0;
        $model->is_match=1;

         $model->win = round(( $model->size*( $model->rate-1 ) ));
         $model->loss = $model->size;

        $model->save();
        $betId = $model->id;
        $oldMarketExpose = $newMarketExpose = $pendingMarketExpose = $exposeBalance = $newMarketProfit = 0;

        // OLD EXPOSE CODE
        /*$userExpose = DB::table('tbl_user_market_expose')
            ->where([['uid',$uid],['status',1],['mid','!=',$request['market_id']]])->sum("expose");

        if(!empty($userExpose>0)){
            $balExpose = (-1)*($userExpose);
            if( $balExpose != null ){
                $oldMarketExpose = $balExpose;
            }
        }*/

        $oldMarketExpose = $this->getOldMarketExpose($uid, $request['market_id']);
        $getUserFancy = $this->checkExposeBalance($uid,$request);
        $newMarketExpose = $getUserFancy['expose'];
        $newMarketProfit = $getUserFancy['profit'];
        $exposeBalance = $oldMarketExpose+$newMarketExpose;

        $userAvailableBalance = 0;
        if($userProfitLossBalance > 0){
            $userAvailableBalance = ( $userBalance + $userProfitLossBalance );
        }else{
            $userAvailableBalance = ( $userBalance -(-1)*$userProfitLossBalance );
        }

        if($userAvailableBalance > 0){
            $userAvailableBalance = ( $userAvailableBalance -(-1)*$exposeBalance);
        }

        if ( round($userAvailableBalance) < 0 ) {
            $deleteQuery = DB::table('tbl_bet_pending_4')->where([['id',$betId]])->delete();
            $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" => 'Insufficient funds!!!'];
            return $response; exit;

        }else{
            $checkProfitLimit = $this->checkMaxProfitLimitNew2($uid,$newMarketProfit,$request['m_type'],$request['event_id'],$request['market_id'],$request['sport_id'],$userLimitArray,$newMarketExpose);
            if( ( $checkProfitLimit['is_true'] == false ) ){
                $deleteQuery = DB::table('tbl_bet_pending_4')->where([['id',$betId]])->delete();
                $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" =>$checkProfitLimit['msg']];
                return $response; exit;
            }

             $updated_on = date('Y-m-d H:i:s');
            $dataArr = []; $updatedTime = time();
            $exposeBalance = (-1)*($exposeBalance);

            DB::table('tbl_bet_pending_4')->where('id',$betId)->update(['status' => 1, 'created_on' => $updated_on, 'updated_on' => $updated_on]);
            $dataArr = [ "balance" => round($userAvailableBalance) , "expose" =>  round($exposeBalance) , "mywallet" => round($userBalance) , "updated_time" => $updatedTime ];
            $updated_on = date('Y-m-d H:i:s');
            DB::table('tbl_user_info')->where('uid',$uid)->update(['expose'=>$exposeBalance,'updated_on'=>$updated_on]);
            $market_id=$request['market_id'];
            $this->updateUserExposeAndProfit( $uid,$request['event_id'],$market_id,$newMarketExpose,$newMarketProfit,$request['sport_id'],$request['m_type'], null,$userInfo->systemId);

            $msg = "Bet ".$request['bet_type']." RUN,</br>Placed ".$request['size']." @ ".$request['price']." Odds </br> Matched ".$request['size']." @ ".$request['rate']." Odds";
            $data=array('balance'=>$dataArr,'betId'=>md5($betId));
            $response =  [ "status" => 1 ,"code" => 200 ,"data"=>$data, "message" =>$msg];

            return $response; exit;
        }
     }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }   

    }


    /* function for Lottery */
    public function placeBetJackpot($request,$userInfo,$parentName,$userBalance,$userProfitLossBalance,$userLimitArray,$sportMarket,$eventname)
    {
      try{

         //  print_r($request);
        $sportArr = ['1' => 'Football' , '2' => 'Tennis' , '4' => 'Cricket' , '6' => 'Election' , '10' => 'Jackpot' ,'11' => 'Cricket casino'];
          $uid=$userInfo->id;

        $cache = Redis::connection();
       $jackpotGame = $cache->get("Jackpot_market_".$request->gameId."_".$request->event_id."_".$request->market_id);
       $JacpotArray = json_decode($jackpotGame);
          $redisMarketName=$request['market_name'];
          if(!empty($JacpotArray)) {
              $redisMarketName = $JacpotArray->title;
          }

         /*   if(!empty($sportMarket)){
         if($request['m_type'] == 'jackpot'){

             if($JacpotArray->sid == 4){
                    $sportMarket = json_decode($sportMarket->cricket_fancy);
                }elseif($JacpotArray->sid == 1){
                    $sportMarket = json_decode($sportMarket->football_fancy);
                }else{
                    $sportMarket = json_decode($sportMarket->tennis_fancy);
                }
         }

        if(!empty($sportMarket)){
       
            if($sportMarket->min_stack > $request['size'] && $sportMarket->min_stack != 0 ){
             $response =  [ "status" => 1 ,"code" => 200 ,"data"=>null, "message" => 'Minimum stack value is '.$sportMarket->min_stack];
                return $response; exit;
            }    

            if($sportMarket->max_stack < $request['size'] && $sportMarket->max_stack != 0){

             $response =  [ "status" => 1 ,"code" => 200 ,"data"=>null, "message" => 'Maximum stack value is '.$sportMarket->max_stack];
                return $response; exit;
            }
          }
      }*/


        $jackpotSetting = $cache->get("Jackpot_market_setting_".$request->gameId."_". $request->event_id."_".$request->market_id);
         $jackpotSetting = json_decode($jackpotSetting);

      

        if(!empty($jackpotSetting)){
            if( $jackpotSetting->min_stack > $request->size ){
                $response =  [ "status" => 1 ,"code" => 200 ,"data"=>null, "message" => 'Minimum stack value is '.$jackpotSetting->min_stack];
                return $response; exit;

            }
            if( $jackpotSetting->max_stack < $request->size){
                $response =  [ "status" => 1 ,"code" => 200 ,"data"=>null, "message" => 'Maximum stack value is '.$jackpotSetting->max_stack ];
                return $response; exit;
            }

        }else{
            $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" => "Bet can not placed !!"];
            return $response; exit;
        }

//        $jackpotMarketData1 = $cache->get("Jackpot_" . $request->gameId . "_" . $request->market_id);
//        $jackpotMarketData1 = json_decode($jackpotMarketData1);
//        if( !empty($jackpotMarketData1) ){
//            $jackRateIs = true;
//            foreach ($jackpotMarketData1 as $jData1){
//                if( ( $jData1->secId == $request->sec_id && $jData1->mid == $request->market_id )
//                    && $jData1->rate != $request->rate ){
//                    $jackRateIs = false;
//                }
//            }
//
//            if( $jackRateIs == false ){
//                $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" => "Bed Request ! Bet can not placed !!"];
//                return $response; exit;
//            }
//
//        }

        $deleteQuery = DB::table('tbl_bet_pending_4')->where([['uid',$uid],['status',5]])->delete();
        $model= new JackpotFunction();
        $model->systemId = $userInfo->systemId;
        $model->uid = $uid;
        $model->sid = $request->sport_id;
        $model->eid = $request->event_id;
        $model->mid = $request->market_id;
        $model->price = $request->price;
        $model->size = $request->size;
        $model->bType = $request->bet_type;
        $model->rate = $request->rate;
        $model->mType = $request->m_type;
        $model->runner = $request->runner;
        $model->market = $redisMarketName;
        $model->event = $eventname;
        $model->ip_address =  $this->get_client_ip();
        $game_type = $sportArr[$request->sport_id];
        $model->client = $userInfo->username;
        $model->master = $parentName;
        $model->description = $game_type.' > '.$model->event.' > '.$redisMarketName.' > '.$model->runner;
        $model->sport = $game_type;
        $model->secId = $request->sec_id;
        $model->diff = 0;
        $model->is_match=1;
        $model->win = round(( $model->size*( $model->rate-1 ) ));
        $model->loss = $model->size;

        $model->save();
        $betId = $model->id;
        $oldMarketExpose = $newMarketExpose = $pendingMarketExpose = $exposeBalance = $newMarketProfit = 0;

        //OLD EXPOSE CODE
        /*$userExpose = DB::table('tbl_user_market_expose')
            ->where([['uid',$uid],['status',1],['mid','!=',$request->market_id]])->sum("expose");

        if(!empty($userExpose>0)){
            $balExpose = (-1)*($userExpose);
            if( $balExpose != null ){
                $oldMarketExpose = $balExpose;
            }
        }*/

        $oldMarketExpose = $this->getOldMarketExpose($uid, $request['market_id']);
        $getUserFancy = $this->checkExposeBalance($uid,$request);
        $newMarketExpose = $getUserFancy['expose'];
        $newMarketProfit = $getUserFancy['profit'];
        $exposeBalance = $oldMarketExpose+$newMarketExpose;

        $userAvailableBalance = 0;
        if($userProfitLossBalance > 0){
            $userAvailableBalance = ( $userBalance + $userProfitLossBalance );
        }else{
            $userAvailableBalance = ( $userBalance -(-1)*$userProfitLossBalance );
        }

        if($userAvailableBalance > 0){
            $userAvailableBalance = ( $userAvailableBalance -(-1)*$exposeBalance);
        }

        if ( round($userAvailableBalance) < 0 ) {
            $deleteQuery = DB::table('tbl_bet_pending_4')->where([['id',$betId]])->delete();
            $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" => 'Insufficient funds!!!'];
            return $response; exit;

        }else{
            $checkProfitLimit = $this->checkMaxProfitLimitNew2($uid,$newMarketProfit,$request->m_type,$request->event_id,$request->market_id,$request->sport_id,$userLimitArray,$newMarketExpose);
            if( ( $checkProfitLimit['is_true'] == false ) ){
                $deleteQuery = DB::table('tbl_bet_pending_4')->where([['id',$betId]])->delete();
                $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" =>$checkProfitLimit['msg']];
                return $response; exit;
            }
            $updated_on = date('Y-m-d H:i:s');
            $dataArr = []; $updatedTime = time();
            $exposeBalance = (-1)*($exposeBalance);

            DB::table('tbl_bet_pending_4')->where('id',$betId)->update(['status' => 1, 'created_on' => $updated_on,'updated_on' => $updated_on]);
            $dataArr = [ "balance" => round($userAvailableBalance) , "expose" =>  round($exposeBalance) , "mywallet" => round($userBalance) , "updated_time" => $updatedTime ];
            $updated_on = date('Y-m-d H:i:s'); //,'updated_on'=>$updated_on
            DB::table('tbl_user_info')->where('uid',$uid)->update(['expose'=>$exposeBalance,'updated_on'=>$updated_on]);
            $market_id=$request->market_id;
            $this->updateUserExposeAndProfit( $uid,$request->event_id,$market_id,$newMarketExpose,$newMarketProfit,$request->sport_id,$request->m_type , null,$userInfo->systemId);

            $msg = "Bet ".$request->bet_type." RUN,</br>Placed ".$request->size." @ ".$request->price." Odds </br> Matched ".$request->size." @ ".$request->rate." Odds";
            $data=array('balance'=>$dataArr,'betId'=>md5($betId));
            $response =  [ "status" => 1 ,"code" => 200 ,"data"=>$data, "message" =>$msg];

            return $response; exit;
        }
      }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }   

    }


    public function placeBetKhadoAndBallbyball($request,$userInfo,$parentName,$userBalance,$userProfitLossBalance,$userLimitArray,$sportMarket,$eventname){

        try{
          
        $sportArr = ['1' => 'Football' , '2' => 'Tennis' , '4' => 'Cricket' , '6' => 'Election' , '10' => 'Jackpot' ,'11' => 'lottery'];
        $uid=$userInfo->id;

        $cache = Redis::connection();
        $eventData=$cache->get("Market_".$request['market_id']);
        $rsArray = json_decode($eventData);
            $redisMarketName=$request['market_name'];
            if(!empty($rsArray) && $request['m_type'] =='ballbyball') {
                $redisMarketName= $rsArray->title."-".$rsArray->ball;
            }

            $difference=0;
            if($request['m_type'] == 'khado') {

                $cache = Redis::connection();
                if( isset($cache) ) {

                    $runnersData = $cache->get($request['market_id']);
                    $sessionData = json_decode($runnersData);

                    $difference= $sessionData->difference;

                    $redisMarketName= $rsArray->title."-".$difference;
                    //$model->diff = $model->price + $difference;
                }
            }



         if(!empty($sportMarket)) {
             if (!empty($sportMarket)) {
                 if ($sportMarket->min_stack > $request['size'] && $sportMarket->min_stack != 0) {
                     $response = ["status" => 1, "code" => 200, "data" => null, "message" => 'Minimum stack value is ' . $sportMarket->min_stack];
                     return $response; exit;
                 }

                 if ($sportMarket->max_stack < $request['size'] && $sportMarket->max_stack != 0) {
                     $response = ["status" => 1, "code" => 200, "data" => null, "message" => 'Maximum stack value is ' . $sportMarket->max_stack];
                     return $response; exit;
                 }
             }
         }

        //print_r($fancyArray);
        if(!empty($rsArray)){
            if( $rsArray->min_stack > $request['size'] ){
                $response =  [ "status" => 1 ,"code" => 200 ,"data"=>null, "message" => 'Minimum stack value is '.$rsArray->min_stack];
                return $response; exit;

            }
            if( $rsArray->max_stack < $request['size']){
                $response =  [ "status" => 1 ,"code" => 200 ,"data"=>null, "message" => 'Maximum stack value is '.$rsArray->max_stack ];
                return $response; exit;
            }

        }else{
            $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" => "Bet can not placed !!"];
            return $response; exit;
        }

        $deleteQuery = DB::table('tbl_bet_pending_3')->where([['uid',$uid],['status',5]])->delete();
        $model= new KhadoBallbyballFunction();
        $model->systemId = $userInfo->systemId;
        $model->uid = $uid;
        $model->sid = $request['sport_id'];
        $model->eid = $request['event_id'];
        $model->mid = $request['market_id'];
        $model->price = $request['price'];
        $model->size = $request['size'];
        $model->bType = $request['bet_type'];
        $model->rate = $request['rate'];
        $model->mType = $request['m_type'];
        $model->runner = $redisMarketName;
        $model->market = $redisMarketName;
        $model->event = $eventname;
        $model->ip_address =  $this->get_client_ip();
        $game_type = $sportArr[$request['sport_id']];
        $model->client = $userInfo->username;
        $model->master = $parentName;
        $model->description = $game_type.' > '.$model->event.' > '.$redisMarketName;
        $model->sport = $game_type;
        $model->secId = 0;
        $model->diff = 0;
        $model->is_match=1;


        if($request['m_type'] == 'khado') {
            $model->diff = $model->price + $difference;
        }

        if($request['m_type'] == 'meter') {
            $model->win = $model->loss = 0;
        }else{
            if ($request['bet_type'] == 'yes' && $request['rate'] != null) {
                $model->win = round(($request['size'] * $request['rate']) / 100);
                $model->loss =$request['size'];
            } elseif ($model->bType == 'no' && $request['rate'] != null) {
                $model->win = $request['size'];
                $model->loss = round(($request['size'] * $request['rate']) / 100);
            } else {
                $model->win = $request['size'];
                $model->loss = $request['size'];
            }
        }

        $model->save();
        $betId = $model->id;
        $oldMarketExpose = $newMarketExpose = $pendingMarketExpose = $exposeBalance = $newMarketProfit = 0;

        // OLD CODE FOR EXPOSE
        /*
         $userExpose = DB::table('tbl_user_market_expose')
            ->where([['uid',$uid],['status',1],['mid','!=',$request['market_id']]])->sum("expose");

        if(!empty($userExpose>0)){
            $balExpose = (-1)*($userExpose);
            if( $balExpose != null ){
                $oldMarketExpose = $balExpose;
            }
        }*/

        $oldMarketExpose = $this->getOldMarketExpose($uid, $request['market_id']);
        $getUserFancy = $this->checkExposeBalance($uid,$request);
        $newMarketExpose = $getUserFancy['expose'];
        $newMarketProfit = $getUserFancy['profit'];
        $exposeBalance = $oldMarketExpose+$newMarketExpose;

        $userAvailableBalance = 0;
        if($userProfitLossBalance > 0){
            $userAvailableBalance = ( $userBalance + $userProfitLossBalance );
        }else{
            $userAvailableBalance = ( $userBalance -(-1)*$userProfitLossBalance );
        }

        if($userAvailableBalance > 0){
            $userAvailableBalance = ( $userAvailableBalance -(-1)*$exposeBalance);
        }

        if ( round($userAvailableBalance) < 0 ) {
            $deleteQuery = DB::table('tbl_bet_pending_3')->where([['id',$betId]])->delete();
            $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" => 'Insufficient funds!!!'];
            return $response; exit;

        }else{
            $checkProfitLimit = $this->checkMaxProfitLimitNew2($uid,$newMarketProfit,$request['m_type'],$request['event_id'],$request['market_id'],$request['sport_id'],$userLimitArray,$newMarketExpose);
            if( ( $checkProfitLimit['is_true'] == false ) ){
                $deleteQuery = DB::table('tbl_bet_pending_3')->where([['id',$betId]])->delete();
                $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" =>$checkProfitLimit['msg']];
                return $response; exit;
            }

             $updated_on = date('Y-m-d H:i:s');
            $dataArr = []; $updatedTime = time();
            $exposeBalance = (-1)*($exposeBalance);

            DB::table('tbl_bet_pending_3')->where('id',$betId)->update(['status' => 1,'created_on' => $updated_on, 'updated_on' => $updated_on]);
            $dataArr = [ "balance" => round($userAvailableBalance) , "expose" =>  round($exposeBalance) , "mywallet" => round($userBalance) , "updated_time" => $updatedTime ];
            $updated_on = date('Y-m-d H:i:s'); //,'updated_on'=>$updated_on
            DB::table('tbl_user_info')->where('uid',$uid)->update(['expose'=>$exposeBalance,'updated_on'=>$updated_on]);
            $market_id=$request['market_id'];
            $this->updateUserExposeAndProfit( $uid,$request['event_id'],$market_id,$newMarketExpose,$newMarketProfit,$request['sport_id'],$request['m_type'] , null,$userInfo->systemId);

            $msg = "Bet ".$request['bet_type']." RUN,<br>Placed ".$request['size']." @ ".$request['price']." Odds <br> Matched ".$request['size']." @ ".$request['rate']." Odds";
            $data=array('balance'=>$dataArr,'betId'=>md5($betId));
            $response =  [ "status" => 1 ,"code" => 200 ,"data"=>$data, "message" =>$msg];

            return $response; exit;
        }

      }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }   

    }


    // Function Update User Bet Expose
    public function updateUserBetExpose( $uid, $eventId, $marketId, $minExpose,$profit,$loss,$secId,$mType,$sportId )
    {

      try{

        $where = [ ['uid', $uid ], ['eid' ,$eventId],['mid', $marketId] , ['status', 1],['secId',$secId]];
        $query = DB::table('tbl_user_market_expose_2')->select(['id'])->where($where);
        $userExpose = $query->first();
        if( $userExpose != null ){
            $updateData = [
                'expose' => $minExpose,
                'profit' => $profit
            ];
            DB::table('tbl_user_market_expose_2')->where($where)->update($updateData);
        }else{
            $addData = [
                'uid' => $uid,
                'eid' => $eventId,
                'mid' => $marketId,
                'sid'=>$sportId,
                'mType'=>$mType,
                'expose' => $minExpose,
                'profit' => $profit,
                'secId' => $secId,
                'status' => 1,
            ];

            DB::table('tbl_user_market_expose_2')->insert($addData);
        }
      }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }   
    }

    // Function Update User Expose
    public function updateUserExposeAndProfit( $uid, $eventId, $marketId, $minExpose,$profit,$sportId,$mType,$secId,$systemId)
    {
      try{

        if( $minExpose != 0 ){ $minExpose = (-1)*($minExpose); }

        $where = [ ['uid', $uid ], ['eid' ,$eventId],['mid', $marketId] , ['status', 1]];
        $query = DB::table('tbl_user_market_expose')->select(['id'])->where($where);
        $userExpose = $query->first();
        if( $userExpose != null ){
            $updateData = [
                'expose' => $minExpose,
                'profit' => $profit
            ];
            DB::table('tbl_user_market_expose')->where($where)->update($updateData);
        }else{
            $addData = [
                'uid' => $uid,
                'systemId' => $systemId,
                'eid' => $eventId,
                'mid' => $marketId,
                'sid' => $sportId,
                'mType' => $mType,
                'expose' => $minExpose,
                'profit' => $profit,
                'status' => 1,
            ];

            DB::table('tbl_user_market_expose')->insert($addData);
        }

     //}
      }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }   
    

    }


    //checkMaxProfitLimit
    public function checkMaxProfitLimitNew2($uid,$newPl,$sessionType,$eventId,$marketId,$sportId,$userLimitArray,$newExpose)
    {
        try{
            if($newExpose < 0) {
                $newExpose = (-1) * $newExpose;
            }

        // Check Profit Limit
        $newProfitVal = $profitLimitEvent = $profitMatchOddVal = $profitMatchOdd2Val = $profitFancyVal = $profitFancy2Val = $profitJackpotVal = $profitLotteryVal = 0;
        $maxProfitLimitMatchOdd = $maxProfitLimitMatchOdd2 = $maxProfitLimitFancy = $maxProfitLimitFancy2 = $maxProfitLimitJackpot = $maxProfitLimitLottery = $maxProfitLimitEvent = 1000000;
        $isTrueExpose = true;
        //MatchOdd
        $type = 'market is over'; $isTrue = false;

            $expose_type='market is over';
            $cache = Redis::connection();
             $eventData=$cache->get("Market_".$marketId);
             $event = json_decode($eventData);
            if ($event != null) {
                $maxProfitLimitFancy = $event->max_profit_limit;
            }



           $sportMarket = DB::table('tbl_user_sport_market_limits')
                     ->select('*')
                     ->where('uid',$uid)->first();

            if(!empty($sportMarket)) {
                if ($sessionType == 'bookmaker') {
                    if ($sportId == 4) {
                        $sportMarket = json_decode($sportMarket->cricket_bookmaker);
                    } elseif ($sportId == 1) {
                        $sportMarket = json_decode($sportMarket->football_bookmaker);
                    } else {
                        $sportMarket = json_decode($sportMarket->tennis_bookmaker);
                    }

                    if ($newPl > $sportMarket->max_profit_limit && $sportMarket->max_profit_limit != 0) {
                        $isTrue = false;
                        $type = 'market is ' . $sportMarket->max_profit_limit;
                    } else {
                        $isTrue = true;
                        $type = 'market is ' . $sportMarket->max_profit_limit;
                    }

                     //echo "isTrueExpose-->".$isTrue.'===newPl==>'.$newPl.'--max_profit_limit-->'.$sportMarket->max_profit_limit.'--'.$type; exit;

                } elseif ( $sessionType == 'set_market' || $sessionType == 'goals' || $sessionType == 'match_odd' || $sessionType == 'winner') {

                    if ($sportId == 4) {
                        $sportMarket = json_decode($sportMarket->cricket_matchodd);
                    } elseif ($sportId == 1) {
                        $sportMarket = json_decode($sportMarket->football_matchodd);
                    } else {
                        $sportMarket = json_decode($sportMarket->tennis_matchodd);
                    }


                    if ($newPl > $sportMarket->max_profit_limit && $sportMarket->max_profit_limit != 0) {
                        $isTrue = false;
                        $type = 'market is ' . $sportMarket->max_profit_limit;
                    } else {
                        $isTrue = true;
                        $type = 'market is ' . $sportMarket->max_profit_limit;
                    }

                } else {

                    if ($sportId == 4) {
                        $sportMarket = json_decode($sportMarket->cricket_fancy);
                    } elseif ($sportId == 1) {
                        $sportMarket = json_decode($sportMarket->football_fancy);
                    } else {
                        $sportMarket = json_decode($sportMarket->tennis_fancy);
                    }

                    if ($newPl > $sportMarket->max_profit_limit && $sportMarket->max_profit_limit != 0) {
                        $isTrue = false;
                        $type = 'market is ' . $sportMarket->max_profit_limit;
                    } else {
                        $isTrue = true;
                        $type = 'market is ' . $sportMarket->max_profit_limit;
                    }


                }

                if ($sportMarket->max_expose_limit != 0 && ($newExpose > $sportMarket->max_expose_limit)) {
                    $isTrueExpose = false;
                    $expose_type = 'market is ' . $sportMarket->max_expose_limit;
                }

               // echo "isTrueExpose-->".$isTrueExpose.'===newExpose==>'.$newExpose.'--max_expose_limit-->'.$sportMarket->max_expose_limit.'--'.$expose_type; exit;
                if($isTrue == false){
                    return ['is_true' => $isTrue, 'msg' => 'Your max profit limit for this ' . $type . ' ! Bet can not placed!!'];
                }

            }


            if ( $sessionType == 'meter' && (int)$newExpose > $maxProfitLimitFancy) {
                $isTrue = false;
                $type = 'market is ' . $maxProfitLimitFancy;
            } else if ((int)$newPl > $maxProfitLimitFancy) {
                $isTrue = false;
                $type = 'market is ' . $maxProfitLimitFancy;
            } else {
                $isTrue = true;
                $type = 'market is ' . $maxProfitLimitFancy;
            }


          //  echo "newPl". $newPl. "==maxProfitLimit==>" . $maxProfitLimitFancy;
//exit; die();

        // Check Client Profit Limit
        if ( $isTrue == true ) {
            $maxProfitLimitUser = 0;

            if ($userLimitArray != null) {
                $maxProfitLimitUser = $userLimitArray->max_profit_limit;
            }
            if ((int)$newPl > $maxProfitLimitUser && $maxProfitLimitUser != 0) {
                $isTrue = false;
            } else {
                $isTrue = true;
            }

            $type = 'client is '.$maxProfitLimitEvent;
        }


        if ( $isTrue == true ) {
            $query = DB::table('tbl_user_market_expose')->select(['profit as profitVal'])
                ->where([['eid',$eventId],['uid',$uid],['status',1]])->whereNotIn('mid',[$marketId]);
            $profitLimitDataEvent = $query->first();

            if ( $profitLimitDataEvent != null) {
                if ( isset($profitLimitDataEvent->profitVal) && $profitLimitDataEvent->profitVal != null ) {
                    $profitValEvent = $profitLimitDataEvent->profitVal;
                    $newProfitVal = (int)$profitValEvent+$newPl;
                }
            }
        }

        if($isTrueExpose == false){
            return ['is_true' => $isTrueExpose, 'msg' => 'Your max expose limit for this ' . $expose_type . ' ! Bet can not placed!!'];
        }else{
            return ['is_true' => $isTrue, 'msg' => 'Your max profit limit for this ' . $type . ' ! Bet can not placed!!'];
        }
       }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }   

    }


    public function checkExposeBalance($uid, $request)
    {
      try{

        $m_type= $request['m_type'];
        $marketId= $request['market_id'];
        $eventId= $request['event_id'];
        $resultArray=[];
        $dataExpose = $maxBal['expose'] = $maxBal['plus'] = $balPlus = $balPlus1 = $balExpose = $balExpose2 = $profitLossNew = [];

        $minExpose=0;   $maxProfit =0;
        if( $m_type == 'fancy2' ||$m_type == 'fancy') {
            if (!empty($marketId)) {
                $profitLossData = $this->getProfitLossFancyOnZeroFinal($uid, $marketId, $m_type);
                if ($profitLossData != null) {
                    $minExpose = min($profitLossData);
                }

                if ($profitLossData != null) {
                    $maxProfit = max($profitLossData);
                }
            }
        }

          if( $m_type == 'meter' ) {
              if (!empty($marketId)) {
                  $profitLossData = $this->getProfitLossMeterOnZeroFinal($uid, $marketId, $m_type);
                  if ($profitLossData != null) {
                      $minExpose = $profitLossData;
                      $maxProfit = 0;
                  }
              }
          }

          if($m_type == 'USDINR' || $m_type == 'GOLD'|| $m_type == 'SILVER'|| $m_type == 'EURINR'|| $m_type == 'GBPINR'|| $m_type == 'ALUMINIUM' || $m_type == 'COPPER' || $m_type == 'CRUDEOIL' || $m_type == 'ZINC' || $request->m_type == 'BANKNIFTY' || $request->m_type == 'NIFTY'){
              if (!empty($marketId)) {
                  $profitLossData = $this->getProfitLossBinaryOnZeroFinal($uid, $marketId, $m_type);
                  if ($profitLossData != null) {
                      $minExpose = min($profitLossData);
                  }

                  if ($profitLossData != null) {
                      $maxProfit = max($profitLossData);
                  }
              }
          }

        if($m_type == 'fancy3' || $m_type == 'oddeven'){
            if( !empty( $marketId) ) {
                $profitLossData = $this->getProfitLossSingleFancyOnZeroFinal($uid,$marketId, $m_type);
                if ($profitLossData != null) {
                    $minExpose = min($profitLossData);
                }

                if( $profitLossData != null ){
                    $maxProfit = max($profitLossData);
                }
            }
        }


        if($m_type == 'ballbyball') {
            if (!empty($marketId)) {
                $profitLossData = $this->getProfitLossBallByBallFinal($uid, $marketId, $m_type);
                if ($profitLossData != null) {
                    $minExpose = min($profitLossData);
                }

                if ($profitLossData != null) {
                    $maxProfit = max($profitLossData);
                }
            }
        }

        if( $m_type== 'khado') {
            if( !empty( $marketId) ) {
                $profitLossData = $this->getProfitLossKhadoFinal($uid,$marketId, $m_type);
                if ($profitLossData != null) {
                    $minExpose = min($profitLossData);
                }

                if( $profitLossData != null ){
                    $maxProfit = max($profitLossData);
                }
            }
        }


        if($m_type == 'cricket_casino'){
            if( !empty( $marketId) ) {
                $balExpose = $balPlus = [];
                for($n=0;$n<10;$n++){
                    $profitLoss = $this->getLotteryProfitLossFinal($uid,$eventId,$marketId,$n);
                    if( $profitLoss < 0 ){
                        $balExpose[] = $profitLoss;
                    }else{
                        $balPlus[] = $profitLoss;
                    }
                }

                if( $balExpose != null ){
                    $minExpose = min($balExpose);
                }
                if( $balPlus != null ){
                    $maxProfit = max($balPlus);
                }
            }
        }

         if($m_type == 'jackpot'){
            if( !empty( $marketId) ) {
               
                 $marketList = DB::table('tbl_bet_pending_4')->select('secId','mid','eid','win','loss')
                  ->where([['uid',$uid],['eid',$eventId],['mid',$marketId],['result','PENDING'],['mType',$m_type]])
                  ->whereIn('status',[1,5])
                  ->orderBy('created_on' ,'DESC')
                   ->get();

                $balExpose = $balPlus = $betmarkrtID = [];
                if( $marketList != null ){
                    $betarray = [];

                    foreach ( $marketList as $market ){
                        $betarray[] = $market;
                        $betsecID[] = $market->secId;
                        //$eventId = $market['event_id'];
                    }

                    if(!empty( $betsecID ) &&  $eventId != 0 ){
                      
                      // $gameid = 1;
                        $cache = Redis::connection();
                        $listData=$cache->get("Jackpot_type");
                        $listData = json_decode($listData);

                        foreach ($listData as $value) {
                         $gameid = $value->id;
                      
                       $jackpotMarket = $cache->get("Jackpot_" . $gameid . "_" . $marketId);
                       $jackpotMarket = json_decode($jackpotMarket);

                        $total = $expose = $count = 0;  $totalLoss1=0;
                        $final_bet_array = $expose_array = $jackpot_array = [];

                         if(!empty($jackpotMarket)){
                        foreach ($jackpotMarket as $key => $jackpotarray ) {

                        
                            $rate = $jackpotarray->rate;
                            $secId = $jackpotarray->secId;

                            $total = $totalWin = $totalLoss = 0;

                            foreach ($betarray as $key => $betarrays) {
                                if($secId!= $betarrays->secId){
                                    $totalLoss= $totalLoss+$betarrays->loss;
                                    $totalLoss1=$totalLoss;
                                }
                            }

                            $total1=0;
                            foreach ($betarray as $key => $betarrays) {
                                if($secId== $betarrays->secId){
                                    $totalWin= $totalWin+$betarrays->win;
                                    $total1 = $totalWin-$totalLoss;
                                    //$this->updateUserBetExpose( $uid, $eventId, $market_id, $total1,$totalWin,$totalLoss1,0 ,$session_type );
                                }
                            }

                            $total = $totalWin-$totalLoss;

                            if( $total < 0 ){
                                $balExpose[] = $total;
                            }else{
                                $balPlus[] = $total;
                            }

                        }
                    }
                }

                        if( $balExpose != null ){
                            $minExpose = min($balExpose);
                            // $this->updateUserExpose( $uid,$eventId,$eventId.'-JKPT',$minExpose );
                        }

                        if( $balPlus != null ){
                            $maxProfit = max($balPlus);
                            //$this->updateUserProfit( $uid,$eventId,$eventId.'-JKPT',$maxProfit );
                        }
                    }
                }
            }

        }

        $resultArray=array('expose'=>$minExpose,'profit'=>$maxProfit);

        return $resultArray;

      }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }   

    }



  // Cricket: get Lottery Profit Loss On Bet
    public function getLotteryProfitLossFinalOLD($userId,$eventId,$marketId ,$selectionId)
    {
      try{

        $total = 0;
        //$userId = \Yii::$app->user->id;
       
        $where = [['mType','cricket_casino'], ['uid', $userId], ['mid', $marketId],['eid',$eventId]];
        // IF RUNNER WIN

        $betWinList = DB::table('tbl_bet_pending_4')
            ->where($where)->whereIn('status',[1,5])->where('secId',$selectionId)->sum("win");

        // IF RUNNER LOSS
        
        $betLossList = DB::table('tbl_bet_pending_4')
            ->where($where)->whereIn('status',[1,5])->where('secId','!=',$selectionId)->sum("loss");

        if( $betWinList == null ){
            $totalWin = 0;
        }else{ $totalWin = $betWinList; }

        if( $betLossList == null ){
            $totalLoss = 0;
        }else{ $totalLoss = (-1)*$betLossList; }

        $total = $totalWin+$totalLoss;

        return $total;

     }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }   

    }

    public function getLotteryProfitLossFinal($userId,$eventId,$marketId ,$selectionId)
    {
        try{

            $total = 0;
            $where = [['mType','cricket_casino'], ['uid', $userId], ['mid', $marketId],['eid',$eventId]];
            $betList = DB::table('tbl_bet_pending_4')->select(['win','loss','secId'])
                ->where($where)->whereIn('status',[1,5])->get();
            $totalWin = $totalLoss = 0;
            if( $betList->isNotEmpty() ){
                foreach ($betList as $list){
                    if( $list->secId == $selectionId ){
                        $totalWin = $totalWin+$list->win; // IF RUNNER WIN
                    }else{
                        $totalLoss = $totalLoss+$list->loss; // IF RUNNER LOSS
                    }
                }
            }

            $total = $totalWin-$totalLoss;

            return $total;

        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    public function getProfitLossSingleFancyOnZeroFinalOLD($userId,$marketId,$m_Type)
    {
      try{

        //$userId = \Yii::$app->user->id;
        $yes_sum =$yes_loss= $no_loss= $no_win = 0;
        $where = [['result', 'Pending'], ['bType', 'back'], ['mType', $m_Type], ['uid', $userId], ['mid', $marketId]];
        $yeswinbetList = DB::table('tbl_bet_pending_2')
            ->where($where)->whereIn('status',[1,5])->sum("win");

        $yes_sum = $yeswinbetList;


        $where = [['result', 'Pending'], ['bType', 'back'], ['mType', $m_Type], ['uid', $userId], ['mid', $marketId]];
        $yeslossbetList = DB::table('tbl_bet_pending_2')
            ->where($where)->whereIn('status',[1,5])->sum("loss");
            $yes_loss = $yeslossbetList;



        $where = [['result', 'Pending'], ['bType', 'lay'], ['mType', $m_Type], ['uid', $userId], ['mid', $marketId]];
        $nowinbetList = DB::table('tbl_bet_pending_2')
            ->where($where)->whereIn('status',[1,5])->sum("win");
              $no_win =$nowinbetList;


        $nolossbetList = DB::table('tbl_bet_pending_2')
            ->where($where)->whereIn('status',[1,5])->sum("loss");
            $no_loss = $nolossbetList;


        //print_r($nobetList);
        $yes_profitloss = round($yes_sum - $no_loss);
        $no_profitloss = round($no_win - $yes_loss) ;

        $newbetresult[] = $yes_profitloss;
        $newbetresult[] = $no_profitloss;

        return $newbetresult;

      }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }


    }

    public function getProfitLossSingleFancyOnZeroFinal($userId,$marketId,$m_Type)
    {
        try{

            $yesWin = $yesLoss = $noLoss = $noWin = 0;
            $where = [['result', 'Pending'], ['mType', $m_Type], ['uid', $userId], ['mid', $marketId]];
            $yesWinBetList = DB::table('tbl_bet_pending_2')->select(['win','loss','bType'])
                ->where($where)->whereIn('status',[1,5])->get();
            if( $yesWinBetList->isNotEmpty() ){
                foreach ( $yesWinBetList as $list ){
                    if( $list->bType == 'back' ){
                        $yesWin = $yesWin+$list->win;
                        $yesLoss = $yesLoss+$list->loss;
                    }
                    if( $list->bType == 'lay' ){
                        $noWin = $noWin+$list->win;
                        $noLoss = $noLoss+$list->loss;
                    }
                }
            }

            /*
             $where = [['result', 'Pending'], ['bType', 'back'], ['mType', $m_Type], ['uid', $userId], ['mid', $marketId]];
            $yesLossBetList = DB::table('tbl_bet_pending_2')->select('loss')
                ->where($where)->whereIn('status',[1,5])->get();
            if( $yesLossBetList->isNotEmpty() ){
                foreach ( $yesLossBetList as $list ){
                    $yesLoss = $yesLoss+$list->loss;
                }
            }

            $where = [['result', 'Pending'], ['bType', 'lay'], ['mType', $m_Type], ['uid', $userId], ['mid', $marketId]];
            $noWinBetList = DB::table('tbl_bet_pending_2')->select('win')
                ->where($where)->whereIn('status',[1,5])->get();
            if( $noWinBetList->isNotEmpty() ){
                foreach ( $noWinBetList as $list ){
                    $noWin = $noWin+$list->win;
                }
            }
            $noLossBetList = DB::table('tbl_bet_pending_2')->select('loss')
                ->where($where)->whereIn('status',[1,5])->get();
            if( $noLossBetList->isNotEmpty() ){
                foreach ( $noLossBetList as $list ){
                    $noLoss = $noLoss+$list->loss;
                }
            }
            */

            $yesProfitLoss = round($yesWin-$noLoss);
            $noProfitLoss = round($noWin-$yesLoss) ;

            $newBetResult[] = $yesProfitLoss;
            $newBetResult[] = $noProfitLoss;
            return $newBetResult;

        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }


    }

    public function getProfitLossKhadoFinal($userId,$marketId,$m_type)
    {

       try{

        $query = DB::table('tbl_bet_pending_3')->select(['bType as bet_type','price','win','loss'])
            ->where([['mType',$m_type],['uid',$userId],['result','Pending'],['mid',$marketId]])->whereIn('status',[1,5]);
        $betList = $query->get();

        $result = $newbetresult = [];
        if( $betList != null ){
            $result = [];
            $betresult = [];
            $min = 0;
            $max = 0;


            foreach ($betList as $index => $bet) {
                $betresult[] = array('price'=>$bet->price,'bet_type'=>$bet->bet_type,'loss'=>$bet->loss,'win'=>$bet->win);
                if ($index == 0) {
                    $min = $bet->price;
                    $max = $bet->price;
                }
                if ($min > $bet->price)
                    $min = $bet->price;
                if ($max < $bet->price)
                    $max = $bet->price;
            }

            $min = $min-1;
            $max = $max+1;
            $betarray = []; $bet_type='';
            $win = $loss=0;
            $count = $min;
            $totalbetcount = count($betresult);
            foreach ($betresult as $key => $value) {
                $val = $value['price']- $count;
                $minval = $value['price'] -$min;
                $maxval = $max-$value['price'];
                $bet_type = $value['bet_type'];
                $loss = $value['loss'];
                $newresult = [];
                $top = $bottom = $profitcount = $losscount = 0;

                for( $i= 0; $i < $minval; $i++){
                    $newresult[] = array( 'count'=>$count, 'price' => $value['price'], 'bet_type' => $value['bet_type'], 'totalbetcount' => $totalbetcount, 'expose'=> (-1)*$value['loss'] );
                    $count++;
                }

                for( $i= 0; $i <= $maxval; $i++){
                    $newresult[] = array( 'count' => $count, 'price' => $value['price'], 'bet_type' => $value['bet_type'], 'totalbetcount' => $totalbetcount, 'expose' => $value['win'] );
                    $count++;
                }
                $result[] = array( 'count' => $value['price'], 'bet_type' => $value['bet_type'], 'profit'=>$top, 'loss'=>$bottom, 'profitcount' => $profitcount, 'losscount' => $losscount, 'newarray' => $newresult );
            }

            $newbetarray = $newbetresult = [];
            $totalmaxcount = $max-$min;
            if( $totalmaxcount > 0 ){
                for( $i = 0; $i < $totalmaxcount; $i++ ){
                    $newbetarray1 = []; $finalexpose = 0;
                    for( $x = 0; $x < $totalbetcount; $x++ ){
                        $expose = $result[$x]['newarray'][$i]['expose'];
                        $finalexpose = $finalexpose+$expose;

                        $newbetarray1[] = array( 'bet_price' => $result[$x]['count'], 'bet_type' => $result[$x]['bet_type'], 'expose' => $expose );
                    }
                    $newbetresult[] = $finalexpose;
                    $newbetarray[] = array( 'exposearray' => $newbetarray1, 'finalexpose' => $finalexpose );
                }
            }


            return $newbetresult;
        }

       }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }   

    }

    public function getProfitLossFancyOnZeroFinal($userId,$marketId,$m_type)
    {

       try{

        //$userId = \Yii::$app->user->id;
        $query = DB::table('tbl_bet_pending_2')->select(['bType','price','win','loss'])
            ->where([['mType',$m_type],['uid',$userId],['result','Pending'],['mid',$marketId]])->whereIn('status',[1,5]);
        $betList = $query->get();
        //print_r($betList); exit;
        $result = $newbetresult = [];
        if( $betList != null ){
            $result = [];
            $betresult = [];
            $min = 0;
            $max = 0;


            foreach ($betList as $index => $bet) {
                $betresult[] = array('price'=>$bet->price,'bet_type'=>$bet->bType,'loss'=>$bet->loss,'win'=>$bet->win);
                if ($index == 0) {
                    $min = $bet->price;
                    $max = $bet->price;
                }
                if ($min > $bet->price)
                    $min = $bet->price;
                if ($max < $bet->price)
                    $max = $bet->price;
            }

            $min = $min-1;
            $max = $max+1;
            $betarray = []; $bet_type='';
            $win = $loss=0;
            $count = $min;
            $totalbetcount = count($betresult);
            foreach ($betresult as $key => $value) {
                $val = $value['price']- $count;
                $minval = $value['price'] -$min;
                $maxval = $max-$value['price'];
                $bet_type = $value['bet_type'];
                $loss = $value['loss'];
                $newresult = [];
                $top = $bottom = $profitcount = $losscount = 0;

                for( $i= 0; $i < $minval; $i++){
                    if($bet_type == 'no'){
                        $top = $top+$value['win'];
                        $profitcount++;
                        $newresult[] = array( 'count' => $count, 'price' => $value['price'], 'bet_type' => $value['bet_type'], 'totalbetcount' => $totalbetcount, 'expose' => $value['win'] );
                    }else{
                        $bottom = $bottom + $value['loss'];
                        $losscount++;
                        $newresult[] = array( 'count'=>$count, 'price' => $value['price'], 'bet_type' => $value['bet_type'], 'totalbetcount' => $totalbetcount, 'expose'=> (-1)*$value['loss'] );
                    }
                    $count++;
                }

                for( $i= 0; $i <= $maxval; $i++){
                    if($bet_type == 'no'){
                        $newresult[] = array( 'count' => $count,'price' => $value['price'], 'bet_type' => $value['bet_type'], 'totalbetcount' => $totalbetcount, 'expose' => (-1)*$value['loss'] );
                        $bottom = $bottom + $value['loss'];
                        $losscount++;
                    }else{
                        $top = $top + $value['win'];
                        $profitcount++;
                        $newresult[] = array( 'count' => $count, 'price' => $value['price'], 'bet_type' => $value['bet_type'], 'totalbetcount' => $totalbetcount, 'expose' => $value['win'] );
                    }

                    $count++;
                }
                $result[] = array( 'count' => $value['price'], 'bet_type' => $value['bet_type'], 'profit'=>$top, 'loss'=>$bottom, 'profitcount' => $profitcount, 'losscount' => $losscount, 'newarray' => $newresult );
            }

            $newbetarray = $newbetresult = [];
            $totalmaxcount = $max-$min;
            if( $totalmaxcount > 0 ){
                for( $i = 0; $i < $totalmaxcount; $i++ ){
                    $newbetarray1 = []; $finalexpose = 0;
                    for( $x = 0; $x < $totalbetcount; $x++ ){
                        // echo "<pre>"; print_r($result[$x]['newarray']);echo "<br>";echo "<br>"; exit;
                        $expose = $result[$x]['newarray'][$i]['expose'];
                        $finalexpose = $finalexpose+$expose;

                        $newbetarray1[] = array( 'bet_price' => $result[$x]['count'], 'bet_type' => $result[$x]['bet_type'], 'expose' => $expose );
                    }
                    $newbetresult[] = $finalexpose;
                    $newbetarray[] = array( 'exposearray' => $newbetarray1, 'finalexpose' => $finalexpose );
                }
            }

            return $newbetresult;
        }

      }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }   
    }


    public function getProfitLossMeterOnZeroFinal($userId,$marketId,$m_type)
    {

        try{
            $finalexpose = 0;
            $betList = DB::table('tbl_bet_pending_3')->select(['size'])
                ->where([['mType',$m_type],['uid',$userId],['result','Pending'],['mid',$marketId]])
                ->whereIn('status',[1,5])->get();
            if( $betList != null ){
                foreach ($betList as $bet) {
                    $finalexpose = $finalexpose+( ($bet->size)*100 );
                }
                $finalexpose = (-1)*$finalexpose;
            }
            return $finalexpose;
        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }


    public function getProfitLossBinaryOnZeroFinal($userId,$marketId,$m_type)
    {

        try{

            //$userId = \Yii::$app->user->id;
            $query = DB::table('tbl_bet_pending_5')->select(['bType','price','win','loss'])
                ->where([['mType',$m_type],['uid',$userId],['result','Pending'],['mid',$marketId]])->whereIn('status',[1,5]);
            $betList = $query->get();
            //print_r($betList); exit;
            $result = $newbetresult = [];
            if( $betList != null ){
                $result = [];
                $betresult = [];
                $min = 0;
                $max = 0;


                foreach ($betList as $index => $bet) {
                    $betresult[] = array('price'=>$bet->price,'bet_type'=>$bet->bType,'loss'=>$bet->loss,'win'=>$bet->win);
                    if ($index == 0) {
                        $min = $bet->price;
                        $max = $bet->price;
                    }
                    if ($min > $bet->price)
                        $min = $bet->price;
                    if ($max < $bet->price)
                        $max = $bet->price;
                }

                $min = $min-1;
                $max = $max+1;
                $betarray = []; $bet_type='';
                $win = $loss=0;
                $count = $min;
                $totalbetcount = count($betresult);
                foreach ($betresult as $key => $value) {
                    $val = $value['price']- $count;
                    $minval = $value['price'] -$min;
                    $maxval = $max-$value['price'];
                    $bet_type = $value['bet_type'];
                    $loss = $value['loss'];
                    $newresult = [];
                    $top = $bottom = $profitcount = $losscount = 0;

                    for( $i= 0; $i < $minval; $i++){
                        if($bet_type == 'no'){
                            $top = $top+$value['win'];
                            $profitcount++;
                            $newresult[] = array( 'count' => $count, 'price' => $value['price'], 'bet_type' => $value['bet_type'], 'totalbetcount' => $totalbetcount, 'expose' => $value['win'] );
                        }else{
                            $bottom = $bottom + $value['loss'];
                            $losscount++;
                            $newresult[] = array( 'count'=>$count, 'price' => $value['price'], 'bet_type' => $value['bet_type'], 'totalbetcount' => $totalbetcount, 'expose'=> (-1)*$value['loss'] );
                        }
                        $count++;
                    }

                    for( $i= 0; $i <= $maxval; $i++){
                        if($bet_type == 'no'){
                            $newresult[] = array( 'count' => $count,'price' => $value['price'], 'bet_type' => $value['bet_type'], 'totalbetcount' => $totalbetcount, 'expose' => (-1)*$value['loss'] );
                            $bottom = $bottom + $value['loss'];
                            $losscount++;
                        }else{
                            $top = $top + $value['win'];
                            $profitcount++;
                            $newresult[] = array( 'count' => $count, 'price' => $value['price'], 'bet_type' => $value['bet_type'], 'totalbetcount' => $totalbetcount, 'expose' => $value['win'] );
                        }

                        $count++;
                    }
                    $result[] = array( 'count' => $value['price'], 'bet_type' => $value['bet_type'], 'profit'=>$top, 'loss'=>$bottom, 'profitcount' => $profitcount, 'losscount' => $losscount, 'newarray' => $newresult );
                }

                $newbetarray = $newbetresult = [];
                $totalmaxcount = $max-$min;
                if( $totalmaxcount > 0 ){
                    for( $i = 0; $i < $totalmaxcount; $i++ ){
                        $newbetarray1 = []; $finalexpose = 0;
                        for( $x = 0; $x < $totalbetcount; $x++ ){
                            // echo "<pre>"; print_r($result[$x]['newarray']);echo "<br>";echo "<br>"; exit;
                            $expose = $result[$x]['newarray'][$i]['expose'];
                            $finalexpose = $finalexpose+$expose;

                            $newbetarray1[] = array( 'bet_price' => $result[$x]['count'], 'bet_type' => $result[$x]['bet_type'], 'expose' => $expose );
                        }
                        $newbetresult[] = $finalexpose;
                        $newbetarray[] = array( 'exposearray' => $newbetarray1, 'finalexpose' => $finalexpose );
                    }
                }

                return $newbetresult;
            }

        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }

    public function getProfitLossBallByBallFinal($userId,$marketId,$m_type)
    {

      try{

        //$userId = \Yii::$app->user->id;
        $query = DB::table('tbl_bet_pending_3')->select(['bType','price','win','loss'])
            ->where([['mType',$m_type],['uid',$userId],['result','Pending'],['mid',$marketId]])->whereIn('status',[1,5]);
        $betList = $query->get();
        //print_r($betList); exit;
        $result = $newbetresult = [];
        if( $betList != null ){
            $result = [];
            $betresult = [];
            $min = 0;
            $max = 0;


            foreach ($betList as $index => $bet) {
                $betresult[] = array('price'=>$bet->price,'bet_type'=>$bet->bType,'loss'=>$bet->loss,'win'=>$bet->win);
                if ($index == 0) {
                    $min = $bet->price;
                    $max = $bet->price;
                }
                if ($min > $bet->price)
                    $min = $bet->price;
                if ($max < $bet->price)
                    $max = $bet->price;
            }

            $min = $min-1;
            $max = $max+1;
            $betarray = []; $bet_type='';
            $win = $loss=0;
            $count = $min;
            $totalbetcount = count($betresult);
            foreach ($betresult as $key => $value) {
                $val = $value['price']- $count;
                $minval = $value['price'] -$min;
                $maxval = $max-$value['price'];
                $bet_type = $value['bet_type'];
                $loss = $value['loss'];
                $newresult = [];
                $top = $bottom = $profitcount = $losscount = 0;

                for( $i= 0; $i < $minval; $i++){
                    if($bet_type == 'no'){
                        $top = $top+$value['win'];
                        $profitcount++;
                        $newresult[] = array( 'count' => $count, 'price' => $value['price'], 'bet_type' => $value['bet_type'], 'totalbetcount' => $totalbetcount, 'expose' => $value['win'] );
                    }else{
                        $bottom = $bottom + $value['loss'];
                        $losscount++;
                        $newresult[] = array( 'count'=>$count, 'price' => $value['price'], 'bet_type' => $value['bet_type'], 'totalbetcount' => $totalbetcount, 'expose'=> (-1)*$value['loss'] );
                    }
                    $count++;
                }

                for( $i= 0; $i <= $maxval; $i++){
                    if($bet_type == 'no'){
                        $newresult[] = array( 'count' => $count,'price' => $value['price'], 'bet_type' => $value['bet_type'], 'totalbetcount' => $totalbetcount, 'expose' => (-1)*$value['loss'] );
                        $bottom = $bottom + $value['loss'];
                        $losscount++;
                    }else{
                        $top = $top + $value['win'];
                        $profitcount++;
                        $newresult[] = array( 'count' => $count, 'price' => $value['price'], 'bet_type' => $value['bet_type'], 'totalbetcount' => $totalbetcount, 'expose' => $value['win'] );
                    }

                    $count++;
                }
                $result[] = array( 'count' => $value['price'], 'bet_type' => $value['bet_type'], 'profit'=>$top, 'loss'=>$bottom, 'profitcount' => $profitcount, 'losscount' => $losscount, 'newarray' => $newresult );
            }

            $newbetarray = $newbetresult = [];
            $totalmaxcount = $max-$min;
            if( $totalmaxcount > 0 ){
                for( $i = 0; $i < $totalmaxcount; $i++ ){
                    $newbetarray1 = []; $finalexpose = 0;
                    for( $x = 0; $x < $totalbetcount; $x++ ){
                        // echo "<pre>"; print_r($result[$x]['newarray']);echo "<br>";echo "<br>"; exit;
                        $expose = $result[$x]['newarray'][$i]['expose'];
                        $finalexpose = $finalexpose+$expose;

                        $newbetarray1[] = array( 'bet_price' => $result[$x]['count'], 'bet_type' => $result[$x]['bet_type'], 'expose' => $expose );
                    }
                    $newbetresult[] = $finalexpose;
                    $newbetarray[] = array( 'exposearray' => $newbetarray1, 'finalexpose' => $finalexpose );
                }
            }

            return $newbetresult;
        }

      }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }   
    }

  public function sportMarketLimits($uid){


     $sportMarket = DB::table('tbl_user_sport_market_limits')
                     ->select('*')
                     ->where('uid',$uid)->first();

      if(!empty($sportMarket)){
        return $sportMarket;
      }else{
        return [];
      }



  }

  // check verify data with redis data
  public function verifyWithRedis($request){

        try{

          $redis = Redis::connection();
          $eventName = '';
          $marketId = $request->market_id;
          $eventId = $request->event_id;
          $sportId = $request->sport_id;
          $mType = $request->m_type;
          $betType = $request->bet_type;
          $marketName = $request->market_name;
          $runnerName = $request->runner;
          $secId = $request->sec_id;
          if( $mType == 'jackpot' ){
              $gameId = $request->gameId;
              $eventName = $request->eventname;
              $marketDataJson = $redis->get("Jackpot_market_".$gameId."_".$eventId."_".$marketId);
              $marketData = json_decode($marketDataJson);
              $marketRunnersDataJson = $redis->get("Jackpot_" . $gameId . "_" . $marketId);
              $marketRunnersData = json_decode($marketRunnersDataJson);
          }else{
              $marketDataJson = $redis->get("Market_" . $marketId);
              $marketData = json_decode($marketDataJson);
          }

          if($mType == 'cricket_casino'){ print_r($marketData); }

          $response = ["status" => 0, "code" => 400, "data" => null, "message" => "Invalid Data! Plz Try Again 123"];

          if( empty($marketData) ){
              return $response; exit;
          }
          if( !empty($marketData) ){
              if( isset($marketData->event_id) && $marketData->event_id != $eventId ){
                  return $response; exit;
              }
              if( isset($marketData->eventId) && $marketData->eventId != $eventId ){
                  return $response; exit;
              }
              if( isset($marketData->eid) && $marketData->eid != $eventId ){
                  return $response; exit;
              }
              if( isset($marketData->sportId) && $marketData->sportId != $sportId ){
                  return $response; exit;
              }
              if( isset($marketData->sid) && $marketData->sid != $sportId ){
                  return $response; exit;
              }
              if( isset($marketData->mType) && $marketData->mType != $mType ){
                  return $response; exit;
              }

              if( !isset($marketData->mType) ){
                  if( isset($marketData->slug) && $marketData->slug != $mType ){
                      return $response; exit;
                  }
              }

              if( isset($marketData->sid) && $marketData->sid == 10 ){
                  if($mType != 'jackpot'){ return $response; exit; }
                  if($betType != 'back'){ return $response; exit; }
                  if( str_replace(' ','_', strtolower( trim($marketData->title) )) != str_replace(' ','_',  strtolower( trim($eventName) ) ) ){
                      return $response; exit;
                  }
              }
              if( isset($marketData->sid) && $marketData->sid == 11 ){
                  if($mType != 'cricket_casino'){ return $response; exit; }
                  if($betType != 'back'){ return $response; exit; }
                  if( str_replace(' ','_', strtolower( trim($marketData->title) )) != str_replace(' ','_',  strtolower( trim($request->runner) ) ) ){
                      return $response; exit;
                  }
              }
              if( isset($marketData->title) ){
                  if( str_replace(' ','_', strtolower( trim($marketData->title) )) != str_replace(' ','_',  strtolower( trim($marketName) ) ) ){
                      return $response; exit;
                  }
              }
              if( isset($marketData->marketName) ){
                  if( str_replace(' ','_', strtolower( trim($marketData->marketName) )) != str_replace(' ','_',  strtolower( trim($marketName) ) ) ){
                      return $response; exit;
                  }
              }
              if( isset($marketData->runners) ){
                  $secIdArr = []; $secIdRunner = []; $marketMtype = null;
                  $marketRunnersData = json_decode($marketData->runners);
                  foreach ( $marketRunnersData as $runners ){
                      $secIdArr[] = $runners->secId;
                      $secIdRunner[$runners->secId] = $runners->runner;
                      $marketMtype = isset($runners->mType) ? $runners->mType : null;
                  }

                  if( $secIdArr != null && !in_array( $secId , $secIdArr ) ){
                      return $response; exit;
                  }

                  if( $secIdRunner != null && isset( $secIdRunner[$secId] ) ){
                      if( str_replace(' ','_', strtolower( trim($secIdRunner[$secId]) )) != str_replace(' ','_',  strtolower( trim($runnerName) ) ) ){
                          return $response; exit;
                      }
                  }

                  if( $marketMtype != null && $marketMtype != $mType ){
                      return $response; exit;
                  }
              }

              if( $mType == 'jackpot' ){
                  if( !empty($marketRunnersData) ){
                      return $response; exit;
                  }
                  $secIdArr = $secIdRunner = [];
                  foreach ( $marketRunnersData as $runners ){
                      $secIdArr[] = $runners->secId;
                      $secIdRunner[$runners->secId]['rate'] = $runners->rate;
                      $runner = $request->eventname." (".$runners->col_1."-".$runners->col_2." / ".$runners->col_3."-".$runners->col_4.")";
                      $secIdRunner[ $runners->secId ]['runner'] = strtolower( trim($runner) );
                  }

                  if( $secIdArr != null && !in_array( $secId , $secIdArr ) ){
                      return $response; exit;
                  }

                  if( $secIdRunner != null && isset( $secIdRunner[$secId]['rate'] ) ){
                      if( trim($secIdRunner[$secId]['rate']) != trim($request->rate) ){
                          return $response; exit;
                      }
                      if( trim($secIdRunner[$secId]['rate']) != trim($request->price) ){
                          return $response; exit;
                      }
                  }

                  if( $secIdRunner != null && isset( $secIdRunner[$secId]['runner'] ) ){
                      if( str_replace(' ','_', $secIdRunner[$secId]['runner'] ) != str_replace(' ','_',  strtolower( trim($runnerName) ) ) ){
                          return $response; exit;
                      }
                  }
              }

          }

          return;

      }catch (\Exception $e) {
        $response = $this->errorLog($e);
        return response()->json($response, 501);
      }

  }

  public function getOldMarketExpose($uid,$marketId){
      $oldMarketExpose = 0;
      $userExpose = DB::table('tbl_user_market_expose')->select('expose')
          ->where([['uid',$uid],['status',1],['mid','!=',$marketId]])->get();

      if( $userExpose->isNotEmpty() ){
          foreach ( $userExpose as $uex ){
              if( $uex->expose > 0 ){
                  $oldMarketExpose = $oldMarketExpose+(-1)*$uex->expose;
              }
          }
      }

      return $oldMarketExpose;
  }

    // Function to get the client IP address
   public function get_client_ip() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }



}
