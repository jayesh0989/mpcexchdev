<?php
namespace App\Http\Controllers\Event;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use stdClass;


class EventCronJobController extends Controller
{

    public $FANCY_KEY = 'ManualFancy-';
    public $BOOKMAKER_KEY = 'BookMaker-';
    /**
     * Handles Create New serries event Request
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    /*public function sportList(Request $request)
    {
        $cache = Redis::connection();
        $sportList=$cache->get("SportList");
        return $sportList;
    }*/


    public function eventList(Request $request)
    {
        $eventDataArray=null;
        $cache = Redis::connection();
        $sportList=$cache->get("SportList");
        $sportList = json_decode($sportList);
        if(!empty($sportList)) {
            foreach ($sportList as $sport_List) {
                $sportId = $sport_List->sportId;
                $sport_name = $sport_List->name;
                $eventData = $cache->get("Event_List_" . $sportId);
                $eventData = json_decode($eventData);
                $eventDataArray[] = array('sportId' => $sportId, 'sport_name' => $sport_name, 'eventData' => $eventData);
            }
            $eventDataArray = json_encode($eventDataArray);
            return $eventDataArray;
        }
    }

    public function allCronJobs(){
        $sportList = $this->sportList();
    }
    

    public function sportList(){
        $cache = Redis::connection();
        $sportData=null; $eventDataArray=null;
        $sportList = DB::table('tbl_sport')->select('*')->where('status',1)->limit(1000)->get();
 
        if(!empty( $sportList)) {

        $sportList = json_encode($sportList);
        $cache->set('SportList_bt', $sportList);
        
       /* foreach ($sportList as $sport_List) {
            $sportId = $sport_List->sportId;
            $sport_name = $sport_List->name;
            $sportDT = json_encode($sport_List);
            $cache->set('SportBT_'.$sportId, $sportDT);
        }*/
    }
    }

}
