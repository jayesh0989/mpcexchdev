<?php

namespace App\Http\Controllers\Rule;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Collection;
use Illuminate\Support\MessageBag;
use Illuminate\Http\UploadedFile;

use Illuminate\Routing\Controller as BaseController;
// use App\User;
use App\Http\Controllers\Controller;
use JWTAuth;
use App\Session\UserData;


use JWTAuthException;


class RulesController extends Controller
{

    public function rules(Request $request)
    {

          $response = response()->json(['status'=>0,'message'=>'Bad request !!!','data'=>null]);

      try{
          $request_data = json_decode( file_get_contents('php://input') , JSON_FORCE_OBJECT );

           $sportArr = DB::table('tbl_rules')->select('category_name')
                       ->where('status', 1)
                       ->get();

          $sportArr = $sportArr->toArray();

          // $sportArr = json_decode(json_encode($sportArr), True);
           $sportArr = array_unique($sportArr,SORT_REGULAR);

           if(!empty($sportArr)){

            $arr1 =[];
            foreach ($sportArr as $key => $value) {
                # code...
                 $rules=[]; 
                 $sport = $value->category_name;
                 $sports[] = $value->category_name;

                 $rules = DB::table('tbl_rules')->select('id','category_name','sub_category','rule')
                       ->where([['category_name', $sport],['status', 1]])
                       ->get();
         
             
 
              $arr =[]; 
                 foreach ($rules as $key => $rules) {

                    $arr[] =  [
                                    
                            'title' => $rules->sub_category,
                            'detail' => $rules->rule
                          ];

                  }

                  if($arr == null || $arr == ' '){
                     $arr = null;
                  }

                   $arr1[]= [
                           
                           'sportName' => $value->category_name,
                           'rule'      => $arr
                            ];
         }

         
        $response = response()->json(['status'=>1,'code'=>200,'data'=>$arr1,'message'=> "Data Found !!"]);
      }else{

        $response = response()->json(['status'=>0,'code'=>404,'data'=>null,'message'=> "Data Not Found !!"]);
      }

       return $response;
    }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

       
      }
   
   

 
}
