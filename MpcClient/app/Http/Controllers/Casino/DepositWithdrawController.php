<?php

namespace App\Http\Controllers\Casino;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;
class DepositWithdrawController extends Controller
{
    public function casinoTransaction(Request $request){
      try {
            $user             = Auth::user();
            $amount           = (int)$request->get('amount');
            $transactionType  = $request->get('transactionType');
            $userInfo         = DB::table('tbl_user_info')->where('uid',$user->id)->first();
            $user_casino_info = DB::connection('mongodb')->table('tbl_casino_user')->where('uid',$user->id)->first();
            //transaction type 1 for deposit
            if($transactionType == 1){
              if($amount <= $userInfo->balance){

                $token   = md5($user->systemId.'-'.$user->uid.'-'.$user->username.'-BETEX567');
                
                $balance = (int) $userInfo->balance - $amount;
                $deduct  = DB::connection('mysql')->table('tbl_user_info')->where('uid',$user->id)->update(['balance' => $balance ]);
                if($deduct){
                  if(!empty($user_casino_info)){
                    $user_total_casino_balance = (int)($user_casino_info['balance'] + $amount);
                    DB::connection('mongodb')->table('tbl_casino_user')->where('uid',$user->id)->update(['balance' => $user_total_casino_balance]);
                  }else{
                  $deposit  = DB::connection('mongodb')->table('tbl_casino_user')->insert(
                                                                                          [ 
                                                                                            'uid'     => $user->id,
                                                                                            'username'=> $user->username,
                                                                                            'systemId'=> $user->systemId,
                                                                                            'token'   => $token,
                                                                                            'balance' => $amount,
                                                                                            'status'  => 1 
                                                                                          ]
                                                                                        );
                  }
                  
                 return response()->json(['status'=>1,'code'=>200,'data'=>null,'message'=>'Amount deposited successfully.']);
                }else{
                  return response()->json(['status'=>0,'code'=>200,'data'=>null,'message'=>'Failed to deposit amount in casino.']);
                }
              }
              else
              {
                return response()->json(['status'=>0,'code'=>200,'data'=>null,'message'=>"Maximum deposit amount is ".$userInfo->balance]);
              }
            }

            //transaction type 2 for withdraw
            if($transactionType == 2){
              if(empty($user_casino_info['balance'])){
                return response()->json(['status'=>0,'code'=>200,'data'=>null,'message'=>'You have zero balance in casino.']);
              }
              if($user_casino_info['balance'] >= $amount){
                $user_casino_balance = (int) $user_casino_info['balance'] - $amount;
                $deduct = DB::connection('mongodb')->table('tbl_casino_user')->where('uid',$user->id)->update(['balance' => $user_casino_balance]);
                
                if($deduct){
                  $balance = (int) $userInfo->balance + $amount;
                  DB::table('tbl_user_info')->where('uid',$user->id)->update(['balance' => $balance ]);
                  return response()->json(['status'=>1,'code'=>200,'data'=>null,'message'=>'Amount withdraw successfully.']);
                }else{
                return response()->json(['status'=>0,'code'=>200,'data'=>null,'message'=>'Failed to withdraw amount from casino.']);
                }
              }
              else
              {
                return response()->json(['status'=>0,'code'=>200,'data'=>null,'message'=>"Maximum withdrawable amount is ".$user_casino_info['balance']]);
              }
            }

      } catch (\Exception $e) {
        dd($e);
      }
    }
}
