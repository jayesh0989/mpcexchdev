<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Log;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\User;

class LogController extends Controller
{
    public function mpcLog(Request $request){
        $log = storage_path().'/logs';
            
            if(isset($log) && file_exists($log)){
                    $files = scandir($log);
                    rsort($files);
                    $flength = count($files);
                
                    for($x = 0; $x < $flength; $x++) {
                        $arr[] =  $files[$x];
                    }

                    $currentPage = LengthAwarePaginator::resolveCurrentPage();
 
                    $arrCollection = collect($arr);
             
                    $perPage = 10;
             
                    $currentPageproducts = $arrCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
             
                    $log_files= new LengthAwarePaginator($currentPageproducts , count($arrCollection), $perPage);
             
                    $log_files->setPath($request->url());

            }
            else{
                echo "File Not Found";
            }

        return view('mpc-log',['LogFile'=>$log_files]);
    }

    public function mpcActivityLog(Request $request){
        $activity = storage_path().'/activity';
        if(isset($activity) && file_exists($activity)){
            $files = scandir($activity);
            rsort($files);
            $flength = count($files);
            
            for($x = 0; $x < $flength; $x++) {
                $arr[] =  $files[$x];
            }
              $arrCollection = collect($arr);
              $currentPage = LengthAwarePaginator::resolveCurrentPage();
              $perPage = 10;
       
              $currentPageproducts = $arrCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
       
              $activity_files = new LengthAwarePaginator($currentPageproducts , count($arrCollection), $perPage);
       
              $activity_files->setPath($request->url());
        }
        else{
            echo "File Not Found";
        }
        return view('mpc-activity-log',['ActivityFile'=>$activity_files]);
    }

    public function mpcCasinoLog(Request $request){
        $casino = storage_path().'/casino';
        if(isset($casino) && file_exists($casino)){
            $files = scandir($casino);
            rsort($files);
            $flength = count($files);
            
            for($x = 0; $x < $flength; $x++) {
                $arr[] =  $files[$x];
            }
              $arrCollection = collect($arr);
              $currentPage = LengthAwarePaginator::resolveCurrentPage();
              $perPage = 10;
       
              $currentPageproducts = $arrCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
       
              $casino = new LengthAwarePaginator($currentPageproducts , count($arrCollection), $perPage);
       
              $casino->setPath($request->url());
        }
        else{
            echo "File Not Found";
        }
        return view('mpc-casino-log',['casino'=>$casino]);
    }

    public function viewLog(Request $request){
        $date = $request->date;
        $id = $request->id;
        if (isset($id)) {
            $log = [$id];
            return view('view-log',['log' => $log]);
        }
        elseif(isset($date)){
            $log = ["20210201-".$date.'.'."log"];
            return view('view-log',['log' => $log]);
        }
        else{
            echo "select date";
        }
    }

    public function viewActivityLog(Request $request){
        $date = $request->date;
        $id = $request->id;
        if (isset($date)) {
            $activity = ["20210201-".$date.'.'."log"];
            return view('view-activity-log',['activity' => $activity]);
        }
        elseif(isset($id)){
            $activity = [$id];
            return view('view-activity-log',['activity' => $activity]);
        }
        else{
            echo "select date";
        }
    }

    public function viewCasinoLog(Request $request){
        $date = $request->date;
        $id = $request->id;
        if (isset($date)) {
            $filename = ["20210201-".$date.'.'."log"];
            return view('view-casino-log',['filename' => $filename]);
        }
        elseif(isset($id)){
            $casino = [$id];
            return view('view-casino-log',['casino' => $casino]);
        }
        else{
            echo "select date";
        }
    }
}
