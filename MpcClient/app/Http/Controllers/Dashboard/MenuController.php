<?php

namespace App\Http\Controllers\Dashboard;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redis;


class MenuController extends Controller
{
  public function frontMenuList(Request $request)
  {
    $response = response()->json(['status'=>0,'code'=>400,'message'=>'Bad request !!!','data'=>null]);
    try{
        $uid = Auth::user()->id;
        $url       = '';
        $menu      = [];
  	    $cache     = Redis::connection();
        $sportList = $cache->get("SportList");
        $sportList = json_decode($sportList,true);
        $serverUrl = DB::table('tbl_common_setting')->select('value')->where([['status',1],['key_name','SERVER_URL']])->first();
        if(!empty($serverUrl)){
         $url = $serverUrl->value.'/'.'dashboard_images';
        }
        
        $unblockSport = $this->checkUnBlockSportList($uid);
          if(!empty($sportList)){
            foreach ($sportList as $list) {
               if(!in_array($list['sportId'], $unblockSport )){
              $menu[] = [
                          'name'        => $list['name'],
                          'slug'        => strtolower($list['slug']),
                          'sportId'     => $list['sportId'],
                          'status'      => $list['status'],
                          'icon'        => $url.'/'.$list['img'],
                          'icon_class'  => $list['icon_class'],
                          'sport_class' => $list['sport_class'],
                          'list'        => $this->getEventList($list['sportId'])
                        ];
                      }
            }
            $response = response()->json(['status'=>1,'code'=>200,'data'=>$menu,'message'=>'List found !!']);
          }else{
            $response = response()->json(['status'=>1,'code'=>200,'message'=>'List not found !!','data'=>null]);
          }
        return $response;
    }catch (\Exception $e) {
      $response = $this->errorLog($e);
      return response()->json($response, 501);
    }
  }

  public function getBlockStatusAdmin($eventIdOrSportId,$type)
  {
    if($type=='event') {
        $redis             = Redis::connection();
        $blockEventIdsJson = $redis->get('Block_Event_Market');
        $blockEventIds     = json_decode($blockEventIdsJson);
        if(!empty($blockEventIds)&&in_array($eventIdOrSportId,$blockEventIds)){ return 1; }else{ return 0; }
    }
  }
  
  public function getEventList($sportId)
  {
    try{
      $uid = Auth::user()->id;

      $list       = [];
      $cache      = Redis::connection();
      $event_Data = $cache->get("Event_List_" . $sportId);
      $event_Data1= json_decode($event_Data);
      $event_Data = [];
      if(!empty($event_Data1)) {
         foreach ($event_Data1->eventData as $key => $value) {
             $event_Data[] = $value;
         }
         $event_Data=  array_reverse($event_Data);
      }
      if(!empty($event_Data)){
        foreach ($event_Data as $event) {
          $unblockEvents = $this->getBlockStatusAdmin( $event->eventId, 'event');

            $blockEventList = $this->checkUnBlockList($uid);
          
            if(!in_array($event->eventId, $blockEventList)){

          if($event->game_over != 1 && $event->is_block != 1 && $unblockEvents==0){
            $runners = json_decode($event->runners);
            $runner1 = $runner2 = $runner3 = 0;
            if(isset($runners[0])){
              $runner1 = $runners[0]->runner;
            }
            if(isset($runners[1])){
              $runner2 = $runners[1]->runner;
            }
            if(isset($runners[3])){
              $runner3 = $runners[3]->runner;
            }

          

            $list[] = [
                        'eventId' => $event->eventId,
                        'name'    => $event->name,
                        'type'    => $event->type,
                        'runner1' => $runner1,
                        'runner2' => $runner2,
                        'runner3' => $runner3
                      ];
          }
        }
        }
      }
    return $list;
   }catch (\Exception $e) {
      $response = $this->errorLog($e);
      return response()->json($response, 501);
    }
  }


  public function checkUnBlockSportList($uId){
       try{

        $sportId =[];
        $user = DB::table('tbl_user_sport_status')->select('sid')->where('uid',$uId)->get();

       

        if(!empty($user) && $user != null){

            foreach ($user as $value) {
               $sportId[] = $value->sid; 
            }

        }
         //print_r($sportId); die('iuieor');
        return $sportId;

        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }

    public function checkUnBlockList($uId){

         $eventID =[];
         $user = DB::table('tbl_user_event_status')->select('eid')->where('uid',$uId)->get();       
        if(!empty($user) && $user != null){

            foreach ($user as $value) {
               $eventID[] = $value->eid; 
            }

        }
       
        return $eventID;
        
}
}