<?php

namespace App\Http\Controllers\currentBet;
use App\User;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\Controller;



class CurrentBetsController extends Controller
{

    /**
     * Bet History List
     */
    public function list()
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{

            $user = Auth::user(); $uid = $user->id;
            $where = [['uid',$uid],['status',1],['result','PENDING'],['systemId',$user->systemId]];
            if( $user != null ){
                $data = [];
                $select = ['_id','price','size','rate','win','loss','bType','mType','result','description','created_on','updated_on'];
                for( $i=1;$i<=7;$i++ ){
                    if( $i == 7 ){
                        $tbl = 'tbl_bet_pending_teenpatti';
                    }else{
                        $tbl = 'tbl_bet_pending_'.$i;
                    }

                    $list = DB::connection('mongodb')->table($tbl)->select($select)->where($where)->orderBy('updated_on', 'asc')->get();

                    if( $list->isNotEmpty() ){
                        foreach ( $list as $item ){
                            $data[] = $item;
                        }
                    }

                }

                $ord = [];
                foreach ($data as $key => $value){
                    $ord[] = isset($value->updated_on)?strtotime($value->updated_on):null;
                }
                array_multisort($data, SORT_DESC, $ord);

                if( $data != null ){
                    $response = [ "status" => 1 ,'code'=> 200, "data" => $data ,'message'=> 'Data Found !!' ];
                }else{
                    $response = [ "status" => 1 ,'code'=> 200, "data" => null ,'message'=>'Data not found !!' ];
                }
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    public function currentBetsOLD(Request $request)
    {
        
      try{

          $response = [ "status" => 0 , "code" => 400 , "message" => "Bad request!" ];
          $uid = Auth::guard('api')->user()->id;

          if( json_last_error() == JSON_ERROR_NONE ){
        
            if( isset( $request->start_date ) && isset( $request->end_date )
                && ( $request->start_date != '' ) && ( $request->end_date != '' )
                && ( $request->start_date != null ) && ( $request->end_date != null ) ){

                $startDate = date('yy-m-d', strtotime($request->start_date));
                $startDate =$startDate." 00:00:01";
                $endDate = date('yy-m-d', strtotime($request->end_date));
                $endDate =$endDate." 23:59:59";                
             }else{   	
                $start = new \DateTime('now +1 day');
                $endDate =  $start->format('yy-m-d h:i:s');
                $end = new \DateTime('now -5 day');
                $startDate = $end->format('yy-m-d h:i:s');  
             }     
           }

          if( isset( $request->type ) && $request->type != '' ){
             $type = $request->type;
             $where = ([['mType',$type],['uid',$uid],['status',1],['result','PENDING']]);
          }else{  
            $where = ([['uid',$uid],['status',1],['result','PENDING']]);
          }

          $models = [];
    
          $fancyCurrentBet = DB::connection('mongodb')->table('tbl_bet_pending_2')->select('_id','mType','rate','is_match','price','size','win','loss','bType','result','description','created_on')
                  ->where($where)
                  ->orderBy('created_on' ,'DESC')
                   ->get();
            foreach ($fancyCurrentBet as $value) {
                $models[] =$value;
            }

          $BookmakerCurrentBet = DB::connection('mongodb')->table('tbl_bet_pending_1')->select('_id','mType','rate','is_match','price','size','win','loss','bType','result','description','created_on')
                      ->where($where)
                      ->orderBy('created_on' ,'DESC')
                           ->get();
              foreach ($BookmakerCurrentBet as $value) {
                    $models[] =$value;
                }

             $khadoBallCurrentBet = DB::connection('mongodb')->table('tbl_bet_pending_3')->select('_id','mType','rate','is_match','price','size','win','loss','bType','result','description','diff','market','created_on')
                        ->where($where)
                        ->orderBy('created_on' ,'DESC')
                         ->get();

                 foreach ($khadoBallCurrentBet as $value) {
                      if($value->mType == 'ballbyball' ){
                            $market = $value->market;
                            $space = ' ';
                            $ball = strstr($market,$space,true);        
                            $value->ball= $ball;
                          }
                          if($value->mType == 'khado'){
                             $difference = $value->diff - $value->price ;        
                            $value->difference= $difference;
                          }
                            
                        
                      $models[] =$value;
                  }
              $jackpotLotteryCurrentBet = DB::connection('mongodb')->table('tbl_bet_pending_4')->select('_id','mType','rate','is_match','price','size','win','loss','bType','result','description','created_on')
                          ->where($where)
                          ->orderBy('created_on' ,'DESC')
                           ->get();

                       foreach ($jackpotLotteryCurrentBet as $value) {
                        $models[] =$value;
                    }

          $BinaryCurrentBet = DB::connection('mongodb')->table('tbl_bet_pending_5')->select('_id','mType','rate','is_match','price','size','win','loss','bType','result','description','created_on')
              ->where($where)
              ->orderBy('created_on' ,'DESC')
              ->get();

          foreach ($BinaryCurrentBet as $value) {
              $value->slug ='binary';
              $models[] =$value;
          }

               
                     $ord = array();
                      foreach ($models as $key => $value){
                          $ord[] = strtotime($value->created_on);
                      }
                      array_multisort($ord, SORT_DESC, $models);
     
        
              if( $models != null ){
                  $response = [ "status" => 1 ,'code'=> 200, "data" => $models ,'message'=> 'Data Found !!' ];
              }else{
                   $response = [ "status" => 1 ,'code'=> 200, "data" => null ,'message'=>'Data not found !!' ];
              }


      return $response;
    
    }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

  }
}

