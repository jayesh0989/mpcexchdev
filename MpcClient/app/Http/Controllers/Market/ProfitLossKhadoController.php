<?php

namespace App\Http\Controllers\Market;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;


class ProfitLossKhadoController extends Controller
{

    public function profitLossKhado(Request $request)
    {
       

       try{

        $response = ['status'=>0,'code'=>400,'message'=>'Bad request !!!','data'=>null];

        if ($request['market_id']) {
            $userId = Auth::user()->id;
            $marketId = $request['market_id'];
            $eventId = $request['event_id'];
            $sessionType = $request['session_type'];
            $betList = null;

            $where = [ ['eid', $eventId],['mid', $marketId] , ['uid', $userId], ['mType',$sessionType] ];
            $betListData = DB::table('tbl_bet_pending_3')->select('runner','rate','price','size','win','loss','bType','mType')
                  ->where($where)
                  ->orderBy('id' ,'DESC')
                   ->get();

            if ($betListData != null) {
                $betList = $betListData;
            }

            $profitLossData = $this->getProfitLossKhado($userId, $eventId, $marketId, $sessionType);

            $response = ['status' => 1, 'code'=> 200, 'data' =>[ 'profit_loss' => $profitLossData, 'betList' => $betList], 'message'=>'Data Found !!'];

        }

        return $response;

       }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
  
    }


    // getProfitLossFancy 
    public function getProfitLossKhado($userId,$eventId, $marketId, $sessionType)
    {

        $where = [ ['result', 'PENDING'],['mid', $marketId] , ['uid', $userId], ['mType',$sessionType], ['status',1] ];
        $betList = DB::table('tbl_bet_pending_3')->select('price','win','loss','bType','diff')
            ->where($where)
            ->get();
    

        $dataReturn = null;
        $result = $newbetresult = $betresult = [];
        if( $betList != null ){
            $min = $max = 0;

            foreach ($betList as $index => $bet) {
                $betresult[]= array('price'=>$bet->price,'bet_type'=>$bet->bType,'loss'=>$bet->loss,'win'=>$bet->win,'difference'=>$bet->diff);
                if ($index == 0) {
                    $min = $bet->price;
                    $max = $bet->diff;
                }
                if ($min > $bet->price)
                    $min = $bet->price;
                if ($max < $bet->diff)
                    $max = $bet->diff;
            }

            $min = $min-20;
            if( $min < 0 ){
                $min = 0;
            }
            // $max = $max+1;
            $max =  $min+100;
            $betarray = []; $bet_type = '';
            $win=0; $loss=0; $count= $min;
            $lossVal1 = 0;
            $totalbetcount=count($betresult);
            for($i=$min;$i<=$max;$i++){
                $winVal=0;

                $currentVal=(int)$i;
                  $sqlQuery = "SELECT SUM(win) as winVal FROM tbl_bet_pending_3 where diff > $currentVal and price <= $currentVal and status=1 and result='PENDING' and mid='".$marketId."' and uid=".$userId." and mType='".$sessionType."'";
                $betList1 = DB::select(DB::raw($sqlQuery));
               // print_r($betList1); exit;
                if(!empty($betList1)) {
                    $winVal = $betList1[0]->winVal;
                }


                $sqlQuery = "SELECT SUM(loss) as lossVal FROM tbl_bet_pending_3 where diff <= $currentVal OR price > $currentVal and status=1 and result='PENDING' and mid='".$marketId."' and uid=".$userId." and mType='".$sessionType."'";
                $betList3 = DB::select(DB::raw($sqlQuery));

                $lossVal1 = 0;
                if(!empty($betList3)) {
                    $lossVal1 = $betList3[0]->lossVal;
                }

                $total = $winVal-$lossVal1;

                $dataReturn[] = [
                    'price' => $i,
                    'profitLoss' => round($total,0)
                ];
            }

        }


        return $dataReturn;
    }


 }