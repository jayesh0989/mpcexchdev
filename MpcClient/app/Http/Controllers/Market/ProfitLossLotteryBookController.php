<?php

namespace App\Http\Controllers\Market;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redis;


class ProfitLossLotteryBookController extends Controller
{

    public function profitLossLotteryBook(Request $request)
    {
        $betList = null;
        $response = response()->json(['status' => 0, 'code' => 400, 'message' => 'Bad request !!!', 'data' => null]);
        try {
            if (json_last_error() == JSON_ERROR_NONE) {
                $uid = Auth::user()->id;
                $marketId = $request->market_id;
                $eventId = $request->event_id;
                $sessionType = $request->session_type;
                $where = ([['eid', $eventId], ['mid', $marketId], ['mType', $sessionType], ['uid', $uid], ['status', 1]]);
                $betListData = DB::table('tbl_bet_pending_4')->select('id', 'runner', 'rate', 'price', 'size', 'win', 'loss', 'bType', 'runner')
                    ->where($where)
                    ->orderBy('id', 'DESC')
                    ->get();
                if ($betListData != null) {
                    $betList = $betListData;
                }

                $numbers = [];
                for ($n = 0; $n < 10; $n++) {
                    $profitLossData = $this->getLotteryProfitLossOnBet($uid,$eventId,$marketId, $n);
                    $price[] = [
                        'price' => $n,
                        'profitLoss' => $profitLossData
                    ];
                }

                $response = response()->json(['status' => 1, 'code' => 200, 'data' => ['profit_loss' => $price, 'betList' => $betList]]);
            } else {
                $response = response()->json(['status' => 1, 'code' => 200, 'data' => ['profit_loss' => null, 'betList' => $betList]]);
            }

            return $response;
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }


    //Event: get Lottery Profit Loss On Bet
    public function getLotteryProfitLossOnBet($userId,$eventId, $marketId, $selectionId)
    {
        try {
            $total = 0;
            $where = ([['eid', $eventId], ['mid', $marketId], ['mType', 'cricket_casino'], ['result', 'PENDING'], ['uid', $userId], ['status', 1]]);
            // IF RUNNER WIN
            $betWinList = DB::table('tbl_bet_pending_4')->where($where)->where('secId', $selectionId)->sum('win');
            // IF RUNNER LOSS
            $betLossList = DB::table('tbl_bet_pending_4')->where($where)->where('secId', '!=', $selectionId)->sum('loss');
            if ($betWinList == null) {
                $totalWin = 0;
            } else {
                $totalWin = $betWinList;
            }
            if ($betLossList == null) {
                $totalLoss = 0;
            } else {
                $totalLoss = (-1) * $betLossList;
            }
            $total = round($totalWin + $totalLoss);
            return $total;
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }


 }